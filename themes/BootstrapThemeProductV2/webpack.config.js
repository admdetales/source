const Encore = require('@symfony/webpack-encore');

Encore
  .setOutputPath('public/bootstrap-theme-product-v2')
  .setPublicPath('/bootstrap-theme-product-v2')
  .addEntry('appProductV2', './themes/BootstrapThemeProductV2/assets/appProductV2.js')
  .disableSingleRuntimeChunk()
  .cleanupOutputBeforeBuild()
  .enableSassLoader()
  .enableSourceMaps(!Encore.isProduction())
  .enableVersioning(Encore.isProduction());

const config = Encore.getWebpackConfig();
config.name = 'bootstrapThemeProductV2';

module.exports = config;
