/* global document */
import jQuery from 'jquery';
import { docReady } from './utilities';

docReady(() => {
  const toggleEls = document.querySelectorAll('.toggle-by-selector');

  toggleEls.forEach((toggleEl) => {
    toggleEl.addEventListener('click', () => {
      const toggleSelector = toggleEl.dataset.toggleselector;

      if (toggleSelector) {
        const $elsToToggle = jQuery(toggleSelector);

        $elsToToggle.each((i, elToToggle) => {
          const $elToToggle = jQuery(elToToggle);
          $elToToggle.toggleClass('hide');
        });
      }
    });
  });
});
