/* global document, fetch, AbortController */
import { docReady } from './utilities';

docReady(() => {
  const modal = document.getElementById('analogue-modal');

  if (modal) {
    const modalBody = modal.querySelector('#analogue-modal-body');
    const modalLoadingScreen = modal.querySelector('#analogue-card-loading');
    const modalErrorMessage = modal.querySelector('#modal-error-container');
    const show = (el) => {
      el.classList.add('d-block');
      el.classList.remove('d-none');
    };
    const hide = (el) => {
      el.classList.remove('d-block');
      el.classList.add('d-none');
    };
    const resetModal = () => {
      modalBody.innerHTML = '';
      hide(modalLoadingScreen);
      hide(modalErrorMessage);
      hide(modalBody);
    };
    let controller;

    modal.addEventListener('show.bs.modal', (e) => {
      controller = new AbortController();
      const { signal } = controller;

      show(modalLoadingScreen);
      fetch(e.relatedTarget.dataset.requestPath, {
        signal,
        method: 'GET',
        headers: {
          'Content-Type': 'text/html',
        },
      })
        .then((response) => {
          if (!response.ok) {
            throw Error(response.statusText);
          }
          return response.text();
        })
        .then((body) => {
          resetModal();
          modalBody.innerHTML = body;
          show(modalBody);
        })
        .catch((error) => {
          if (error && error.name && error.name === 'AbortError') return;

          resetModal();
          show(modalErrorMessage);
        });
    }, false);

    modal.addEventListener('hidden.bs.modal', () => {
      controller.abort();
      resetModal();
    }, false);
  }
});
