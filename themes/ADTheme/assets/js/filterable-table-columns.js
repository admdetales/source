import jQuery from 'jquery';
// eslint-disable-next-line no-unused-vars
import dataTables from 'mdbootstrap/js/addons/datatables.min';
import { docReady } from './utilities';

docReady(() => {
  jQuery('.filterable-columns-table').dataTable({
    paging: false,
    info: false,
    ordering: false,
    initComplete() {
      this.api().columns().every(function columnCallback() {
        const column = this;
        const $headerInput = jQuery(this.header()).find('input');

        if ($headerInput) {
          $headerInput.on('change input', (e) => {
            const val = e.target.value;

            column.search(val || '', true, false).draw();
          });
        }

        return null;
      });
    },
  });
});
