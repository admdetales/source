import jQuery from 'jquery';
// eslint-disable-next-line no-unused-vars
import select2 from 'select2';
import 'select2/dist/css/select2.min.css';
import '@ttskch/select2-bootstrap4-theme/dist/select2-bootstrap4.css';
import { docReady } from './utilities';

docReady(() => {
  const $selectAddress = jQuery('.address-select');
  const selectAddress = $selectAddress[0];

  if (selectAddress) {
    const $cityInput = jQuery("input[name='sylius_checkout_address[shippingAddress][city]']");
    const $streetInput = jQuery("input[name='sylius_checkout_address[shippingAddress][street]']");
    const $houseNumber = jQuery("input[name='sylius_checkout_address[shippingAddress][houseNumber]']");
    const $postCodeInput = jQuery("input[name='sylius_checkout_address[shippingAddress][postcode]']");
    const {
      placeholder, searching, searchingByCity,
    } = selectAddress.dataset;
    const selectCityInput = $cityInput[0];

    $selectAddress.select2({
      theme: 'bootstrap4',
      allowClear: true,
      ajax: {
        url: 'https://api.postit.lt/v2/',
        dataType: 'json',
        delay: 250,
        data(params) {
          return {
            key: window.postitApiKey,
            city: selectCityInput.value,
            address: params.term,
            page: params.page,
          };
        },
        processResults(response) {
          const data = jQuery.map(response.data, item => ({
            post_code: item.post_code,
            city: item.city,
            street: item.street,
            number: item.number,
            text: `${selectCityInput.value ? '' : item.city} ${item.street} ${item.number}`,
            id: item.number,
          }));

          return {
            results: data,
            pagination: {
              more: response.page.current < response.page.total,
            },
          };
        },
      },
      placeholder,
      templateResult(item) {
        if (selectCityInput.value && item.loading) {
          return `${searchingByCity} ${selectCityInput.value}`;
        }

        if (item.loading) {
          return searching;
        }

        return item.text;
      },
    }).on('select2:selecting', (e) => {
      const params = e.params.args.data;

      $cityInput.val(params.city);
      $postCodeInput.val(params.post_code);
      $streetInput.val(params.street);
      $houseNumber.val(params.number);
    });
  }
});
