$('#footer-newsletter-form').submit(function(event) {
    event.preventDefault();
    var form = $(this);

    var successElement = form.find('.success-element');
    var validationElement = form.find('.validation-element');

    successElement.text('');
    validationElement.text('');

    $.ajax({
        url: $(form).attr('action'),
        type: $(form).attr('method'),
        data: form.serialize()
    })
        .done(function (response) {
            if (response.hasOwnProperty('message')) {
                successElement.html(response.message);
            }
        })
        .fail(function (response) {
            const errors = JSON.parse(JSON.parse(response.responseText).errors);
            let message = '';
            $(errors).each(function (key, value) {
                message += value + " ";
            });
            validationElement.html(message);
        });
});
