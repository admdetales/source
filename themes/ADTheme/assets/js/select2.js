/* global window */
import jQuery from 'jquery';
// eslint-disable-next-line no-unused-vars
import select2 from 'select2';
import 'select2/dist/css/select2.min.css';
import '@ttskch/select2-bootstrap4-theme/dist/select2-bootstrap4.css';
import { docReady } from './utilities';

docReady(() => {
  const $selectEl = jQuery('.dropdown-navigate-on-select');
  const selectEl = $selectEl[0];

  if (selectEl) {
    $selectEl.select2({
      placeholder: selectEl.dataset.placeholder,
      theme: 'bootstrap4',
    });

    $selectEl.on('select2:select', (e) => {
      window.open(e.params.data.id, '_self');
    });
  }
});
