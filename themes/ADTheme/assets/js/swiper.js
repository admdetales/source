/* global document */
import Swiper from 'swiper/bundle';
import 'swiper/css/bundle';

/*const swiperContainers = document.querySelectorAll('.swiper-container');
swiperContainers.forEach((swiperContainer) => {
  const productCount = swiperContainer.dataset.productcount;

  new Swiper(swiperContainer, {
    loop: productCount > 4,
    slidesPerView: 'auto',
    slidesPerGroup: 4,

    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
  });
});*/

const swiper = new Swiper('.swiper', {
  direction: 'horizontal',
  loop: true,

  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev'
  },

  slidesPerView: 4,
  breakpointsBase: 'container'
});