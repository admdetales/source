/* global document */
import { docReady } from './utilities';

docReady(() => {
  const activeCompatibleVehiclesTab = document.querySelector('.compatible-vehicles-nav.active');

  if (activeCompatibleVehiclesTab) {
    setTimeout(() => {
      activeCompatibleVehiclesTab.scrollIntoView({ block: 'center', inline: 'nearest' });
    }, 0);
  }
});
