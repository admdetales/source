// FontAwesome Icons
// Import icons one by one to reduce size of the output
import { library, dom } from '@fortawesome/fontawesome-svg-core';

import { faMobileAlt } from '@fortawesome/free-solid-svg-icons/faMobileAlt';
import { faChevronRight } from '@fortawesome/free-solid-svg-icons/faChevronRight';
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons/faChevronLeft';
import { faGreaterThan } from '@fortawesome/free-solid-svg-icons/faGreaterThan';
import { faEnvelope } from '@fortawesome/free-regular-svg-icons/faEnvelope';
import { faFacebook } from '@fortawesome/free-brands-svg-icons/faFacebook';
import { faLink } from '@fortawesome/free-solid-svg-icons/faLink';
import { faTimesCircle } from '@fortawesome/free-regular-svg-icons/faTimesCircle';
import { faCheckCircle } from '@fortawesome/free-regular-svg-icons/faCheckCircle';

library.add(
  faMobileAlt,
  faChevronRight,
  faChevronLeft,
  faGreaterThan,
  faLink,
  faTimesCircle,
  faCheckCircle,

  faEnvelope,
  faFacebook,
);
dom.watch();
