const Encore = require('@symfony/webpack-encore');

Encore.configureRuntimeEnvironment('dev');

Encore
    .setOutputPath('public/ad-theme')
    .setPublicPath('/ad-theme')
    .addEntry('ad-app', './themes/ADTheme/assets/entry.js')
    .copyFiles({
        from: './themes/ADTheme/assets/images',
        to: 'images/[path][name].[hash:8].[ext]',
    })
    .disableSingleRuntimeChunk()
    .cleanupOutputBeforeBuild()
    .enableSassLoader()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction());

const config = Encore.getWebpackConfig();
config.name = 'ad-app';
config.mode = 'production';

module.exports = config;