const Encore = require('@symfony/webpack-encore');

Encore
  .setOutputPath('public/bootstrap-theme-product-v4')
  .setPublicPath('/bootstrap-theme-product-v4')
  .addEntry('appProductV4', './themes/BootstrapThemeProductV4/assets/appProductV4.js')
  .disableSingleRuntimeChunk()
  .cleanupOutputBeforeBuild()
  .enableSassLoader()
  .enableSourceMaps(!Encore.isProduction())
  .enableVersioning(Encore.isProduction());

const config = Encore.getWebpackConfig();
config.name = 'bootstrapThemeProductV4';

module.exports = config;
