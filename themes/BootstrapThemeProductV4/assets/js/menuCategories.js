const handleHamburgerMenu = () => {
  const toggleOpener = document.querySelector('[hamburgerMenuOpen]');
  const toggleCloser = document.querySelector('[hamburgerMenuClose]');
  const mobileMenu = document.querySelector('.category-menu--mobile');
  const mobileMenuActiveClass = 'category-menu--mobile--active';

  toggleOpener.onclick = () => {
    mobileMenu.classList.add(mobileMenuActiveClass);
  };

  toggleCloser.onclick = () => {
    mobileMenu.classList.remove(mobileMenuActiveClass);
  };
};

const handleDesktopMenu = () => {

  if (!document.querySelectorAll('.toplevel-categories__item')[0] || document.querySelector('.category-dropdown--no-child-grid')) {
    return;
  }

  const topLevelCategories = document.querySelectorAll(`[topLevelCategoryId]`);
  const topLevelCategoryId = "topLevelCategoryId";
  const childCategoryGridId = "childCategoryGridId";
  const topLevelCategoryItemActiveCssClass = "toplevel-categories__item--active";
  const childCategoryGridVisibleCssClass = "child-categories__grid--visible";
  const childCategoryGridActiveCssClass = "child-categories__grid--active";
  const transitionFast = 200;

  document.querySelectorAll('.toplevel-categories__item')[0].classList.add(topLevelCategoryItemActiveCssClass);
  document.querySelectorAll('.child-categories__grid')[0].classList.add(childCategoryGridVisibleCssClass, childCategoryGridActiveCssClass);

  topLevelCategories.forEach((category) => {
    category.onmouseenter = () => {
      topLevelCategories.forEach((category) =>
        category.classList.remove(topLevelCategoryItemActiveCssClass)
      );
      category.classList.add(topLevelCategoryItemActiveCssClass);

      const categoryId = category.getAttribute(topLevelCategoryId);

      document
        .querySelectorAll(`[${childCategoryGridId}]`)
        .forEach((category) => {
          category.classList.remove(childCategoryGridActiveCssClass);
          setTimeout(() => {
            category.classList.remove(childCategoryGridVisibleCssClass);
          }, transitionFast);
        });

      const childCategory = document.querySelector(
        `[${childCategoryGridId}='${categoryId}']`
      );
      childCategory.classList.add(childCategoryGridActiveCssClass);
      setTimeout(() => {
        childCategory.classList.add(childCategoryGridVisibleCssClass);
      }, transitionFast);
    };
  });
}

export default function initMenuCategories() {
  handleDesktopMenu();
  handleHamburgerMenu();
}


