const OmniParcelMachine = (form) => {
  const choices = form.querySelectorAll('input[type=radio]');

  choices.forEach(el => {
    el.addEventListener('change', function() {
      let request = new XMLHttpRequest();
      request.open('POST', form.action);
      request.send(new FormData(form));

      request.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          window.location.reload();
        }
      };
    });
  });
};

export default OmniParcelMachine;
