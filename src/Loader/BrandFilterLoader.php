<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Loader;

use App\Entity\Product\Product;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\ChoiceList\ORMQueryBuilderLoader;

class BrandFilterLoader extends ORMQueryBuilderLoader
{
    private $queryBuilder;

    public function __construct(QueryBuilder $queryBuilder)
    {
        parent::__construct($queryBuilder);

        $this->queryBuilder = $queryBuilder;
    }

    /**
     * {@inheritdoc}
     */
    public function getEntities()
    {
        $qd = $this->queryBuilder;
        $qd->leftJoin(
            Product::class,
            'p',
            \Doctrine\ORM\Query\Expr\Join::WITH,
            'p.brand = e.id'
        );
        $qd->where('p.id IS NOT NULL');

        return $qd->getQuery()->execute();
    }
}
