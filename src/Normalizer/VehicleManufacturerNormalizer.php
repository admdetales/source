<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Normalizer;

use App\Repository\TecDoc\PopularManufacturerRepositoryInterface;
use App\Router\Generator\VehicleUrlGenerator;
use Nfq\Bundle\TecDocBundle\Entity\VehicleManufacturer;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class VehicleManufacturerNormalizer implements NormalizerInterface
{
    /** @var PopularManufacturerRepositoryInterface */
    private $popularManufacturerRepository;

    /** @var VehicleUrlGenerator */
    private $vehicleUrlGenerator;

    public function __construct(
        PopularManufacturerRepositoryInterface $popularManufacturerRepository,
        VehicleUrlGenerator $vehicleUrlGenerator
    ) {
        $this->popularManufacturerRepository = $popularManufacturerRepository;
        $this->vehicleUrlGenerator           = $vehicleUrlGenerator;
    }

    /**
     * @param VehicleManufacturer $object
     * @param string|null         $format
     * @param array               $context
     *
     * @return array<string, int|string|bool>
     */
    public function normalize($object, string $format = null, array $context = []): array
    {
        return [
            'id'        => $object->getId(),
            'title'     => $object->getTitle(),
            'url'       => $this->vehicleUrlGenerator->generateManufacturerUrl($object),
            'isPopular' => $this->popularManufacturerRepository->findByVehicleManufacturer($object) !== null,
        ];
    }

    public function supportsNormalization($data, string $format = null): bool
    {
        return $data instanceof VehicleManufacturer;
    }
}
