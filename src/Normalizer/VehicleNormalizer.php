<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Normalizer;

use App\Router\Generator\VehicleUrlGenerator;
use Nfq\Bundle\TecDocBundle\Entity\Vehicle;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

use function sprintf;

class VehicleNormalizer implements NormalizerInterface
{
    /** @var VehicleUrlGenerator */
    private $vehicleUrlGenerator;

    public function __construct(VehicleUrlGenerator $vehicleUrlGenerator)
    {
        $this->vehicleUrlGenerator = $vehicleUrlGenerator;
    }

    /**
     * @param Vehicle     $object
     * @param string|null $format
     * @param array       $context
     *
     * @return array<string, int|string|bool>
     */
    public function normalize($object, string $format = null, array $context = []): array
    {
        return [
            'id'                => $object->getId(),
            'manufacturerTitle' => $object->getManufacturer()->getTitle(),
            'modelTitle'        => $object->getModel()->getTitle(),
            'constructionYear'  => $object->getModel()->getYearOfConstructionInterval(),
            'engine'            => sprintf('%s kW, %s hp', $object->getPowerKwTo(), $object->getPowerHpTo()),
            'url'               => $this->vehicleUrlGenerator->generateProductUrl($object),
        ];
    }

    public function supportsNormalization($data, string $format = null): bool
    {
        return $data instanceof Vehicle;
    }
}
