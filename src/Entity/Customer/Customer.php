<?php

declare(strict_types=1);

namespace App\Entity\Customer;

use App\Entity\Consent\ConsentAgreement;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Sylius\Component\Core\Model\Customer as BaseCustomer;

/**
 * @ORM\Entity
 * @ORM\Table(name="sylius_customer")
 */
class Customer extends BaseCustomer implements CustomerInterface
{
    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     */
    private $companyCode;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     */
    private $companyName;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     */
    private $vatNumber;

    /**
     * @var ConsentAgreement[]|Collection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Consent\ConsentAgreement", cascade={"PERSIST"})
     * @ORM\JoinTable(
     *     name="sylius_customer_consent_agreement",
     *     joinColumns={@ORM\JoinColumn(name="customer_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="consent_agreement_id", referencedColumnName="id", unique=true)},
     * )
     */
    private $consentAgreements;

    public function __construct()
    {
        parent::__construct();

        $this->consentAgreements = new ArrayCollection();
    }

    public function getCompanyCode(): ?string
    {
        return $this->companyCode;
    }

    public function setCompanyCode(?string $companyCode): void
    {
        $this->companyCode = $companyCode;
    }

    public function getCompanyName(): ?string
    {
        return $this->companyName;
    }

    public function setCompanyName(?string $companyName): void
    {
        $this->companyName = $companyName;
    }

    public function getVatNumber(): ?string
    {
        return $this->vatNumber;
    }

    public function setVatNumber(?string $vatNumber): void
    {
        $this->vatNumber = $vatNumber;
    }

    public function addConsentAgreement(ConsentAgreement $consentAgreement): void
    {
        if (false === $this->consentAgreements->contains($consentAgreement)) {
            $this->consentAgreements->add($consentAgreement);
        }
    }

    public function removeConsentAgreement(ConsentAgreement $consentAgreement): void
    {
        if ($this->consentAgreements->contains($consentAgreement)) {
            $this->consentAgreements->removeElement($consentAgreement);
        }
    }
}
