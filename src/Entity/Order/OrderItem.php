<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Entity\Order;

use Doctrine\ORM\Mapping as ORM;
use Sylius\Component\Core\Calculator\ProductVariantPriceCalculatorInterface;
use Sylius\Component\Core\Model\OrderItem as BaseOrderItem;

/**
 * @ORM\Entity
 * @ORM\Table(name="sylius_order_item")
 */
class OrderItem extends BaseOrderItem
{
    /** @var ProductVariantPriceCalculatorInterface|null */
    private $productVariantPriceCalculator;

    public function setProductVariantPriceCaltulator(
        ?ProductVariantPriceCalculatorInterface $productVariantPriceCalculator
    ): void {
        $this->productVariantPriceCalculator = $productVariantPriceCalculator;
    }

    public function getProductVariantPriceCaltulator(): ?ProductVariantPriceCalculatorInterface
    {
        return $this->productVariantPriceCalculator;
    }

    public function hasDiscount(): bool
    {
        return $this->getFullPrice() !== $this->getDiscountedPrice();
    }

    public function getDiscountPercentage(): int
    {
        return (int)round(100 - (100 * $this->getDiscountedPrice() / $this->getFullPrice()));
    }

    public function getFullPrice(): int
    {
        $productVariantPriceCalculator = $this->getProductVariantPriceCaltulator();
        return $productVariantPriceCalculator->calculate(
            $this->getVariant(),
            ['price_only' => true]
        );
    }

    public function getDiscountedPrice(): int
    {
        return $this->getTotal();
    }
}
