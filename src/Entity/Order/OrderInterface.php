<?php

declare(strict_types=1);

namespace App\Entity\Order;

use App\Entity\Customer\CustomerInterface;
use Sylius\Component\Core\Model\OrderInterface as BaseOrderInterface;

interface OrderInterface extends BaseOrderInterface
{
    public function getConsentSubject(): CustomerInterface;
}
