<?php

declare(strict_types=1);

namespace App\Entity\Channel;

use App\Entity\Surcharge\CustomerSurchargePrice;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Selectable;
use Doctrine\ORM\Mapping as ORM;
use Sylius\Component\Core\Model\ChannelPricing as BaseChannelPricing;

/**
 * @ORM\Entity
 * @ORM\Table(name="sylius_channel_pricing")
 */
class ChannelPricing extends BaseChannelPricing implements ChannelPricingInterface
{
    /**
     * @var Collection<CustomerSurchargePrice>&Selectable<CustomerSurchargePrice>
     *
     * @ORM\OneToMany(
     *     targetEntity="App\Entity\Surcharge\CustomerSurchargePrice",
     *     mappedBy="channelPricing",
     *     cascade={"persist", "remove"},
     *     orphanRemoval=true
     * )
     */
    private $surchargePrices;

    public function __construct()
    {
        $this->surchargePrices = new ArrayCollection();
    }

    public function getSurchargePrices(): Collection
    {
        return $this->surchargePrices;
    }

    public function addSurchargePrice(CustomerSurchargePrice $customerSurchargePrice): void
    {
        if (!$this->surchargePrices->contains($customerSurchargePrice)) {
            $this->surchargePrices->add($customerSurchargePrice);
            $customerSurchargePrice->setChannelPricing($this);
        }
    }

    public function removeSurchargePrice(CustomerSurchargePrice $customerSurchargePrice): void
    {
        if ($this->surchargePrices->contains($customerSurchargePrice)) {
            $this->surchargePrices->removeElement($customerSurchargePrice);
        }
    }
}
