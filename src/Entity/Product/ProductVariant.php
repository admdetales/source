<?php

declare(strict_types=1);

namespace App\Entity\Product;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Sylius\Component\Core\Model\ChannelPricingInterface;
use Sylius\Component\Core\Model\ProductVariant as BaseProductVariant;
use Sylius\Component\Product\Model\ProductVariantTranslationInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="sylius_product_variant")
 */
class ProductVariant extends BaseProductVariant implements ProductVariantInterface
{
    /**
     * @var ProductVariantWarehouseStock[]|Collection
     * @ORM\OneToMany(
     *     targetEntity="App\Entity\Product\ProductVariantWarehouseStock",
     *     mappedBy="productVariant",
     *     cascade={"persist", "remove"},
     *     orphanRemoval=true,
     *     fetch="EAGER"
     * )
     * @ORM\OrderBy({"warehouseName" = "ASC"})
     */
    private $warehousesStock;

    public function __construct()
    {
        parent::__construct();

        $this->warehousesStock = new ArrayCollection();
    }

    /**
     * {@inheritdoc}
     */
    public function getWarehousesStock(): Collection
    {
        return $this->warehousesStock;
    }

    /**
     * {@inheritdoc}
     */
    public function getWarehouseStockByName(string $warehouseName): ?ProductVariantWarehouseStock
    {
        $encoding = \mb_internal_encoding();
        $warehouseName = \mb_strtoupper($warehouseName, $encoding);

        foreach ($this->warehousesStock as $warehouseStock) {
            if (\strcmp($warehouseName, \mb_strtoupper($warehouseStock->getWarehouseName(), $encoding)) === 0) {
                return $warehouseStock;
            }
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function addWarehouseStock(ProductVariantWarehouseStock $warehouseStock): void
    {
        if (!$this->warehousesStock->contains($warehouseStock)) {
            $this->warehousesStock->add($warehouseStock);
            $warehouseStock->setProductVariant($this);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function removeWarehouseStock(ProductVariantWarehouseStock $warehouseStock): void
    {
        if ($this->warehousesStock->contains($warehouseStock)) {
            $this->warehousesStock->removeElement($warehouseStock);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function recalculateOnHandFromWarehouseStock(): void
    {
        $onHand = 0;
        foreach ($this->warehousesStock as $warehouseStock) {
            $onHand += $warehouseStock->getStock();
        }

        $this->setOnHand($onHand);
    }

    protected function createTranslation(): ProductVariantTranslationInterface
    {
        return new ProductVariantTranslation();
    }

    /**
     * {@inheritdoc}
     */
    public function getChannelPricingForChannelCode(string $channelCode): ?ChannelPricingInterface
    {
        foreach ($this->getChannelPricings() as $channelPricing) {
            if ($channelPricing->getChannelCode() === $channelCode) {
                return $channelPricing;
            }
        }

        return null;
    }
}
