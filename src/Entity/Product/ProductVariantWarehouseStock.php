<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Entity\Product;

use Doctrine\ORM\Mapping as ORM;
use Sylius\Component\Resource\Model\ResourceInterface;

/**
 * @ORM\Entity()
 * @ORM\Table(name="sylius_product_variant_warehouse_stock")
 */
class ProductVariantWarehouseStock implements ResourceInterface
{
    /**
     * @var int|null
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     * @ORM\Id()
     */
    private $id;

    /**
     * @var ProductVariantInterface
     * @ORM\ManyToOne(targetEntity="App\Entity\Product\ProductVariant", inversedBy="warehousesStock")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private $productVariant;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    private $warehouseName;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=false, options={"default": 0})
     */
    private $stock;

    /**
     * @param ProductVariantInterface $productVariant
     * @param string $warehouseName
     * @param int $stock
     */
    public function __construct(ProductVariantInterface $productVariant, string $warehouseName, int $stock = 0)
    {
        $this->productVariant = $productVariant;
        $this->productVariant->addWarehouseStock($this);
        $this->warehouseName = $warehouseName;
        $this->stock = $stock;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return ProductVariantInterface
     */
    public function getProductVariant(): ProductVariantInterface
    {
        return $this->productVariant;
    }

    /**
     * @param ProductVariantInterface $productVariant
     */
    public function setProductVariant(ProductVariantInterface $productVariant): void
    {
        if ($this->productVariant !== $productVariant) {
            $this->productVariant = $productVariant;
            $this->productVariant->addWarehouseStock($this);
        }
    }

    /**
     * @return string
     */
    public function getWarehouseName(): string
    {
        return $this->warehouseName;
    }

    /**
     * @param string $warehouseName
     */
    public function setWarehouseName(string $warehouseName): void
    {
        $this->warehouseName = $warehouseName;
    }

    /**
     * @return int
     */
    public function getStock(): int
    {
        return $this->stock;
    }

    /**
     * @param int $stock
     */
    public function setStock(int $stock): void
    {
        $this->stock = $stock;
    }
}
