<?php

declare(strict_types=1);

namespace App\Entity\Product;

use App\Entity\Brand\BrandInterface;
use App\Entity\Brand\InternalBrand;
use App\Entity\Supplier\Supplier;
use App\Entity\Supplier\SupplierInterface;
use App\Entity\Taxonomy\InternalCategory;
use App\Entity\TecDoc\TecDocDocument;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Omni\Sylius\FilterPlugin\Model\Product as BaseProduct;
use Sylius\Component\Attribute\Model\AttributeValueInterface;
use Sylius\Component\Core\Model\ImageInterface;
use Sylius\Component\Core\Model\ProductTaxonInterface;
use Sylius\Component\Product\Model\ProductTranslationInterface;

use function array_reduce;

/**
 * @ORM\Entity
 * @ORM\Table(name="sylius_product")
 */
class Product extends BaseProduct implements ProductInterface
{
    /**
     * @var BrandInterface|null
     *
     * @ORM\ManyToOne(
     *     targetEntity="App\Entity\Brand\BrandInterface",
     *     cascade={"persist"},
     *     fetch="EAGER",
     *     inversedBy="products"
     * )
     * @ORM\JoinColumn(name="brand_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $brand;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $brandCode;

    /**
     * @var OeNumber[]|Collection
     * @ORM\OneToMany(targetEntity="OeNumber", mappedBy="product", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $oeNumbers;

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=false, options={"default"="0"})
     */
    private $weekly = false;

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=false, options={"default"="0"})
     */
    private $mostPopular = false;

    /**
     * @var InternalBrand|null
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Brand\InternalBrand", cascade={"persist"})
     * @ORM\JoinColumn(name="internal_brand_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $internalBrand;

    /**
     * @var InternalCategory|null
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Taxonomy\InternalCategory", cascade={"persist"})
     * @ORM\JoinColumn(name="internal_category_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $internalCategory;

    /** @var string|null */
    private $tecDocName;

    /** @var AttributeValueInterface[] */
    private $tecDocAttributes = [];

    /** @var ImageInterface[] */
    private $tecDocImages = [];

    /** @var TecDocDocument[] */
    private $tecDocDocuments = [];

    /** @var bool */
    private $hasVehicles = false;

    /**
     * @var Supplier|null
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Supplier\Supplier", inversedBy="products")
     * @ORM\JoinColumn(name="supplier_id", referencedColumnName="id")
     */
    private $supplier;

    public function __construct()
    {
        parent::__construct();

        $this->oeNumbers = new ArrayCollection();
    }

    public function getBrandCode(): ?string
    {
        return $this->brandCode;
    }

    public function setBrandCode(?string $brandCode): void
    {
        $this->brandCode = $brandCode;
    }

    protected function createTranslation(): ProductTranslationInterface
    {
        return new ProductTranslation();
    }

    public function isMostPopular(): bool
    {
        return $this->mostPopular;
    }

    public function setMostPopular(bool $mostPopular): void
    {
        $this->mostPopular = $mostPopular;
    }

    public function isWeekly(): bool
    {
        return $this->weekly;
    }

    public function isInStock(): bool
    {
        return $this->variants->exists(
            static function ($key, ProductVariant $variant): bool {
                return $variant->isInStock();
            }
        );
    }

    public function setWeekly(bool $weekly): void
    {
        $this->weekly = $weekly;
    }

    /**
     * @return OeNumber[]|Collection
     */
    public function getOeNumbers(): Collection
    {
        return $this->oeNumbers;
    }

    public function addOeNumber(OeNumber $oeNumber): void
    {
        if ($this->oeNumbers->contains($oeNumber)) {
            return;
        }

        $oeNumber->setProduct($this);
        $this->oeNumbers->add($oeNumber);
    }

    public function getOeNumbersSearchIndex(): string
    {
        return array_reduce(
            $this->oeNumbers->getValues(),
            static function (string $index, OeNumber $number): string {
                return $index . $number->getCodeNumber();
            },
            ''
        );
    }

    public function removeOeNumber(OeNumber $oeNumber): void
    {
        $this->oeNumbers->removeElement($oeNumber);
    }

    public function getBrand(): ?BrandInterface
    {
        return $this->brand;
    }

    public function setBrand(?BrandInterface $brand): void
    {
        $this->brand = $brand;
    }

    public function getBrandNameIndex(): string
    {
        $brandName = $this->brand ? $this->brand->getName() : null;

        return $brandName ?: '';
    }

    public function getTecDocDocuments(): array
    {
        return $this->tecDocDocuments;
    }

    public function addTecDocDocument(TecDocDocument $document): void
    {
        $this->tecDocDocuments[] = $document;
    }

    public function hasVehicles(): bool
    {
        return $this->hasVehicles;
    }

    public function setHasVehicles(bool $hasVehicles): void
    {
        $this->hasVehicles = $hasVehicles;
    }

    public function getInternalBrand(): ?InternalBrand
    {
        return $this->internalBrand;
    }

    public function setInternalBrand(?InternalBrand $internalBrand): void
    {
        $this->internalBrand = $internalBrand;
    }

    public function getInternalCategory(): ?InternalCategory
    {
        return $this->internalCategory;
    }

    public function setInternalCategory(?InternalCategory $internalCategory): void
    {
        $this->internalCategory = $internalCategory;
    }

    public function replaceProductTaxon(ProductTaxonInterface $productTaxon): void
    {
        $this->productTaxons->clear();

        $this->addProductTaxon($productTaxon);
    }

    public function setTecDocName(string $name): void
    {
        $this->tecDocName = $name;
    }

    public function getName(): ?string
    {
        return $this->tecDocName ?: parent::getName();
    }

    public function addTecDocAttribute(AttributeValueInterface $attribute): void
    {
        $this->tecDocAttributes[] = $attribute;
    }

    public function getTecDocAttributes(): array
    {
        return $this->tecDocAttributes;
    }

    public function addTecDocImage(ImageInterface $image): void
    {
        $this->tecDocImages[] = $image;
    }

    public function getImages(): Collection
    {
        $images = $this->images->getValues();

        foreach ($this->tecDocImages as $image) {
            $images[] = $image;
        }

        return new ArrayCollection($images);
    }

    public function getImagesByType(string $type): Collection
    {
        return $this->getImages()->filter(
            static function (ImageInterface $image) use ($type) {
                return $type === $image->getType();
            }
        );
    }

    public function findVariantByCode(string $code): ?ProductVariantInterface
    {
        foreach ($this->variants as $variant) {
            if (\strcasecmp((string)$variant->getCode(), $code) === 0) {
                return $variant;
            }
        }

        return null;
    }

    public function findProductTaxonByCode(string $code): ?ProductTaxonInterface
    {
        foreach ($this->productTaxons as $productTaxon) {
            $taxon = $productTaxon->getTaxon();
            if ($taxon === null) {
                continue;
            }

            if (\strcasecmp((string)$taxon->getCode(), $code) === 0) {
                return $productTaxon;
            }
        }

        return null;
    }

    /**
     * @return Supplier|null
     */
    public function getSupplier(): ?Supplier
    {
        return $this->supplier;
    }

    /**
     * @param SupplierInterface|null $supplier
     * @return void
     */
    public function setSupplier(?SupplierInterface $supplier): void
    {
        $this->supplier = $supplier;
    }
}
