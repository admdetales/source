<?php

declare(strict_types=1);

namespace App\Entity\Product;

use Doctrine\ORM\Mapping as ORM;
use Sylius\Component\Resource\Model\ResourceInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Product\OeNumberRepository")
 * @ORM\Table(name="sylius_oenumber")
 */
class OeNumber implements ResourceInterface
{
    /**
     * @var int|null
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     * @ORM\Id()
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false)
     */
    private $codeNumber;

    /**
     * @var ProductInterface|null
     * @ORM\ManyToOne(targetEntity="Product", inversedBy="oeNumbers")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private $product;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $brandName;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodeNumber(): string
    {
        return $this->codeNumber ?? '';
    }

    public function setCodeNumber(?string $codeNumber): void
    {
        $this->codeNumber = $codeNumber ?? '';
    }

    public function getProduct(): ?ProductInterface
    {
        return $this->product;
    }

    public function setProduct(?ProductInterface $product): void
    {
        $this->product = $product;
    }

    public function getBrandName(): ?string
    {
        return $this->brandName;
    }

    public function setBrandName(?string $brandName): void
    {
        $this->brandName = $brandName;
    }
}
