<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Entity\Taxonomy;

use Doctrine\Common\Comparable;
use Doctrine\ORM\Mapping as ORM;
use Sylius\Component\Core\Model\Taxon as BaseTaxon;
use Sylius\Component\Taxonomy\Model\TaxonTranslationInterface;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="sylius_taxon")
 */
class Taxon extends BaseTaxon implements TaxonInterface, Comparable
{
    /**
     * @var array<int>
     *
     * @ORM\Column(type="json")
     */
    private $genericArticleIds = [];

    /**
     * @var Collection|InternalCategory[]
     * @ORM\OneToMany(targetEntity="App\Entity\Taxonomy\InternalCategory", mappedBy="taxon")
     */
    private $internalCategories;

    public function __construct()
    {
        parent::__construct();
        $this->internalCategories = new ArrayCollection();
    }


    /** @return array<int> */
    public function getGenericArticleIds(): array
    {
        return $this->genericArticleIds ?: [];
    }

    /**
     * @param array<int> $genericArticleIds
     */
    public function setGenericArticleIds(array $genericArticleIds): void
    {
        $this->genericArticleIds = $genericArticleIds;
    }

    /**
     * @return Collection|InternalCategory[]
     */
    public function getInternalCategories(): Collection
    {
        return $this->internalCategories;
    }

    public function addInternalCategory(InternalCategory $internalCategory): void
    {
        if (!$this->internalCategories->contains($internalCategory)) {
            $this->internalCategories->add($internalCategory);
            $internalCategory->setTaxon($this);
        }
    }

    public function removeInternalCategory(InternalCategory $internalCategory): void
    {
        if ($this->internalCategories->contains($internalCategory)) {
            $this->internalCategories->removeElement($internalCategory);
            $internalCategory->setTaxon(null);
        }
    }

    public function createTranslation(): TaxonTranslationInterface
    {
        return new TaxonTranslation();
    }

    /**
     * @inheritDoc
     */
    public function compareTo($other): int
    {
        return $this->code === $other->getCode() ? 0 : 1;
    }
}
