<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Entity\Supplier;

use App\Entity\Product\ProductInterface;
use Sylius\Component\Resource\Model\ResourceInterface;

interface SupplierInterface extends ResourceInterface
{
    /**
     * @return mixed
     */
    public function getProducts();

    /**
     * @param array $products
     * @return Supplier
     */
    public function setProducts(array $products): Supplier;

    /**
     * @param ProductInterface $product
     * @return Supplier
     */
    public function addProduct(ProductInterface $product): Supplier;

    /**
     * @return string|null
     */
    public function getName(): ?string;

    /**
     * @param string $amount
     * @return Supplier
     */
    public function setName(string $amount): Supplier;
}
