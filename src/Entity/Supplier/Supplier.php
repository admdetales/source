<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Entity\Supplier;

use App\Entity\Product\ProductInterface;
use App\Entity\Surcharge\SupplierSurchargePrice;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Selectable;
use Doctrine\ORM\Mapping as ORM;
use phpDocumentor\Reflection\Types\Collection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 */
class Supplier implements SupplierInterface
{
    /**
     * @var int|null
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Product\Product", mappedBy="supplier")
     */
    private $products;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var Collection<SupplierSurchargePrice>&Selectable<SupplierSurchargePrice>
     *
     * @ORM\OneToMany(
     *     targetEntity="App\Entity\Surcharge\SupplierSurchargePrice",
     *     mappedBy="supplier",
     *     cascade={"persist", "remove"},
     *     orphanRemoval=true
     * )
     */
    private $surchargePrices;

    public function __construct()
    {
        $this->surchargePrices = new ArrayCollection();
        $this->products = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return Supplier
     */
    public function setId(?int $id): Supplier
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return ArrayCollection|mixed
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param array $products
     * @return Supplier
     */
    public function setProducts(array $products): Supplier
    {
        $this->products = $products;
        return $this;
    }

    /**
     * @param ProductInterface $product
     * @return Supplier
     */
    public function addProduct(ProductInterface $product): Supplier
    {
        if (!$this->products->contains($product)) {
            $this->products->add($product);
            $product->setSupplier($this);
        }
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Supplier
     */
    public function setName(string $name): Supplier
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return ArrayCollection|Selectable|Collection
     */
    public function getSurchargePrices()
    {
        return $this->surchargePrices;
    }

    public function addSurchargePrice(SupplierSurchargePrice $supplierSurchargePrice): void
    {
        if (!$this->surchargePrices->contains($supplierSurchargePrice)) {
            $this->surchargePrices->add($supplierSurchargePrice);
            $supplierSurchargePrice->setSupplier($this);
        }
    }

    public function removeSurchargePrice(SupplierSurchargePrice $supplierSurchargePrice): void
    {
        if ($this->surchargePrices->contains($supplierSurchargePrice)) {
            $this->surchargePrices->removeElement($supplierSurchargePrice);
        }
    }
}
