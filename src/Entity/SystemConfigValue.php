<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class SystemConfigValue
{
    public const TYPE_MAINTENANCE = 'maintenance';
    public const TYPE_MAINTENANCE_WHITELIST = 'maintenance-whitelist';

    /**
     * @var int|null
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", unique=true)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $value;

    public function __construct(string $type, string $value)
    {
        $this->type  = $type;
        $this->value = $value;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function update(string $value): void
    {
        $this->value = $value;
    }
}
