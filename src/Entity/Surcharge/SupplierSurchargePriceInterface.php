<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Entity\Surcharge;

use App\Entity\Customer\CustomerInterface;
use App\Entity\Supplier\Supplier;
use App\Entity\Supplier\SupplierInterface;
use Sylius\Component\Resource\Model\ResourceInterface;

interface SupplierSurchargePriceInterface extends ResourceInterface
{
    /**
     * @return CustomerInterface|null
     */
    public function getCustomer(): ?CustomerInterface;

    /**
     * @param CustomerInterface $customer
     */
    public function setCustomer(CustomerInterface $customer): void;

    /**
     * @return SupplierInterface|null
     */
    public function getSupplier(): ?SupplierInterface;

    /**
     * @param Supplier $supplier
     */
    public function setSupplier(Supplier $supplier): void;

    /**
     * @return string|null
     */
    public function getAmount(): ?string;

    /**
     * @param string $amount
     */
    public function setAmount(string $amount): void;
}
