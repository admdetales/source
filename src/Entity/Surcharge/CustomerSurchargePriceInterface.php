<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Entity\Surcharge;

use App\Entity\Channel\ChannelPricingInterface;
use App\Entity\Customer\CustomerInterface;
use Sylius\Component\Resource\Model\ResourceInterface;

interface CustomerSurchargePriceInterface extends ResourceInterface
{
    /**
     * @return CustomerInterface|null
     */
    public function getCustomer(): ?CustomerInterface;

    /**
     * @param CustomerInterface $customer
     */
    public function setCustomer(CustomerInterface $customer): void;

    /**
     * @return ChannelPricingInterface|null
     */
    public function getChannelPricing(): ?ChannelPricingInterface;

    /**
     * @param ChannelPricingInterface $channelPricing
     */
    public function setChannelPricing(ChannelPricingInterface $channelPricing): void;

    /**
     * @return string|null
     */
    public function getAmount(): ?string;

    /**
     * @param string $amount
     */
    public function setAmount(string $amount): void;
}
