<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Entity\Surcharge;

use App\Entity\Customer\CustomerInterface;
use App\Entity\Supplier\Supplier;
use App\Entity\Supplier\SupplierInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Surcharge\SupplierSurchargePriceRepository")
 * @UniqueEntity(fields={"customer", "supplier"}, errorPath="customer")
 */
class SupplierSurchargePrice implements SupplierSurchargePriceInterface
{
    /**
     * @var int|null
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var CustomerInterface
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Customer\Customer")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=false)
     */
    private $customer;

    /**
     * @var SupplierInterface
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Supplier\Supplier", inversedBy="surchargePrices")
     * @ORM\JoinColumn(name="supplier_id", referencedColumnName="id", nullable=false)
     */
    private $supplier;

    /**
     * @var string
     *
     * @ORM\Column(type="decimal", precision=5, scale=2)
     * @Assert\NotBlank()
     * @Assert\Range(min=0, max=100)
     */
    private $amount;

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getCustomer(): ?CustomerInterface
    {
        return $this->customer;
    }

    /**
     * {@inheritdoc}
     */
    public function setCustomer(CustomerInterface $customer): void
    {
        $this->customer = $customer;
    }

    /**
     * {@inheritdoc}
     */
    public function getSupplier(): ?SupplierInterface
    {
        return $this->supplier;
    }

    /**
     * {@inheritdoc}
     */
    public function setSupplier(Supplier $supplier): void
    {
        $this->supplier = $supplier;
    }

    /**
     * {@inheritdoc}
     */
    public function getAmount(): ?string
    {
        return $this->amount;
    }

    /**
     * {@inheritdoc}
     */
    public function setAmount(string $amount): void
    {
        $this->amount = $amount;
    }
}
