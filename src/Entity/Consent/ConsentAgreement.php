<?php

declare(strict_types=1);

namespace App\Entity\Consent;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class ConsentAgreement
{
    /**
     * @var int|null
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var Consent
     *
     * @ORM\ManyToOne(targetEntity="Consent")
     */
    private $consent;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $agreed;

    /**
     * @var \DateTimeInterface
     *
     * @ORM\Column(type="datetime")
     */
    private $datetime;

    public function __construct()
    {
        $this->datetime = new \DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getConsent(): Consent
    {
        return $this->consent;
    }

    public function setConsent(Consent $consent): void
    {
        $this->consent = $consent;
    }

    public function isAgreed(): bool
    {
        return $this->agreed;
    }

    public function setAgreed(bool $agreed): void
    {
        $this->agreed = $agreed;
    }

    public function getDatetime(): \DateTimeInterface
    {
        return $this->datetime;
    }

    public function setDatetime(\DateTimeInterface $datetime): void
    {
        $this->datetime = $datetime;
    }
}
