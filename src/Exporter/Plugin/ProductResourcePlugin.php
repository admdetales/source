<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Exporter\Plugin;

use App\Entity\Channel\ChannelPricing;
use Doctrine\ORM\EntityManagerInterface;
use FriendsOfSylius\SyliusImportExportPlugin\Exporter\Plugin\ResourcePlugin;
use Sylius\Component\Core\Model\ProductInterface;
use Sylius\Component\Core\Model\ProductVariantInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;

class ProductResourcePlugin extends ResourcePlugin
{
    /** @var RepositoryInterface */
    private $channelPricingRepository;

    /** @var RepositoryInterface */
    private $productVariantRepository;

    /**
     * @param RepositoryInterface $productRepository
     * @param PropertyAccessorInterface $propertyAccessor
     * @param EntityManagerInterface $entityManager
     * @param RepositoryInterface $channelPricingRepository
     * @param RepositoryInterface $productVariantRepository
     */
    public function __construct(
        RepositoryInterface $productRepository,
        PropertyAccessorInterface $propertyAccessor,
        EntityManagerInterface $entityManager,
        RepositoryInterface $channelPricingRepository,
        RepositoryInterface $productVariantRepository
    ) {
        parent::__construct($productRepository, $propertyAccessor, $entityManager);
        $this->channelPricingRepository = $channelPricingRepository;
        $this->productVariantRepository = $productVariantRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function init(array $idsToExport): void
    {
        parent::init($idsToExport);

        /** @var ProductInterface $resource */
        foreach ($this->resources as $resource) {
            $this->addTranslationData($resource);
            $this->addTaxonData($resource);
            $this->addAttributeData($resource);
            $this->addChannelData($resource);
            $this->addPriceData($resource);
        }
    }

    /**
     * @param ProductInterface $resource
     */
    private function addTranslationData(ProductInterface $resource): void
    {
        $translation = $resource->getTranslation();

        $this->addDataForResource($resource, 'Name', $translation->getName());
        $this->addDataForResource($resource, 'Locale', $translation->getLocale());
        $this->addDataForResource($resource, 'Slug', $translation->getSlug());
        $this->addDataForResource($resource, 'Description', $translation->getDescription());
        $this->addDataForResource($resource, 'Short description', $translation->getShortDescription());
        $this->addDataForResource($resource, 'Meta description', $translation->getMetaDescription());
        $this->addDataForResource($resource, 'Meta keywords', $translation->getMetaKeywords());
    }

    /**
     * @param ProductInterface $resource
     */
    private function addTaxonData(ProductInterface $resource): void
    {
        $mainCategory = '';
        $mainCategoryCode = '';

        $mainTaxon = $resource->getMainTaxon();
        if (null !== $mainTaxon) {
            $mainCategory = $mainTaxon->getName();
            $mainCategoryCode = $mainTaxon->getCode();
        }

        $this->addDataForResource($resource, 'Main category', $mainCategory);
        $this->addDataForResource($resource, 'Main category code', $mainCategoryCode);
    }

    /**
     * @param ProductInterface $resource
     */
    private function addChannelData(ProductInterface $resource): void
    {
        $channelSlug = '';

        $channels = $resource->getChannels();
        foreach ($channels as $channel) {
            $channelSlug .= $channel->getCode() . '|';
        }

        $channelSlug = \rtrim($channelSlug, '|');

        $this->addDataForResource($resource, 'Channels', $channelSlug);
    }

    /**
     * @param ProductInterface $resource
     */
    private function addAttributeData(ProductInterface $resource): void
    {
        $attributes = $resource->getAttributes();

        foreach ($attributes as $attribute) {
            $this->addDataForResource($resource, $attribute->getCode(), $attribute->getValue());
        }
    }

    /**
     * @param ProductInterface $resource
     */
    private function addPriceData(ProductInterface $resource): void
    {
        /** @var ProductVariantInterface|null $productVariant */
        $productVariant = $this->productVariantRepository->findOneBy(['code' => $resource->getCode()]);
        if ($productVariant === null) {
            return;
        }

        $channels = $resource->getChannels();
        foreach ($channels as $channel) {
            /** @var ChannelPricing|null $channelPricing */
            $channelPricing = $this->channelPricingRepository->findOneBy(
                [
                    'channelCode' => $channel->getCode(),
                    'productVariant' => $productVariant,
                ]
            );

            $originalPriceInCents = $channelPricing ? $channelPricing->getOriginalPrice() : '';
            $priceInCents = $channelPricing ? $channelPricing->getPrice() : '';

            $this->addDataForResource($resource, 'Original price in cents', $originalPriceInCents);
            $this->addDataForResource($resource, 'Price in cents', $priceInCents);
        }
    }
}
