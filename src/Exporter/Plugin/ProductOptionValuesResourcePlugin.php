<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Exporter\Plugin;

use Doctrine\ORM\EntityManagerInterface;
use FriendsOfSylius\SyliusImportExportPlugin\Exporter\Plugin\ResourcePlugin;
use Sylius\Component\Product\Model\ProductOptionValueInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;

class ProductOptionValuesResourcePlugin extends ResourcePlugin
{
    /**
     * @param RepositoryInterface $productOptionValueRepository
     * @param PropertyAccessorInterface $propertyAccessor
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        RepositoryInterface $productOptionValueRepository,
        PropertyAccessorInterface $propertyAccessor,
        EntityManagerInterface $entityManager
    ) {
        parent::__construct($productOptionValueRepository, $propertyAccessor, $entityManager);
    }

    /**
     * {@inheritdoc}
     */
    public function init(array $idsToExport): void
    {
        parent::init($idsToExport);

        /** @var ProductOptionValueInterface $resource */
        foreach ($this->resources as $resource) {
            $this->addAttributeData($resource);
            $this->addTranslationData($resource);
        }
    }

    /**
     * @param ProductOptionValueInterface $resource
     */
    private function addAttributeData(ProductOptionValueInterface $resource): void
    {
        $this->addDataForResource($resource, 'Attribute code', $resource->getOptionCode());
    }

    /**
     * @param ProductOptionValueInterface $resource
     */
    private function addTranslationData(ProductOptionValueInterface $resource): void
    {
        $this->addDataForResource($resource, 'Value', $resource->getValue());
    }
}
