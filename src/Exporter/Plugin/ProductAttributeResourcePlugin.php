<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Exporter\Plugin;

use App\Constant\ImportExport;
use Doctrine\ORM\EntityManagerInterface;
use FriendsOfSylius\SyliusImportExportPlugin\Exporter\Plugin\ResourcePlugin;
use Sylius\Component\Product\Model\ProductAttributeInterface;
use Sylius\Component\Product\Model\ProductOptionValueInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;

class ProductAttributeResourcePlugin extends ResourcePlugin
{
    /**
     * @param RepositoryInterface $productAttributeRepository
     * @param PropertyAccessorInterface $propertyAccessor
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        RepositoryInterface $productAttributeRepository,
        PropertyAccessorInterface $propertyAccessor,
        EntityManagerInterface $entityManager
    ) {
        parent::__construct($productAttributeRepository, $propertyAccessor, $entityManager);
    }

    /**
     * {@inheritdoc}
     */
    public function init(array $idsToExport): void
    {
        parent::init($idsToExport);

        /** @var ProductAttributeInterface $resource */
        foreach ($this->resources as $resource) {
            $this->convertArrayDataToString($resource);
            $this->addTranslationData($resource);
        }
    }

    /**
     * @param ProductAttributeInterface $resource
     */
    private function convertArrayDataToString(ProductAttributeInterface $resource): void
    {
        $this->addDataForResource(
            $resource,
            'Configuration',
            implode(ImportExport::COLLECTION_DELIMITER, $resource->getConfiguration())
        );
    }

    /**
     * @param ProductAttributeInterface $resource
     */
    private function addTranslationData(ProductAttributeInterface $resource): void
    {
        $this->addDataForResource($resource, 'Name', $resource->getName());
    }
}
