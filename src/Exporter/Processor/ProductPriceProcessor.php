<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Exporter\Processor;

use App\Exporter\Reader\ReaderInterface;
use App\Repository\Channel\ChannelRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use FriendsOfSylius\SyliusImportExportPlugin\Processor\ResourceProcessorInterface;
use Sylius\Component\Core\Model\ChannelPricingInterface;
use Sylius\Component\Product\Model\ProductInterface;
use Sylius\Component\Core\Model\ProductVariantInterface;
use Sylius\Component\Resource\Factory\FactoryInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;

class ProductPriceProcessor implements ResourceProcessorInterface
{
    /** @var RepositoryInterface */
    private $channelPricingRepository;
    /** @var RepositoryInterface */
    private $productVariantRepository;
    /** @var FactoryInterface */
    private $productVariantFactory;
    /** @var RepositoryInterface */
    private $productRepository;
    /** @var FactoryInterface */
    private $productFactory;
    /** @var RepositoryInterface */
    private $productOptionValueRepository;
    /** @var ChannelRepositoryInterface */
    private $channelRepository;
    /** @var FactoryInterface */
    private $channelPricingFactory;
    /** @var EntityManagerInterface */
    private $entityManager;
    /** @var ReaderInterface */
    private $priceReader;
    /** @var string|null */
    private $productProviderCode;
    /** @var string|null */
    private $productUniqueCode;
    /** @var int|null */
    private $productPrice;
    /** @var int|null */
    private $productOriginalPrice;
    /** @var string[]|null */
    private $channelsCode;

    public function __construct(
        RepositoryInterface $productVariantRepository,
        FactoryInterface $productVariantFactory,
        RepositoryInterface $productRepository,
        FactoryInterface $productFactory,
        RepositoryInterface $productOptionValueRepository,
        ChannelRepositoryInterface $channelRepository,
        RepositoryInterface $channelPricingRepository,
        FactoryInterface $channelPricingFactory,
        EntityManagerInterface $entityManager,
        ReaderInterface $priceReader
    ) {
        $this->productVariantRepository = $productVariantRepository;
        $this->productVariantFactory = $productVariantFactory;
        $this->productRepository = $productRepository;
        $this->productFactory = $productFactory;
        $this->productOptionValueRepository = $productOptionValueRepository;
        $this->channelRepository = $channelRepository;
        $this->channelPricingFactory = $channelPricingFactory;
        $this->entityManager = $entityManager;
        $this->priceReader = $priceReader;
        $this->channelPricingRepository = $channelPricingRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function process(array $data): void
    {
        $productData = $this->priceReader->getData($data);
        $this->productProviderCode = $productData['productProviderCode'];
        $this->productUniqueCode = $this->priceReader->getProductProviderName() . '-' . $this->productProviderCode;
        $product = $this->getProduct();

        if ($product === null) {
            return;
        }

        $productVariants = $this->getProductVariants($product);
        $this->productPrice = $productData['price'] ?? 0;
        $this->productOriginalPrice = $productData['originalPrice'] ?? 0;
        $channelsCode = $this->getChannelsCode();

        foreach ($productVariants as $productVariant) {
            foreach ($channelsCode as $channelCode) {
                $channelPricing = $this->channelPricingRepository->findOneBy(
                    [
                        'channelCode' => $channelCode,
                        'productVariant' => $productVariant,
                    ]
                );

                if (null === $channelPricing) {
                    /** @var ChannelPricingInterface $channelPricing */
                    $channelPricing = $this->channelPricingFactory->createNew();
                    $channelPricing->setChannelCode($channelCode);
                    $productVariant->addChannelPricing($channelPricing);

                    $this->entityManager->persist($channelPricing);
                }

                $channelPricing->setOriginalPrice((int)$this->productOriginalPrice);
                $channelPricing->setPrice((int)$this->productPrice);
            }
        }

        $this->entityManager->flush();
    }

    private function getProduct(): ?ProductInterface
    {
        /** @var ProductInterface|null $product */
        $product = $this->productRepository->findOneBy(['code' => $this->productUniqueCode]);

        return $product;
    }

    /**
     * @param ProductInterface $product
     * @return ProductVariantInterface[]
     */
    private function getProductVariants(ProductInterface $product): array
    {
        /** @var ProductVariantInterface[] $productVariant */
        $productVariants = $this->productVariantRepository->findBy(['product' => $product]);

        return $productVariants;
    }

    /**
     * @return string[]
     */
    private function getChannelsCode(): array
    {
        if ($this->channelsCode === null) {
            $this->channelsCode = $this->channelRepository->getChannelsCode();
        }

        return $this->channelsCode;
    }
}
