<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Exporter\Processor;

use App\Constant\ImportExport;
use FriendsOfSylius\SyliusImportExportPlugin\Processor\ResourceProcessorInterface;
use Sylius\Component\Product\Model\ProductAttributeInterface;
use Sylius\Component\Product\Model\ProductOptionInterface;
use Sylius\Component\Resource\Factory\FactoryInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Webmozart\Assert\Assert;

class ProductAttributeProcessor implements ResourceProcessorInterface
{
    /** @var RepositoryInterface */
    private $productAttributeRepository;

    /** @var FactoryInterface */
    private $productAttributeFactory;

    /** @var RepositoryInterface */
    private $productOptionRepository;

    /** @var FactoryInterface */
    private $productOptionFactory;

    /**
     * @param RepositoryInterface $productAttributeRepository
     * @param RepositoryInterface $productOptionRepository
     * @param FactoryInterface $productAttributeFactory
     * @param FactoryInterface $productOptionFactory
     */
    public function __construct(
        RepositoryInterface $productAttributeRepository,
        RepositoryInterface $productOptionRepository,
        FactoryInterface $productAttributeFactory,
        FactoryInterface $productOptionFactory
    ) {
        $this->productAttributeRepository = $productAttributeRepository;
        $this->productAttributeFactory = $productAttributeFactory;
        $this->productOptionRepository = $productOptionRepository;
        $this->productOptionFactory = $productOptionFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function process(array $data): void
    {
        if ($data['Type'] === ImportExport::OPTION_ATTRIBUTE_TYPE) {
            $this->processProductOption($data);

            return;
        }

        $this->processProductAttribute($data);
    }

    /**
     * @param array $data
     */
    private function processProductOption(array $data): void
    {
        $errorMessage = \sprintf(
            'Wrong data structure provided for %s. %s',
            self::class,
            \implode(',', \array_keys($data))
        );
        Assert::keyExists($data, 'Code', $errorMessage);
        Assert::keyExists($data, 'Name', $errorMessage);

        $productOption = $this->getProductOption($data['Code']);

        $productOption->getTranslation(ImportExport::DEFAULT_LOCALE)->setName($data['Name']);

        $this->productOptionRepository->add($productOption);
    }

    /**
     * @param array $data
     */
    private function processProductAttribute(array $data): void
    {
        $errorMessage = \sprintf(
            'Wrong data structure provided for %s. %s',
            self::class,
            \implode(',', \array_keys($data))
        );
        Assert::keyExists($data, 'Code', $errorMessage);
        Assert::keyExists($data, 'Type', $errorMessage);
        Assert::keyExists($data, 'Name', $errorMessage);
        Assert::keyExists($data, 'Configuration', $errorMessage);

        $productAttribute = $this->getProductAttribute($data['Code']);

        // TODO: validate available types.
        $productAttribute->setType($data['Type']);
        $productAttribute->setStorageType($data['Type']);
        $productAttribute->getTranslation(ImportExport::DEFAULT_LOCALE)->setName($data['Name']);

        // TODO: Find solution to add key for configuration which may be different for each type
        $productAttribute->setConfiguration(explode(ImportExport::COLLECTION_DELIMITER, $data['Configuration']));

        $this->productAttributeRepository->add($productAttribute);
    }

    /**
     * @param string $code
     * @return ProductOptionInterface
     */
    private function getProductOption(string $code): ProductOptionInterface
    {
        /** @var ProductOptionInterface|null $productOption */
        $productOption = $this->productOptionRepository->findOneBy(['code' => $code]);

        if (null === $productOption) {
            /** @var ProductOptionInterface $productOption */
            $productOption = $this->productOptionFactory->createNew();
            $productOption->setCode($code);
        }

        return $productOption;
    }

    /**
     * @param string $code
     * @return ProductAttributeInterface
     */
    private function getProductAttribute(string $code): ProductAttributeInterface
    {
        /** @var ProductAttributeInterface|null $productAttribute */
        $productAttribute = $this->productAttributeRepository->findOneBy(['code' => $code]);

        if (null === $productAttribute) {
            /** @var ProductAttributeInterface $productAttribute */
            $productAttribute = $this->productAttributeFactory->createNew();
            $productAttribute->setCode($code);
        }

        return $productAttribute;
    }
}
