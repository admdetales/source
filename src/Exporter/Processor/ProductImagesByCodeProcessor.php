<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Exporter\Processor;

use App\Constant\ImportExport;
use App\Entity\Product\ProductImage;
use FriendsOfSylius\SyliusImportExportPlugin\Processor\MetadataValidatorInterface;
use Sylius\Component\Core\Model\ProductInterface;
use Sylius\Component\Core\Uploader\ImageUploaderInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Symfony\Component\HttpFoundation\File\File;

class ProductImagesByCodeProcessor extends ProductImagesProcessor
{
    /** @var string */
    private $filenamePattern;

    /**
     * @param RepositoryInterface $productRepository
     * @param ImageUploaderInterface $imageUploader
     * @param MetadataValidatorInterface $metadataValidator
     * @param array $headerKeys
     * @param string $imagesPath
     * @param string $filenamePattern
     */
    public function __construct(
        RepositoryInterface $productRepository,
        ImageUploaderInterface $imageUploader,
        MetadataValidatorInterface $metadataValidator,
        array $headerKeys,
        string $filenamePattern
    ) {
        parent::__construct($productRepository, $imageUploader, $metadataValidator, $headerKeys);

        $this->filenamePattern = $filenamePattern;
    }

    /**
     * @param ProductInterface $product
     * @param array $data
     */
    protected function uploadImages(ProductInterface $product, array $data): void
    {
        $tmpImg = tempnam(sys_get_temp_dir(), 'product_image');
        $ftpConnection = ftp_connect(getenv('CDN_FTP_HOST'));
        ftp_login($ftpConnection, getenv('CDN_FTP_USERNAME'), getenv('CDN_FTP_PASSWORD'));
        ftp_set_option($ftpConnection, FTP_USEPASVADDRESS, false);
        ftp_pasv($ftpConnection, true);

        $files = ftp_nlist($ftpConnection, getenv('CDN_FTP_IMAGES_PATH'));
        $imagePaths = preg_grep(
            str_replace('$code', $product->getCode(), $this->filenamePattern),
            $files
        );
        $imgCount = 1;

        foreach ($imagePaths as $key => $imagePath) {
            ftp_get($ftpConnection, $tmpImg, $imagePath);

            $productImage = new ProductImage();
            $productImage->setFile(new File($tmpImg, false));
            $this->imageUploader->upload($productImage);
            $productImage->setType(ImportExport::DEFAULT_IMAGE_COLUMN_PREFIX . $imgCount);

            $product->addImage($productImage);
            ++$imgCount;
        }

        unlink($tmpImg);
        ftp_close($ftpConnection);
    }
}
