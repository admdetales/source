<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace App\Exporter\Processor;

use App\Entity\Channel\Channel;
use App\Exporter\Reader\ReaderInterface;
use Doctrine\ORM\EntityManagerInterface;
use FriendsOfSylius\SyliusImportExportPlugin\Processor\ResourceProcessorInterface;
use Sylius\Component\Core\Model\ChannelPricingInterface;
use Sylius\Component\Core\Model\ProductVariantInterface;
use Sylius\Component\Product\Model\ProductInterface;
use Sylius\Component\Resource\Factory\FactoryInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;

class ProductQuantityProcessor implements ResourceProcessorInterface
{
    /** @var ReaderInterface */
    private $quantityReader;
    /** @var RepositoryInterface */
    private $productRepository;
    /** @var RepositoryInterface */
    private $productVariantRepository;
    /** @var RepositoryInterface */
    private $channelRepository;
    /** @var FactoryInterface */
    private $channelPricingFactory;
    /** @var FactoryInterface */
    private $productVariantFactory;
    /** @var EntityManagerInterface */
    private $entityManager;

    /**
     * @param ReaderInterface           $quantityReader
     * @param RepositoryInterface       $productRepository
     * @param RepositoryInterface       $productVariantRepository
     * @param RepositoryInterface       $channelRepository
     * @param FactoryInterface          $productVariantFactory
     * @param FactoryInterface          $channelPricingFactory
     * @param EntityManagerInterface    $entityManager
     */
    public function __construct(
        ReaderInterface $quantityReader,
        RepositoryInterface $productRepository,
        RepositoryInterface $productVariantRepository,
        RepositoryInterface $channelRepository,
        FactoryInterface $productVariantFactory,
        FactoryInterface $channelPricingFactory,
        EntityManagerInterface $entityManager
    ) {
        $this->quantityReader           = $quantityReader;
        $this->productRepository        = $productRepository;
        $this->productVariantRepository = $productVariantRepository;
        $this->channelRepository        = $channelRepository;
        $this->productVariantFactory    = $productVariantFactory;
        $this->channelPricingFactory    = $channelPricingFactory;
        $this->entityManager            = $entityManager;
    }

    /**
     * @param string[] $data
     */
    public function process(array $data): void
    {
        $productData = $this->quantityReader->getData($data);
        $productProviderCode = $productData['productProviderCode'];
        $productUniqueCode = $this->quantityReader->getProductProviderName() . '-' . $productProviderCode;
        $product = $this->getProduct($productUniqueCode);

        if ($product === null) {
            return;
        }

        $productStockVariant = $this->getProductVariant(
            $productUniqueCode . '-' . $productData['productWarehouse']
        );
        $productStockVariant->setProduct($product);
        $productStockVariant->setName($productData['productWarehouse']);
        $productStockVariant->setOnHand(
            $productData['productQuantity'] === '>10' ? 10 : $productData['productQuantity']
        );
        /** @var Channel[] $channels */
        $channels = $this->channelRepository->findAll();

        foreach ($channels as $channel) {
            $channelPricing = $productStockVariant->getChannelPricingForChannel($channel);

            if (null === $channelPricing) {
                /** @var ChannelPricingInterface $channelPricing */
                $channelPricing = $this->channelPricingFactory->createNew();
                $channelPricing->setChannelCode($channel->getCode());
                $productStockVariant->addChannelPricing($channelPricing);
            }

            $channelPricing->setPrice(0);
            $channelPricing->setOriginalPrice(0);
        }

        $this->productVariantRepository->add($productStockVariant);
    }

    private function getProductVariant(string $code): ProductVariantInterface
    {
        /** @var ProductVariantInterface|null $productVariant */
        $productVariant = $this->productVariantRepository->findOneBy(['code' => $code]);

        if (null === $productVariant) {
            /** @var ProductVariantInterface $productVariant */
            $productVariant = $this->productVariantFactory->createNew();
            $productVariant->setCode($code);
        }

        return $productVariant;
    }

    private function getProduct(string $code): ?ProductInterface
    {
        /** @var ProductInterface|null $product */
        $product = $this->productRepository->findOneBy(['code' => $code]);

        return $product;
    }
}
