<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Exporter\Processor;

use App\Constant\ImportExport;
use App\Entity\Taxonomy\InternalCategory;
use App\Repository\Taxon\InternalCategoryRepository;
use App\Repository\Taxon\TaxonRepository;
use Doctrine\ORM\EntityManagerInterface;
use FriendsOfSylius\SyliusImportExportPlugin\Processor\ResourceProcessorInterface;
use Sylius\Component\Resource\Factory\FactoryInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Sylius\Component\Taxonomy\Factory\TaxonFactoryInterface;
use App\Entity\Taxonomy\TaxonInterface;
use Webmozart\Assert\Assert;

class TaxonProcessor implements ResourceProcessorInterface
{
    public const CATEGORY_CODE_HEADER = 'CODE';
    public const CATEGORY_PARENT_CODE_HEADER = 'PARENT_CODE';
    public const CATEGORY_TITLE_HEADER = 'TITLE';
    public const CATEGORY_SLUG_HEADER = 'SLUG';
    public const CATEGORY_TITLE_HEADER_RU = 'TITLE_RU';
    public const CATEGORY_SLUG_HEADER_RU = 'SLUG_RU';
    public const CATEGORY_TITLE_HEADER_EN = 'TITLE_EN';
    public const CATEGORY_SLUG_HEADER_EN = 'SLUG_EN';
    public const CATEGORY_LEVEL_HEADER = 'LEVEL';
    public const CATEGORY_GENERIC_ARTICLE_IDS_HEADER = 'GENERIC_ARTICLE_IDS';

    /** @var RepositoryInterface|TaxonRepository */
    private $taxonRepository;
    /** @var FactoryInterface|TaxonFactoryInterface */
    private $taxonFactory;
    /** @var InternalCategoryRepository */
    private $internalCategoryRepository;
    /** @var EntityManagerInterface */
    private $entityManager;
    /** @var string|null */
    private $categoryCode;
    /** @var string|null */
    private $categoryParentCode;
    /** @var string|null */
    private $categoryTitle;
    /** @var string|null */
    private $categorySlug;
    /** @var string|null */
    private $categoryTitleRu;
    /** @var string|null */
    private $categorySlugRu;
    /** @var string|null */
    private $categoryTitleEn;
    /** @var string|null */
    private $categorySlugEn;
    /** @var string|null */
    private $categoryLevel;
    /** @var array<int>|null */
    private $categoryGenericArticleIds;

    public function __construct(
        RepositoryInterface $taxonRepository,
        FactoryInterface $taxonFactory,
        InternalCategoryRepository $internalCategoryRepository,
        EntityManagerInterface $entityManager
    ) {
        $this->taxonRepository = $taxonRepository;
        $this->taxonFactory = $taxonFactory;
        $this->internalCategoryRepository = $internalCategoryRepository;
        $this->entityManager = $entityManager;
    }

    /**
     * {@inheritdoc}
     */
    public function process(array $data): void
    {
        $this->getData($data);
        /** @var TaxonInterface|null $taxon */
        $taxon = $this->taxonRepository->findOneBy(['code' => $this->categoryCode]);
        /** @var InternalCategory|null $internalCategory */
        $internalCategory = $this->internalCategoryRepository->findOneBy(['name' => $this->categoryTitle]);

        if ($internalCategory === null) {
            $internalCategory = $this->createInternalCategory();
            $this->entityManager->persist($internalCategory);
            $this->entityManager->flush();
        }

        if ($taxon === null) {
            /** @var TaxonInterface $taxon */
            $taxon = $this->taxonFactory->createNew();
        }

        $taxon = $this->updateTaxon($taxon);
        $taxon->addInternalCategory($internalCategory);
        $this->taxonRepository->add($taxon);
    }

    private function createInternalCategory(): InternalCategory
    {
        $internalCategory = new InternalCategory();
        $internalCategory->setName($this->categoryTitle);

        return $internalCategory;
    }

    private function updateTaxon(TaxonInterface $taxon): TaxonInterface
    {
        $taxon->setCurrentLocale(ImportExport::DEFAULT_LOCALE);
        $taxon->setFallbackLocale(ImportExport::DEFAULT_LOCALE);
        $taxon->setName($this->categoryTitle);
        $taxon->setSlug($this->categorySlug);
        $taxon->setCurrentLocale(ImportExport::LOCALE_RU);
        $taxon->setFallbackLocale(ImportExport::LOCALE_RU);
        $taxon->setName($this->categoryTitleRu);
        $taxon->setSlug($this->categorySlugRu);
        $taxon->setCurrentLocale(ImportExport::LOCALE_EN);
        $taxon->setFallbackLocale(ImportExport::LOCALE_EN);
        $taxon->setName($this->categoryTitleEn);
        $taxon->setSlug($this->categorySlugEn);
        $parent = null;

        if ($this->categoryLevel !== TaxonInterface::TREE_CODE) {
            /** @var TaxonInterface|null $parent */
            $parent = $this->taxonRepository->findOneBy(['code' => $this->categoryParentCode]);
        }

        $taxon->setParent($parent);
        $taxon->setCode($this->categoryCode);
        $taxon->setGenericArticleIds($this->categoryGenericArticleIds);

        return $taxon;
    }

    /**
     * @param array<string> $data
     */
    private function getData(array $data): void
    {
        $errorMessage = \sprintf(
            'Wrong data structure provided for %s. %s',
            self::class,
            \implode(',', \array_keys($data))
        );
        Assert::keyExists($data, self::CATEGORY_CODE_HEADER, $errorMessage);
        Assert::keyExists($data, self::CATEGORY_PARENT_CODE_HEADER, $errorMessage);
        Assert::keyExists($data, self::CATEGORY_TITLE_HEADER, $errorMessage);
        Assert::keyExists($data, self::CATEGORY_SLUG_HEADER, $errorMessage);
        Assert::keyExists($data, self::CATEGORY_TITLE_HEADER_RU, $errorMessage);
        Assert::keyExists($data, self::CATEGORY_SLUG_HEADER_RU, $errorMessage);
        Assert::keyExists($data, self::CATEGORY_TITLE_HEADER_EN, $errorMessage);
        Assert::keyExists($data, self::CATEGORY_SLUG_HEADER_EN, $errorMessage);
        Assert::keyExists($data, self::CATEGORY_LEVEL_HEADER, $errorMessage);
        Assert::keyExists($data, self::CATEGORY_GENERIC_ARTICLE_IDS_HEADER, $errorMessage);

        $this->categoryCode = $data[self::CATEGORY_CODE_HEADER];
        $this->categoryParentCode = $data[self::CATEGORY_PARENT_CODE_HEADER];
        $this->categoryTitle = $data[self::CATEGORY_TITLE_HEADER];
        $this->categorySlug = $data[self::CATEGORY_SLUG_HEADER];
        $this->categoryTitleRu = $data[self::CATEGORY_TITLE_HEADER_RU];
        $this->categorySlugRu = $data[self::CATEGORY_SLUG_HEADER_RU];
        $this->categoryTitleEn = $data[self::CATEGORY_TITLE_HEADER_EN];
        $this->categorySlugEn = $data[self::CATEGORY_SLUG_HEADER_EN];
        $this->categoryLevel = $data[self::CATEGORY_LEVEL_HEADER];
        $this->categoryGenericArticleIds = [];

        if ($data[self::CATEGORY_GENERIC_ARTICLE_IDS_HEADER] !== '') {
            $this->categoryGenericArticleIds = array_map(
                function ($genericArticleId) {
                    return (int) $genericArticleId;
                },
                explode(", ", $data[self::CATEGORY_GENERIC_ARTICLE_IDS_HEADER])
            );
        }
    }
}
