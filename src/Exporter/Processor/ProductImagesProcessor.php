<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Exporter\Processor;

use App\Entity\Product\Product;
use App\Entity\Product\ProductImage;
use FriendsOfSylius\SyliusImportExportPlugin\Processor\MetadataValidatorInterface;
use FriendsOfSylius\SyliusImportExportPlugin\Processor\ResourceProcessorInterface;
use Sylius\Component\Core\Model\ProductInterface;
use Sylius\Component\Core\Repository\ProductRepositoryInterface;
use Sylius\Component\Core\Uploader\ImageUploaderInterface;
use Sylius\Component\Resource\Factory\FactoryInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Symfony\Component\HttpFoundation\File\File;
use Webmozart\Assert\Assert;

class ProductImagesProcessor implements ResourceProcessorInterface
{
    /** @var ProductRepositoryInterface */
    protected $productRepository;
    /** @var FactoryInterface */
    protected $imageUploader;
    /** @var MetadataValidatorInterface */
    protected $metadataValidator;
    /** @var array */
    protected $headerKeys;
    /** @var array */
    private $imageKeys;

    /**
     * @param RepositoryInterface $productRepository
     * @param ImageUploaderInterface $imageUploader
     * @param MetadataValidatorInterface $metadataValidator
     * @param array $headerKeys
     */
    public function __construct(
        RepositoryInterface $productRepository,
        ImageUploaderInterface $imageUploader,
        MetadataValidatorInterface $metadataValidator,
        array $headerKeys
    ) {
        $this->productRepository = $productRepository;
        $this->imageUploader = $imageUploader;
        $this->metadataValidator = $metadataValidator;
        $this->headerKeys = $headerKeys;
    }

    /**
     * {@inheritdoc}
     */
    public function process(array $data): void
    {
        $errorMessage = \sprintf(
            'Wrong data structure provided for %s. %s',
            self::class,
            \implode(',', \array_keys($data))
        );
        Assert::keyExists($data, 'Code', $errorMessage);

        $this->metadataValidator->validateHeaders($this->headerKeys, $data);
        $this->imageKeys = array_diff(array_keys($data), $this->headerKeys);

        $product = $this->getProduct((string)$data['Code']);

        $this->removeCurrentImages($product);
        $this->uploadImages($product, $data);
    }

    /**
     * @param ProductInterface $product
     */
    protected function removeCurrentImages(ProductInterface $product): void
    {
        $images = $product->getImages();

        foreach ($images as $image) {
            $product->removeImage($image);
        }
    }

    /**
     * @param ProductInterface $product
     * @param array $data
     */
    protected function uploadImages(ProductInterface $product, array $data): void
    {
        foreach ($this->imageKeys as $imageType) {
            $imagePath = $data[$imageType] ?? null;

            if (empty($imagePath)) {
                continue;
            }

            if (preg_match('/^https?:\/\//', $imagePath)) {
                $productImage = $this->uploadProductImageFromUrl($imagePath);
            } else {
                $productImage = $this->uploadProductImageFromFtp($imagePath);
            }

            if (null === $productImage) {
                continue;
            }

            $productImage->setType($imageType);

            $product->addImage($productImage);
        }
    }

    /**
     * @param string $code
     *
     * @return ProductInterface
     */
    protected function getProduct(string $code): ProductInterface
    {
        $product = $this->productRepository->findOneByCode($code);

        if (null === $product) {
            throw new \Exception(sprintf('Product with code "%s" not found', $code));
        }

        return $product;
    }

    /**
     * @param string $url
     *
     * @return ProductImage|null
     */
    private function uploadProductImageFromUrl(string $url): ?ProductImage
    {
        try {
            $tmpImg = tempnam(sys_get_temp_dir(), 'product_image');
            $handle = fopen($tmpImg, 'wb');
            fwrite($handle, file_get_contents($url));
            fclose($handle);

            $image = new ProductImage();
            $image->setFile(new File($tmpImg, false));
            $this->imageUploader->upload($image);

            unlink($tmpImg);

            return $image;
        } catch (\Throwable $e) {
            // TODO: Log error?
            return null;
        }
    }

    /**
     * @param string $path
     *
     * @return ProductImage|null
     */
    private function uploadProductImageFromFtp(string $path): ?ProductImage
    {
        try {
            $tmpImg = tempnam(sys_get_temp_dir(), 'product_image');
            $ftpConnection = ftp_connect(getenv('CDN_FTP_HOST'));
            ftp_login($ftpConnection, getenv('CDN_FTP_USERNAME'), getenv('CDN_FTP_PASSWORD'));
            ftp_set_option($ftpConnection, FTP_USEPASVADDRESS, false);
            ftp_pasv($ftpConnection, true);
            ftp_get($ftpConnection, $tmpImg, $path);
            ftp_close($ftpConnection);

            $image = new ProductImage();
            $image->setFile(new File($tmpImg, false));
            $this->imageUploader->upload($image);

            unlink($tmpImg);

            return $image;
        } catch (\Throwable $e) {
            // TODO: Log error?
            return null;
        }
    }
}
