<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Exporter\Processor;

use App\Constant\ImportExport;
use App\Entity\Brand\BrandInterface;
use App\Entity\Brand\InternalBrand;
use App\Entity\Channel\Channel;
use App\Entity\Product\OeNumber;
use App\Entity\Product\Product;
use App\Entity\Taxonomy\InternalCategory;
use App\Entity\Taxonomy\TaxonInterface;
use App\Exporter\Reader\AdProductReader;
use App\Exporter\Reader\ReaderInterface;
use App\Repository\Brand\InternalBrandRepository;
use App\Repository\Product\OeNumberRepository;
use App\Repository\Taxon\InternalCategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use FriendsOfSylius\SyliusImportExportPlugin\Importer\Transformer\TransformerPoolInterface;
use FriendsOfSylius\SyliusImportExportPlugin\Processor\MetadataValidatorInterface;
use FriendsOfSylius\SyliusImportExportPlugin\Processor\ResourceProcessorInterface;
use FriendsOfSylius\SyliusImportExportPlugin\Service\AttributeCodesProviderInterface;
use Loevgaard\SyliusBrandPlugin\Doctrine\ORM\BrandRepositoryInterface;
use Sylius\Bundle\CoreBundle\Doctrine\ORM\ProductTaxonRepository;
use Sylius\Component\Channel\Model\ChannelInterface;
use Sylius\Component\Channel\Repository\ChannelRepositoryInterface;
use Sylius\Component\Core\Model\ProductInterface;
use Sylius\Component\Core\Model\ProductTaxonInterface;
use Sylius\Component\Core\Model\Taxon;
use Sylius\Component\Core\Repository\ProductRepositoryInterface;
use Sylius\Component\Product\Factory\ProductFactoryInterface;
use Sylius\Component\Product\Generator\SlugGeneratorInterface;
use Sylius\Component\Resource\Factory\FactoryInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Sylius\Component\Taxonomy\Factory\TaxonFactoryInterface;
use Sylius\Component\Taxonomy\Repository\TaxonRepositoryInterface;

class ProductProcessor implements ResourceProcessorInterface
{
    /** @var RepositoryInterface */
    private $channelPricingRepository;
    /** @var FactoryInterface */
    private $channelPricingFactory;
    /** @var ChannelRepositoryInterface */
    private $channelRepository;
    /** @var FactoryInterface */
    private $productTaxonFactory;
    /** @var ProductTaxonRepository */
    private $productTaxonRepository;
    /** @var EntityManagerInterface */
    private $manager;
    /** @var TransformerPoolInterface|null */
    private $transformerPool;
    /** @var ProductFactoryInterface */
    private $resourceProductFactory;
    /** @var TaxonFactoryInterface */
    private $productRepository;
    /** @var TaxonRepositoryInterface */
    private $taxonRepository;
    /** @var MetadataValidatorInterface */
    private $metadataValidator;
    /** @var RepositoryInterface */
    private $productAttributeRepository;
    /** @var FactoryInterface */
    private $productAttributeValueFactory;
    /** @var AttributeCodesProviderInterface */
    private $attributeCodesProvider;
    /** @var SlugGeneratorInterface */
    private $slugGenerator;
    /** @var FactoryInterface */
    private $productVariantFactory;
    /** @var RepositoryInterface */
    private $productVariantRepository;
    /** @var BrandRepositoryInterface */
    private $brandRepository;
    /** @var ReaderInterface */
    private $productReader;
    /** @var InternalCategoryRepository */
    private $internalCategoryRepository;
    /** @var InternalBrandRepository */
    private $internalBrandRepository;
    /** @var OeNumberRepository */
    private $oeNumberRepository;

    /** @var null|string */
    private $productBrandCode;
    /** @var null|string */
    private $brandCode;
    /** @var null|string */
    private $brandName;
    /** @var null|string */
    private $productName;
    /** @var null|string */
    private $productSlug;
    /** @var null|string */
    private $productProviderCategory;
    /** @var null|string */
    private $productProviderCode;
    /** @var null|string */
    private $productUniqueCode;
    /** @var string|null */
    private $supplier;
    /** @var null|string */
    private $providerName;
    /** @var Product */
    private $product;
    /** @var null|string[] */
    private $productOeNumbers;
    /** @var null|bool */
    private $productDisplay;

    public function __construct(
        ProductFactoryInterface $productFactory,
        ProductRepositoryInterface $productRepository,
        TaxonRepositoryInterface $taxonRepository,
        MetadataValidatorInterface $metadataValidator,
        RepositoryInterface $productAttributeRepository,
        AttributeCodesProviderInterface $attributeCodesProvider,
        FactoryInterface $productAttributeValueFactory,
        ChannelRepositoryInterface $channelRepository,
        FactoryInterface $productTaxonFactory,
        FactoryInterface $productVariantFactory,
        FactoryInterface $channelPricingFactory,
        ProductTaxonRepository $productTaxonRepository,
        RepositoryInterface $productVariantRepository,
        RepositoryInterface $channelPricingRepository,
        SlugGeneratorInterface $slugGenerator,
        ?TransformerPoolInterface $transformerPool,
        EntityManagerInterface $manager,
        BrandRepositoryInterface $brandRepository,
        ReaderInterface $productReader,
        InternalCategoryRepository $internalCategoryRepository,
        InternalBrandRepository $internalBrandRepository,
        OeNumberRepository $oeNumberRepository
    ) {
        $this->resourceProductFactory = $productFactory;
        $this->productRepository = $productRepository;
        $this->taxonRepository = $taxonRepository;
        $this->metadataValidator = $metadataValidator;
        $this->productAttributeRepository = $productAttributeRepository;
        $this->productAttributeValueFactory = $productAttributeValueFactory;
        $this->attributeCodesProvider = $attributeCodesProvider;
        $this->slugGenerator = $slugGenerator;
        $this->transformerPool = $transformerPool;
        $this->manager = $manager;
        $this->channelRepository = $channelRepository;
        $this->productTaxonFactory = $productTaxonFactory;
        $this->productTaxonRepository = $productTaxonRepository;
        $this->productVariantFactory = $productVariantFactory;
        $this->productVariantRepository = $productVariantRepository;
        $this->channelPricingFactory = $channelPricingFactory;
        $this->channelPricingRepository = $channelPricingRepository;
        $this->brandRepository = $brandRepository;
        $this->productReader = $productReader;
        $this->internalCategoryRepository = $internalCategoryRepository;
        $this->internalBrandRepository = $internalBrandRepository;
        $this->oeNumberRepository = $oeNumberRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function process(array $data): void
    {
        $this->providerName = $this->productReader->getProductProviderName();
        $this->getData($data);

        if ($this->providerName === AdProductReader::PRODUCT_PROVIDER_NAME && !$this->productDisplay) {
            return;
        }

        $this->setDetails();
        $this->setMainTaxon();
        $this->setChannel();
        $this->productRepository->add($this->product);
    }

    /**
     * @param array $data
     */
    private function getData(array $data): void
    {
        $productData = $this->productReader->getData($data);
        $this->productProviderCode = $productData['productProviderCode'];
        $this->brandCode = $productData['brandCode'];
        $this->brandName = $productData['brandName'];
        $this->productBrandCode = $productData['productBrandCode'];
        $this->productName = $productData['productName'];
        $this->productProviderCategory = $productData['productProviderCategory'];
        $this->productOeNumbers = $productData['productOeNumbers'] ?
            array_unique(explode(',', $productData['productOeNumbers'])) :
            []
        ;
        $this->productUniqueCode = $this->providerName . '-' . $this->productProviderCode;
        $this->supplier = $productData['productSupplier'];
        $this->product = $this->getProduct();
        $this->productSlug = urlencode(
            $this->productUniqueCode . '-' . str_replace([', ', ' '], '-', $this->productName)
        );

        $this->productDisplay = null;
        if ($this->providerName === AdProductReader::PRODUCT_PROVIDER_NAME) {
            $this->productDisplay = $productData['productDisplay'];
        }
    }

    private function getProduct(): ProductInterface
    {
        /** @var ProductInterface|null $product */
        $product = $this->productRepository->findOneBy(['code' => $this->productUniqueCode]);
        if (null === $product) {
            /** @var ProductInterface $product */
            $product = $this->resourceProductFactory->createNew();
            $product->setCode($this->productUniqueCode);
            $product->setCurrentLocale(ImportExport::DEFAULT_LOCALE);
        }

        return $product;
    }

    private function setMainTaxon(): void
    {
        /** @var TaxonInterface|null $taxon */
        $taxon = $this->productProviderCategory ? $this->findTaxon() : null;
        /** @var InternalCategory|null $internalCategory */
        $internalCategory = $this->internalCategoryRepository->findOneBy(['name' => $this->productProviderCategory]);

        if ($internalCategory === null && $this->productProviderCategory !== null) {
            $internalCategory = $this->createInternalCategory();
            $this->manager->persist($internalCategory);
            $this->manager->flush();
        }

        if ($taxon === null && $internalCategory !== null) {
            $taxon = $internalCategory->getTaxon();
        }

        if ($taxon !== null) {
            $taxon->addInternalCategory($internalCategory);
            $this->product->setMainTaxon($taxon);
            $this->addTaxonToProduct($taxon);
        }

        $this->product->setInternalCategory($internalCategory);
    }

    private function findTaxon(): ?Taxon
    {
        return $this->taxonRepository->findByName(
            $this->productProviderCategory,
            ImportExport::DEFAULT_LOCALE
        )[0] ?? null;
    }

    private function setDetails(): void
    {
        $this->product->setName($this->productName);
        $this->product->setSlug($this->productSlug);
        $this->product->setBrandCode($this->productBrandCode);
        $this->product->setCode($this->productUniqueCode);
        $this->product->setSupplier(null);

        foreach ($this->productOeNumbers as $productOeNumber) {
            if (
                !$this->oeNumberRepository->findOneBy(['codeNumber' => $productOeNumber, 'product' => $this->product])
            ) {
                $oeNumber = new OeNumber();
                $oeNumber->setCodeNumber($productOeNumber);
                $this->product->addOeNumber($oeNumber);
            }
        }

        if ($this->brandCode !== null && $this->brandName !== null) {
            return;
        }

        /** @var BrandInterface|null $brand */
        $brand = $this->brandRepository->findOneBy(['tecDocBrandId' => $this->brandCode]);
        /** @var InternalBrand|null $internalBrand */
        $internalBrand = $this->internalBrandRepository->findOneBy([
            'tecDocBrandId' => $this->brandCode,
            'name' => $this->brandName,
        ]);

        if ($internalBrand === null) {
            $internalBrand = $this->createInternalBrand();
            $this->manager->persist($internalBrand);
            $this->manager->flush();
        }

        if ($brand !== null) {
            $brand->addInternalBrand($internalBrand);
            $this->product->setBrand($brand);
        }

        $this->product->setInternalBrand($internalBrand);
    }

    private function setChannel(): void
    {
        /** @var ChannelInterface $channel */
        foreach ($this->channelRepository->findAll() as $channel) {
            $this->product->addChannel($channel);
        }
    }

    private function addTaxonToProduct(TaxonInterface $taxon): void
    {
        $productTaxon = $this->productTaxonRepository->findOneByProductCodeAndTaxonCode(
            $this->product->getCode(),
            $taxon->getCode()
        );

        if (null === $productTaxon) {
            /** @var ProductTaxonInterface $productTaxon */
            $productTaxon = $this->productTaxonFactory->createNew();
            $productTaxon->setTaxon($taxon);
            $this->product->replaceProductTaxon($productTaxon);
        }
    }

    private function createInternalCategory(): InternalCategory
    {
        $internalCategory = new InternalCategory();
        $internalCategory->setName($this->productProviderCategory);

        return $internalCategory;
    }

    private function createInternalBrand(): InternalBrand
    {
        $internalBrand = new InternalBrand();
        $internalBrand->setName($this->brandName);
        $internalBrand->setTecDocBrandId($this->brandCode);

        return $internalBrand;
    }
}
