<?php

namespace App\Exporter\Processor;

use App\Entity\Brand\BrandInterface;
use App\Entity\Brand\InternalBrand;
use App\Repository\Brand\InternalBrandRepository;
use Doctrine\ORM\EntityManager;
use FriendsOfSylius\SyliusImportExportPlugin\Processor\ResourceProcessorInterface;
use Loevgaard\SyliusBrandPlugin\Doctrine\ORM\BrandRepositoryInterface;
use App\Entity\Brand\Brand;
use Webmozart\Assert\Assert;

class BrandProcessor implements ResourceProcessorInterface
{
    public const BRAND_NAME_HEADER = 'BrandName';
    public const BRAND_TEC_DOC_ID_HEADER = 'TecDocBrandId';

    /** @var BrandRepositoryInterface */
    private $brandRepository;
    /** @var InternalBrandRepository */
    private $internalBrandRepository;
    /** @var EntityManager */
    private $entityManager;
    /** @var string|null */
    private $brandName;
    /** @var string|null */
    private $brandTecDocId;

    public function __construct(
        BrandRepositoryInterface $brandRepository,
        InternalBrandRepository $internalBrandRepository,
        EntityManager $entityManager
    ) {
        $this->brandRepository = $brandRepository;
        $this->internalBrandRepository = $internalBrandRepository;
        $this->entityManager = $entityManager;
    }

    public function process(array $data): void
    {
        $this->getData($data);
        /** @var BrandInterface|null $brand */
        $brand = $this->brandRepository->findOneBy([
            'code' => $this->brandTecDocId,
        ]);
        /** @var InternalBrand|null $brandInternal */
        $brandInternal = $this->internalBrandRepository->findOneBy([
            'name'          => $this->brandName,
            'tecDocBrandId' => $this->brandTecDocId
        ]);

        if ($brandInternal === null) {
            $brandInternal = $this->createInternalBrand();
        }

        if ($brand === null && $this->brandTecDocId !== '' && $this->brandName !== '') {
            $brand = $this->createBrand();
            $brand->addInternalBrand($brandInternal);
        }

        $this->entityManager->flush();
    }

    /**
     * @param array<string> $data
     */
    private function getData(array $data): void
    {
        $errorMessage = \sprintf(
            'Wrong data structure provided for %s. %s',
            self::class,
            \implode(',', \array_keys($data))
        );
        Assert::keyExists($data, self::BRAND_NAME_HEADER, $errorMessage);
        Assert::keyExists($data, self::BRAND_TEC_DOC_ID_HEADER, $errorMessage);

        $this->brandName = trim((string)$data[self::BRAND_NAME_HEADER]);
        $this->brandTecDocId = trim((string)$data[self::BRAND_TEC_DOC_ID_HEADER]);
    }

    private function createBrand(): BrandInterface
    {
        $brand = new Brand();
        $brand->setName($this->brandName);
        $brand->setCode($this->brandTecDocId);
        $brand->setTecDocBrandId($this->brandTecDocId);
        $this->entityManager->persist($brand);

        return $brand;
    }

    private function createInternalBrand(): InternalBrand
    {
        $internalBrand = new InternalBrand();
        $internalBrand->setName($this->brandName);
        $internalBrand->setTecDocBrandId((int) $this->brandTecDocId);
        $this->entityManager->persist($internalBrand);

        return $internalBrand;
    }
}
