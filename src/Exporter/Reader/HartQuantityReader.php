<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Exporter\Reader;

use Webmozart\Assert\Assert;

class HartQuantityReader implements ReaderInterface
{
    public const PRODUCT_PROVIDER_NAME = 'HART';
    public const PRODUCT_PROVIDER_CODE = 'productProviderCode';
    public const PRODUCT_QUANTITY = 'productQuantity';
    public const PRODUCT_WAREHOUSE = 'productWarehouse';

    /**
     * @param string[] $data
     * @return array
     */
    public function getData(array $data): array
    {
        $errorMessage = \sprintf(
            'Wrong data structure provided for %s. %s',
            self::class,
            \implode(',', \array_keys($data))
        );
        Assert::keyExists($data, self::PRODUCT_PROVIDER_CODE, $errorMessage);
        Assert::keyExists($data, self::PRODUCT_QUANTITY, $errorMessage);
        Assert::keyExists($data, self::PRODUCT_WAREHOUSE, $errorMessage);

        return [
            self::PRODUCT_PROVIDER_CODE => trim($data[self::PRODUCT_PROVIDER_CODE]),
            self::PRODUCT_QUANTITY      => trim($data[self::PRODUCT_QUANTITY]),
            self::PRODUCT_WAREHOUSE     => trim($data[self::PRODUCT_WAREHOUSE]),
        ];
    }

    public function getProductProviderName(): string
    {
        return self::PRODUCT_PROVIDER_NAME;
    }
}
