<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Exporter\Reader;

use Webmozart\Assert\Assert;

class AdPriceReader implements ReaderInterface
{
    public const PRODUCT_PROVIDER_NAME = 'AD';
    public const PRODUCT_PROVIDER_CODE = 'productProviderCode';
    public const PRODUCT_ORIGINAL_PRICE = 'originalPrice';
    public const PRODUCT_PRICE = 'price';
    public const PRODUCT_PROVIDER_CODE_HEADER = 'Prekės ID';
    public const PRODUCT_ORIGINAL_PRICE_HEADER = 'Original price';
    public const PRODUCT_PRICE_HEADER = 'AD price';

    /**
     * @param  mixed[] $data
     * @return array
     */
    public function getData(array $data): array
    {
        $errorMessage = \sprintf(
            'Wrong data structure provided for %s. %s',
            self::class,
            \implode(',', \array_keys($data))
        );
        Assert::keyExists($data, self::PRODUCT_PROVIDER_CODE_HEADER, $errorMessage);
        Assert::keyExists($data, self::PRODUCT_PRICE_HEADER, $errorMessage);
        Assert::keyExists($data, self::PRODUCT_ORIGINAL_PRICE_HEADER, $errorMessage);

        return [
            self::PRODUCT_PROVIDER_CODE     => trim($data[self::PRODUCT_PROVIDER_CODE_HEADER]),
            self::PRODUCT_PRICE             =>
                (int) (round((float) trim($data[self::PRODUCT_PRICE_HEADER]), 2) * 100),
            self::PRODUCT_ORIGINAL_PRICE    =>
                (int) (round((float) trim($data[self::PRODUCT_ORIGINAL_PRICE_HEADER]), 2) * 100)
        ];
    }

    public function getProductProviderName(): string
    {
        return self::PRODUCT_PROVIDER_NAME;
    }
}
