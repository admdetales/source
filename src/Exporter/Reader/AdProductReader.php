<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Exporter\Reader;

use Webmozart\Assert\Assert;

class AdProductReader implements ReaderInterface
{
    public const PRODUCT_PROVIDER_NAME = 'AD';
    public const PRODUCT_PROVIDER_CODE = 'productProviderCode';
    public const BRAND_CODE = 'brandCode';
    public const BRAND_NAME = 'brandName';
    public const PRODUCT_BRAND_CODE = 'productBrandCode';
    public const PRODUCT_NAME = 'productName';
    public const PRODUCT_NAME_RU = 'productNameRu';
    public const PRODUCT_NAME_EN = 'productNameEn';
    public const PRODUCT_PROVIDER_CATEGORY = 'productProviderCategory';
    public const PRODUCT_OE_NUMBERS = 'productOeNumbers';
    public const PRODUCT_DISPLAY = 'productDisplay';
    public const PRODUCT_SUPPLIER = 'productSupplier';

    public const PRODUCT_PROVIDER_CODE_HEADER = 'Prekės ID';
    public const BRAND_CODE_HEADER = 'Brand ID';
    public const BRAND_NAME_HEADER = 'Gamintojas';
    public const PRODUCT_BRAND_CODE_HEADER = 'Pagrindinis kodas';
    public const PRODUCT_NAME_HEADER = 'Pavadinimas';
    public const PRODUCT_PROVIDER_CATEGORY_HEADER = 'Prekės grupė';
    public const PRODUCT_OE_NUMBERS_HEADER = 'Požymis 1';
    public const PRODUCT_DISPLAY_HEADER = 'Nerodyti www';
    public const PRODUCT_SUPPLIER_HEADER = 'Tiekėjas';
    public const PRODUCT_NAME_RU_HEADER = 'Pavadinimas RU';
    public const PRODUCT_NAME_EN_HEADER = 'Pavadinimas EN';

    /**
     * @param array $data
     * @return array
     */
    public function getData(array $data): array
    {
        $errorMessage = \sprintf(
            'Wrong data structure provided for %s. %s',
            self::class,
            \implode(',', \array_keys($data))
        );
        Assert::keyExists($data, self::BRAND_CODE_HEADER, $errorMessage);
        Assert::keyExists($data, self::BRAND_NAME_HEADER, $errorMessage);
        Assert::keyExists($data, self::PRODUCT_PROVIDER_CODE_HEADER, $errorMessage);
        Assert::keyExists($data, self::PRODUCT_BRAND_CODE_HEADER, $errorMessage);
        Assert::keyExists($data, self::PRODUCT_NAME_HEADER, $errorMessage);
        Assert::keyExists($data, self::PRODUCT_NAME_RU_HEADER, $errorMessage);
        Assert::keyExists($data, self::PRODUCT_NAME_EN_HEADER, $errorMessage);
        Assert::keyExists($data, self::PRODUCT_PROVIDER_CATEGORY_HEADER, $errorMessage);
        Assert::keyExists($data, self::PRODUCT_OE_NUMBERS_HEADER, $errorMessage);
        Assert::keyExists($data, self::PRODUCT_DISPLAY_HEADER, $errorMessage);
        Assert::keyExists($data, self::PRODUCT_SUPPLIER_HEADER, $errorMessage);

        $brandCode = trim($data[self::BRAND_CODE_HEADER]) !== '' ? (int) trim($data[self::BRAND_CODE_HEADER]) : null;
        $brandName = substr(trim($data[self::BRAND_NAME_HEADER]), 0, 255);
        $brandName = $brandName !== '' ?  $brandName : null;

        return [
            self::PRODUCT_PROVIDER_CODE     => str_replace(
                ',',
                '',
                trim($data[self::PRODUCT_PROVIDER_CODE_HEADER])
            ),
            self::BRAND_CODE                => $brandCode,
            self::BRAND_NAME                => $brandName,
            self::PRODUCT_BRAND_CODE        => trim($data[self::PRODUCT_BRAND_CODE_HEADER]),
            self::PRODUCT_NAME              => substr(
                trim($data[self::PRODUCT_NAME_HEADER]),
                0,
                255
            ),
            self::PRODUCT_NAME_RU           => substr(
                trim($data[self::PRODUCT_NAME_RU_HEADER]),
                0,
                255
            ),
            self::PRODUCT_NAME_EN           => substr(
                trim($data[self::PRODUCT_NAME_EN_HEADER]),
                0,
                255
            ),
            self::PRODUCT_PROVIDER_CATEGORY => substr(
                trim($data[self::PRODUCT_PROVIDER_CATEGORY_HEADER]),
                0,
                255
            ),
            self::PRODUCT_OE_NUMBERS        => trim($data[self::PRODUCT_OE_NUMBERS_HEADER]),
            self::PRODUCT_DISPLAY           => trim($data[self::PRODUCT_DISPLAY_HEADER]) === '0',
            self::PRODUCT_SUPPLIER => \trim((string)$data[self::PRODUCT_SUPPLIER_HEADER]),
        ];
    }

    public function getProductProviderName(): string
    {
        return self::PRODUCT_PROVIDER_NAME;
    }
}
