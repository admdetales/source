<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Exporter\Reader;

use Webmozart\Assert\Assert;

class HartProductReader implements ReaderInterface
{
    public const PRODUCT_PROVIDER_NAME = 'HART';
    public const PRODUCT_PROVIDER_CODE = 'productProviderCode';
    public const BRAND_CODE = 'brandCode';
    public const BRAND_NAME = 'brandName';
    public const PRODUCT_BRAND_CODE = 'productBrandCode';
    public const PRODUCT_NAME = 'productName';
    public const PRODUCT_PROVIDER_CATEGORY = 'productProviderCategory';
    public const PRODUCT_OE_NUMBERS = 'productOeNumbers';
    public const PRODUCT_PROVIDER_CODE_HEADER = 'KOD_HART';
    public const BRAND_CODE_HEADER = 'TECDOC_KOD';
    public const BRAND_NAME_HEADER = 'DOSTAWCA';
    public const PRODUCT_BRAND_CODE_HEADER = 'KOD_TOWARU';
    public const PRODUCT_NAME_HEADER = 'NAZWA';
    public const PRODUCT_PROVIDER_CATEGORY_HEADER = 'KATEGORIA';
    public const PRODUCT_OE_NUMBERS_HEADER = 'OE_NUMBER';
    public const PRODUCT_SUPPLIER = 'productSupplier';
    public const PRODUCT_SUPPLIER_HEADER = 'Tiekėjas';

    /**
     * @param  string[] $data
     * @return array
     */
    public function getData(array $data): array
    {
        $errorMessage = \sprintf(
            'Wrong data structure provided for %s. %s',
            self::class,
            \implode(',', \array_keys($data))
        );
        Assert::keyExists($data, self::PRODUCT_PROVIDER_CODE_HEADER, $errorMessage);
        Assert::keyExists($data, self::BRAND_CODE_HEADER, $errorMessage);
        Assert::keyExists($data, self::BRAND_NAME_HEADER, $errorMessage);
        Assert::keyExists($data, self::PRODUCT_BRAND_CODE_HEADER, $errorMessage);
        Assert::keyExists($data, self::PRODUCT_NAME_HEADER, $errorMessage);
        Assert::keyExists($data, self::PRODUCT_PROVIDER_CATEGORY_HEADER, $errorMessage);
        Assert::keyExists($data, self::PRODUCT_OE_NUMBERS_HEADER, $errorMessage);

        return [
            self::PRODUCT_PROVIDER_CODE     => str_replace(
                ',',
                '',
                trim($data[self::PRODUCT_PROVIDER_CODE_HEADER])
            ),
            self::BRAND_CODE                => (int) trim($data[self::BRAND_CODE_HEADER]),
            self::BRAND_NAME                => substr(
                trim($data[self::BRAND_NAME_HEADER]),
                0,
                255
            ),
            self::PRODUCT_BRAND_CODE        => trim($data[self::PRODUCT_BRAND_CODE_HEADER]),
            self::PRODUCT_NAME              => substr(
                trim($data[self::PRODUCT_NAME_HEADER]),
                0,
                255
            ),
            self::PRODUCT_PROVIDER_CATEGORY => substr(
                trim($data[self::PRODUCT_PROVIDER_CATEGORY_HEADER]),
                0,
                255
            ),
            self::PRODUCT_OE_NUMBERS        => trim($data[self::PRODUCT_OE_NUMBERS_HEADER]),
            self::PRODUCT_SUPPLIER => \trim((string)$data[self::PRODUCT_SUPPLIER_HEADER]),
        ];
    }

    public function getProductProviderName(): string
    {
        return self::PRODUCT_PROVIDER_NAME;
    }
}
