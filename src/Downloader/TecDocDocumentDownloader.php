<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Downloader;

use App\Exception\TecDoc\DocumentNotFoundException;
use Nfq\Bundle\TecDocBundle\Provider\PathProvider;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Throwable;

use function sprintf;

final class TecDocDocumentDownloader
{
    /** @var HttpClientInterface */
    private $httpClient;

    /** @var PathProvider */
    private $pathProvider;

    public function __construct(HttpClientInterface $tecDocMediaClient, PathProvider $pathProvider)
    {
        $this->httpClient   = $tecDocMediaClient;
        $this->pathProvider = $pathProvider;
    }

    public function downloadImage(int $thumbnailId): string
    {
        $image = $this->downloadImageContents($thumbnailId);

        if ($image === '') {
            throw new DocumentNotFoundException(sprintf('Image with thumbnail id "%s" not found.', $thumbnailId));
        }

        return $image;
    }

    public function downloadDocument(int $documentId): string
    {
        $document = $this->downloadDocumentContents($documentId);

        if ($document === '') {
            throw new DocumentNotFoundException(sprintf('Document with id "%s" not found.', $documentId));
        }

        return $document;
    }

    private function downloadImageContents(int $thumbnailId): string
    {
        try {
            $response = $this->httpClient->request(
                'GET',
                $this->pathProvider->getThumbnailPath($thumbnailId, PathProvider::IMAGE_FLAG_NORMAL)
            );

            return $response->getContent();
        } catch (Throwable $e) {
            return '';
        }
    }

    private function downloadDocumentContents(int $documentId): string
    {
        try {
            $response = $this->httpClient->request('GET', $this->pathProvider->getDocumentPath($documentId));

            return $response->getContent();
        } catch (Throwable $e) {
            return '';
        }
    }
}
