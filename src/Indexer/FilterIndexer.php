<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Indexer;

use Doctrine\ORM\EntityManager;
use Omni\Sylius\FilterPlugin\Doctrine\ORM\ProductAttributeRepository;
use Omni\Sylius\FilterPlugin\Indexer\OrmIndexer;
use Omni\Sylius\FilterPlugin\Indexer\TagManager;
use Omni\Sylius\FilterPlugin\Model\SearchTagAwareInterface;
use Sylius\Component\Core\Model\ProductInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\OutputInterface;

class FilterIndexer extends OrmIndexer
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var ProductAttributeRepository
     */
    private $attributeRepository;

    /**
     * @var string
     */
    private $class;

    /**
     * @var TagManager
     */
    private $tagManager;

    /**
     * @var OutputInterface|null
     */
    private $output;

    /**
     * @param EntityManager $entityManager
     * @param ProductAttributeRepository $attributeRepository
     * @param string $class
     * @param TagManager $tagManager
     */
    public function __construct(
        EntityManager $entityManager,
        ProductAttributeRepository $attributeRepository,
        string $class,
        TagManager $tagManager
    ) {
        parent::__construct(
            $entityManager,
            $attributeRepository,
            $class,
            $tagManager
        );

        $this->tagManager = $tagManager;
        $this->attributeRepository = $attributeRepository;
        $this->em = $entityManager;
        $this->class = $class;
    }

    /**
     * {@inheritdoc}
     */
    public function populate()
    {
        $filterableAttributes = $this->attributeRepository->getFilterableAttributes();

        $query = $this->em->createQuery("SELECT p FROM {$this->class} p ORDER BY p.id ASC");

        $progressBar = $this->output ? new ProgressBar($this->output) : null;
        $queryIterator = $progressBar ? $progressBar->iterate($query->iterate()) : $query->iterate();

        $i = 0;
        foreach ($queryIterator as $row) {
            /** @var SearchTagAwareInterface|ProductInterface $element */
            $element = $row[0];

            $data = $this->tagManager->extractTags($element, $filterableAttributes);
            $element->setTagIndex($this->tagManager->buildTagIndex($data));

            $this->em->persist($element);

            $i++;
            if (($i % 50) === 0) {
                $this->em->flush();
                $this->em->clear();
                $i = 0;
            }
        }

        $this->em->flush();

        $this->output && $this->output->writeln('');
        $this->output && $this->output->writeln(sprintf('Index updated successfully (%d items indexed)', $i));

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setOutput(OutputInterface $output)
    {
        $this->output = $output;

        return $this;
    }
}
