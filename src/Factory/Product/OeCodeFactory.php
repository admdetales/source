<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Factory\Product;

use App\Entity\Product\OeNumber;
use App\Entity\Product\ProductInterface;
use Nfq\Bundle\TecDocBundle\Entity\OeNumber as TecDocOeNumber;

final class OeCodeFactory
{
    public function createNew(ProductInterface $product, string $code, string $brandName): OeNumber
    {
        $oeNumber = new OeNumber();
        $oeNumber->setCodeNumber($code);
        $oeNumber->setBrandName($brandName);
        $oeNumber->setProduct($product);

        return $oeNumber;
    }

    public function createNewFromTecDoc(ProductInterface $product, TecDocOeNumber $oeNumber): OeNumber
    {
        return $this->createNew($product, $oeNumber->getNumber(), $oeNumber->getBrand());
    }
}
