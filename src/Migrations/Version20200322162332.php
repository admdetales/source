<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200322162332 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE omni_node_image (id INT AUTO_INCREMENT NOT NULL, owner_id INT NOT NULL, type VARCHAR(255) DEFAULT NULL, path VARCHAR(255) NOT NULL, relation_type VARCHAR(255) DEFAULT NULL, relation_id INT DEFAULT NULL, position VARCHAR(255) DEFAULT NULL, INDEX IDX_6FF62DAB7E3C61F9 (owner_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE omni_node (id INT AUTO_INCREMENT NOT NULL, root_id INT DEFAULT NULL, parent_id INT DEFAULT NULL, enabled TINYINT(1) NOT NULL, code VARCHAR(255) DEFAULT NULL, relation_type VARCHAR(255) DEFAULT NULL, relation_id INT DEFAULT NULL, type VARCHAR(255) DEFAULT NULL, lft INT NOT NULL, rgt INT NOT NULL, level INT NOT NULL, link LONGTEXT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, position INT DEFAULT NULL, UNIQUE INDEX UNIQ_C437E9D877153098 (code), INDEX IDX_C437E9D879066886 (root_id), INDEX IDX_C437E9D8727ACA70 (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE omni_node_channels (node_id INT NOT NULL, channel_id INT NOT NULL, INDEX IDX_929BDC3460D9FD7 (node_id), INDEX IDX_929BDC372F5A1AA (channel_id), PRIMARY KEY(node_id, channel_id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE omni_node_image_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT NOT NULL, title VARCHAR(255) DEFAULT NULL, link TEXT DEFAULT NULL, locale VARCHAR(255) NOT NULL, INDEX IDX_303097FC2C2AC5D3 (translatable_id), UNIQUE INDEX omni_node_image_translation_uniq_trans (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE omni_node_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT NOT NULL, slug VARCHAR(255) DEFAULT NULL, title VARCHAR(255) NOT NULL, content LONGTEXT DEFAULT NULL, locale VARCHAR(255) NOT NULL, INDEX IDX_8318E2A42C2AC5D3 (translatable_id), UNIQUE INDEX path_idx (locale, slug), UNIQUE INDEX omni_node_translation_uniq_trans (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE omni_node_image ADD CONSTRAINT FK_6FF62DAB7E3C61F9 FOREIGN KEY (owner_id) REFERENCES omni_node (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE omni_node ADD CONSTRAINT FK_C437E9D879066886 FOREIGN KEY (root_id) REFERENCES omni_node (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE omni_node ADD CONSTRAINT FK_C437E9D8727ACA70 FOREIGN KEY (parent_id) REFERENCES omni_node (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE omni_node_channels ADD CONSTRAINT FK_929BDC3460D9FD7 FOREIGN KEY (node_id) REFERENCES omni_node (id)');
        $this->addSql('ALTER TABLE omni_node_channels ADD CONSTRAINT FK_929BDC372F5A1AA FOREIGN KEY (channel_id) REFERENCES sylius_channel (id)');
        $this->addSql('ALTER TABLE omni_node_image_translation ADD CONSTRAINT FK_303097FC2C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES omni_node_image (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE omni_node_translation ADD CONSTRAINT FK_8318E2A42C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES omni_node (id) ON DELETE CASCADE');
        $this->addSql('CREATE TABLE omni_seo_metadata (id INT AUTO_INCREMENT NOT NULL, metaDescription LONGTEXT DEFAULT NULL, metaKeywords LONGTEXT DEFAULT NULL, metaRobots LONGTEXT DEFAULT NULL, originalUrl VARCHAR(511) DEFAULT NULL, title VARCHAR(255) DEFAULT NULL, extraHttp LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', extraNames LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', extraProperties LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', createdAt DATETIME NOT NULL, updatedAt DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE omni_node_translation ADD seo_metadata_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE omni_node_translation ADD CONSTRAINT FK_8318E2A418F9C0D5 FOREIGN KEY (seo_metadata_id) REFERENCES omni_seo_metadata (id) ON DELETE CASCADE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8318E2A418F9C0D5 ON omni_node_translation (seo_metadata_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE omni_node_image_translation DROP FOREIGN KEY FK_303097FC2C2AC5D3');
        $this->addSql('ALTER TABLE omni_node_image DROP FOREIGN KEY FK_6FF62DAB7E3C61F9');
        $this->addSql('ALTER TABLE omni_node DROP FOREIGN KEY FK_C437E9D879066886');
        $this->addSql('ALTER TABLE omni_node DROP FOREIGN KEY FK_C437E9D8727ACA70');
        $this->addSql('ALTER TABLE omni_node_channels DROP FOREIGN KEY FK_929BDC3460D9FD7');
        $this->addSql('ALTER TABLE omni_node_translation DROP FOREIGN KEY FK_8318E2A42C2AC5D3');
        $this->addSql('DROP TABLE omni_node_image');
        $this->addSql('DROP TABLE omni_node');
        $this->addSql('DROP TABLE omni_node_channels');
        $this->addSql('DROP TABLE omni_node_image_translation');
        $this->addSql('DROP TABLE omni_node_translation');
        $this->addSql('ALTER TABLE omni_node_translation DROP FOREIGN KEY FK_8318E2A418F9C0D5');
        $this->addSql('DROP TABLE omni_seo_metadata');
        $this->addSql('DROP INDEX UNIQ_8318E2A418F9C0D5 ON omni_node_translation');
        $this->addSql('ALTER TABLE omni_node_translation DROP seo_metadata_id');
    }
}
