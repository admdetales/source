<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200429090035 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE sylius_oecode (id INT AUTO_INCREMENT NOT NULL, code_number VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sylius_product_oecodes (product_id INT NOT NULL, oe_code_id INT NOT NULL, INDEX IDX_604E41454584665A (product_id), INDEX IDX_604E4145E3223363 (oe_code_id), PRIMARY KEY(product_id, oe_code_id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE sylius_product_oecodes ADD CONSTRAINT FK_604E41454584665A FOREIGN KEY (product_id) REFERENCES sylius_product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sylius_product_oecodes ADD CONSTRAINT FK_604E4145E3223363 FOREIGN KEY (oe_code_id) REFERENCES sylius_oecode (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sylius_product DROP oe_codes');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE sylius_product_oecodes DROP FOREIGN KEY FK_604E4145E3223363');
        $this->addSql('DROP TABLE sylius_oecode');
        $this->addSql('DROP TABLE sylius_product_oecodes');
        $this->addSql('ALTER TABLE sylius_product ADD oe_codes JSON NOT NULL');
        $this->addSql('ALTER TABLE sylius_shop_user ADD consents_update TINYINT(1) DEFAULT NULL');
    }
}
