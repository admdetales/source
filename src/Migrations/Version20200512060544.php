<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200512060544 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE sylius_internal_brand (id INT AUTO_INCREMENT NOT NULL, brand_id INT DEFAULT NULL, tec_doc_brand_id INT DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, created DATETIME NOT NULL, UNIQUE INDEX UNIQ_CC32C80A5E237E06 (name), INDEX IDX_CC32C80A44F5D008 (brand_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE sylius_internal_brand ADD CONSTRAINT FK_CC32C80A44F5D008 FOREIGN KEY (brand_id) REFERENCES loevgaard_brand (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE sylius_internal_brand');
    }
}
