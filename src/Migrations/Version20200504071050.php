<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200504071050 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE sylius_customer_consent_agreement (customer_id INT NOT NULL, consent_agreement_id INT NOT NULL, INDEX IDX_C4714DF09395C3F3 (customer_id), UNIQUE INDEX UNIQ_C4714DF0937BDF (consent_agreement_id), PRIMARY KEY(customer_id, consent_agreement_id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE consent (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, mandatory TINYINT(1) NOT NULL, location_code VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, enabled TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE consent_agreement (id INT AUTO_INCREMENT NOT NULL, consent_id INT DEFAULT NULL, agreed TINYINT(1) NOT NULL, datetime DATETIME NOT NULL, INDEX IDX_7B4FB8E741079D63 (consent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE sylius_customer_consent_agreement ADD CONSTRAINT FK_C4714DF09395C3F3 FOREIGN KEY (customer_id) REFERENCES sylius_customer (id)');
        $this->addSql('ALTER TABLE sylius_customer_consent_agreement ADD CONSTRAINT FK_C4714DF0937BDF FOREIGN KEY (consent_agreement_id) REFERENCES consent_agreement (id)');
        $this->addSql('ALTER TABLE consent_agreement ADD CONSTRAINT FK_7B4FB8E741079D63 FOREIGN KEY (consent_id) REFERENCES consent (id)');
        $this->addSql('INSERT INTO consent (`name`, `mandatory`, `location_code`, `description`, `enabled`) VALUES ("gdpr1", 1, "payment_method", "consent.form.agree_on_rules_and_privacy_policy", 1)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE consent_agreement DROP FOREIGN KEY FK_7B4FB8E741079D63');
        $this->addSql('ALTER TABLE sylius_customer_consent_agreement DROP FOREIGN KEY FK_C4714DF0937BDF');
        $this->addSql('DROP TABLE sylius_customer_consent_agreement');
        $this->addSql('DROP TABLE consent');
        $this->addSql('DROP TABLE consent_agreement');
    }
}
