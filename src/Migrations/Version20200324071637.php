<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200324071637 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE omni_shipper_config (id INT AUTO_INCREMENT NOT NULL, sender_id INT DEFAULT NULL, gateway VARCHAR(255) DEFAULT NULL, config JSON NOT NULL COMMENT \'(DC2Type:json_array)\', INDEX IDX_DDAB9290F624B39D (sender_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE bitbag_shipping_gateway (id INT AUTO_INCREMENT NOT NULL, code VARCHAR(255) NOT NULL, config JSON NOT NULL COMMENT \'(DC2Type:json_array)\', name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE bitbag_shipping_gateway_method (shipping_gateway_id INT NOT NULL, shipping_method_id INT NOT NULL, INDEX IDX_8606B9CBEF84DE5E (shipping_gateway_id), INDEX IDX_8606B9CB5F7D6850 (shipping_method_id), PRIMARY KEY(shipping_gateway_id, shipping_method_id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE bitbag_shipping_export (id INT AUTO_INCREMENT NOT NULL, shipment_id INT DEFAULT NULL, shipping_gateway_id INT DEFAULT NULL, exported_at DATETIME DEFAULT NULL, label_path VARCHAR(255) DEFAULT NULL, state VARCHAR(255) NOT NULL, external_id VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_20E62D9F7BE036FC (shipment_id), INDEX IDX_20E62D9FEF84DE5E (shipping_gateway_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE omni_parcel_machine (id INT AUTO_INCREMENT NOT NULL, code VARCHAR(255) NOT NULL, country VARCHAR(255) NOT NULL, city VARCHAR(255) NOT NULL, street VARCHAR(255) NOT NULL, provider VARCHAR(255) NOT NULL, enabled TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX code_provider_idx (code, provider), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE omni_shipper_config ADD CONSTRAINT FK_DDAB9290F624B39D FOREIGN KEY (sender_id) REFERENCES sylius_channel (id)');
        $this->addSql('ALTER TABLE bitbag_shipping_gateway_method ADD CONSTRAINT FK_8606B9CBEF84DE5E FOREIGN KEY (shipping_gateway_id) REFERENCES bitbag_shipping_gateway (id)');
        $this->addSql('ALTER TABLE bitbag_shipping_gateway_method ADD CONSTRAINT FK_8606B9CB5F7D6850 FOREIGN KEY (shipping_method_id) REFERENCES sylius_shipping_method (id)');
        $this->addSql('ALTER TABLE bitbag_shipping_export ADD CONSTRAINT FK_20E62D9F7BE036FC FOREIGN KEY (shipment_id) REFERENCES sylius_shipment (id)');
        $this->addSql('ALTER TABLE bitbag_shipping_export ADD CONSTRAINT FK_20E62D9FEF84DE5E FOREIGN KEY (shipping_gateway_id) REFERENCES bitbag_shipping_gateway (id)');
        $this->addSql('ALTER TABLE sylius_shipping_method ADD parcel_machine_provider_code VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE sylius_shipment ADD sender_id INT DEFAULT NULL, ADD parcel_machine_id INT DEFAULT NULL, ADD shipment_total INT DEFAULT NULL, ADD shipper_status VARCHAR(255) DEFAULT NULL, ADD shipper_info VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE sylius_shipment ADD CONSTRAINT FK_FD707B33F624B39D FOREIGN KEY (sender_id) REFERENCES sylius_channel (id)');
        $this->addSql('ALTER TABLE sylius_shipment ADD CONSTRAINT FK_FD707B33A4EBB061 FOREIGN KEY (parcel_machine_id) REFERENCES omni_parcel_machine (id)');
        $this->addSql('CREATE INDEX IDX_FD707B33F624B39D ON sylius_shipment (sender_id)');
        $this->addSql('CREATE INDEX IDX_FD707B33A4EBB061 ON sylius_shipment (parcel_machine_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE bitbag_shipping_gateway_method DROP FOREIGN KEY FK_8606B9CBEF84DE5E');
        $this->addSql('ALTER TABLE bitbag_shipping_export DROP FOREIGN KEY FK_20E62D9FEF84DE5E');
        $this->addSql('ALTER TABLE sylius_shipment DROP FOREIGN KEY FK_FD707B33A4EBB061');
        $this->addSql('DROP TABLE omni_shipper_config');
        $this->addSql('DROP TABLE bitbag_shipping_gateway');
        $this->addSql('DROP TABLE bitbag_shipping_gateway_method');
        $this->addSql('DROP TABLE bitbag_shipping_export');
        $this->addSql('DROP TABLE omni_parcel_machine');
        $this->addSql('ALTER TABLE sylius_shipment DROP FOREIGN KEY FK_FD707B33F624B39D');
        $this->addSql('DROP INDEX IDX_FD707B33F624B39D ON sylius_shipment');
        $this->addSql('DROP INDEX IDX_FD707B33A4EBB061 ON sylius_shipment');
        $this->addSql('ALTER TABLE sylius_shipment DROP sender_id, DROP parcel_machine_id, DROP shipment_total, DROP shipper_status, DROP shipper_info');
        $this->addSql('ALTER TABLE sylius_shipping_method DROP parcel_machine_provider_code');
    }
}
