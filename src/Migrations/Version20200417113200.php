<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200417113200 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('DROP INDEX fulltext_search_idx ON omni_search_index');
        $this->addSql('CREATE FULLTEXT INDEX fulltext_search_idx ON omni_search_index (`index`)'); // should be index, plugin has mismatched name with `resource_id` field and omni:search:index command always resets to `index` field

        $this->addSql('alter table omni_search_index change createdAt created_at datetime not null');
        $this->addSql('alter table sylius_product drop tagIndex');
        $this->addSql('alter table sylius_product add tag_index longtext default null');
        $this->addSql('create fulltext index fulltext_search_idx on sylius_product (tag_index)');
        $this->addSql(
            'alter table sylius_address change houseNumber house_number varchar(36) default null, change apartmentNumber apartment_number varchar(36) default null'
        );
        $this->addSql(
            'alter table omni_seo_metadata change metaDescription meta_description longtext null, change metaKeywords meta_keywords longtext null, change metaRobots meta_robots longtext null, change originalUrl original_url varchar(511) null, change extraHttp extra_http longtext null comment \'(DC2Type:array)\', change extraNames extra_names longtext null comment \'(DC2Type:array)\', change extraProperties extra_properties longtext null comment \'(DC2Type:array)\', change createdAt created_at datetime not null, change updatedAt updated_at datetime not null'
        );
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('DROP INDEX fulltext_search_idx ON omni_search_index');
        $this->addSql('CREATE FULLTEXT INDEX fulltext_search_idx ON omni_search_index (`index`)');

        $this->addSql('alter table omni_search_index change created_at createdAt datetime not null');
        $this->addSql('alter table sylius_product drop tag_index');
        $this->addSql('alter table sylius_product add tagIndex longtext default null');
        $this->addSql('create fulltext index fulltext_search_idx on sylius_product (tagIndex)');
        $this->addSql(
            'alter table sylius_address change house_number houseNumber varchar(36) default null, change apartment_number apartmentNumber varchar(36) default null'
        );
        $this->addSql(
            'alter table omni_seo_metadata change meta_description metaDescription longtext null, change meta_keywords metaKeywords longtext null, change meta_robots metaRobots longtext null, change original_url originalUrl varchar(511) null, change extra_http extraHttp longtext null comment \'(DC2Type:array)\', change extra_names extraNames longtext null comment \'(DC2Type:array)\', change extra_properties extraProperties longtext null comment \'(DC2Type:array)\', change created_at createdAt datetime not null, change updated_at updatedAt datetime not null'
        );
    }
}
