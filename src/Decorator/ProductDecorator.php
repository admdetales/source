<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Decorator;

use App\Entity\Product\OeNumber;
use App\Entity\Product\ProductInterface;
use App\EventListener\Search\ResourceListener;
use App\Factory\Product\OeCodeFactory;
use App\Factory\TecDoc\TecDocDocumentFactory;
use App\Provider\TecDoc\ArticleProvider;
use Nfq\Bundle\TecDocBundle\Entity\Article;
use Nfq\Bundle\TecDocBundle\Entity\Attribute;
use Nfq\Bundle\TecDocBundle\Entity\Document;
use Nfq\Bundle\TecDocBundle\Entity\Thumbnail;
use Sylius\Bundle\ResourceBundle\Event\ResourceControllerEvent;
use Sylius\Component\Attribute\AttributeType\TextAttributeType;
use Sylius\Component\Attribute\Factory\AttributeFactoryInterface;
use Sylius\Component\Core\Model\ProductImageInterface;
use Sylius\Component\Locale\Context\LocaleContextInterface;
use Sylius\Component\Product\Model\ProductAttributeValueInterface;
use Sylius\Component\Resource\Factory\FactoryInterface;
use Symfony\Component\Routing\RouterInterface;

use function pathinfo;
use function sprintf;
use function strtolower;
use function trim;

use const PATHINFO_EXTENSION;

final class ProductDecorator
{
    /** @var ArticleProvider */
    private $articleProvider;

    /** @var AttributeFactoryInterface */
    private $attributeFactory;

    /** @var FactoryInterface */
    private $attributeValueFactory;

    /** @var OeCodeFactory */
    private $oeCodeFactory;

    /** @var FactoryInterface */
    private $productImageFactory;

    /** @var TecDocDocumentFactory */
    private $tecDocDocumentFactory;

    /** @var LocaleContextInterface */
    private $localeContext;

    /** @var RouterInterface */
    private $router;

    /** @var ResourceListener */
    private $indexer;

    public function __construct(
        ArticleProvider $articleProvider,
        AttributeFactoryInterface $attributeFactory,
        FactoryInterface $attributeValueFactory,
        OeCodeFactory $oeCodeFactory,
        FactoryInterface $productImageFactory,
        TecDocDocumentFactory $tecDocDocumentFactory,
        LocaleContextInterface $localeContext,
        RouterInterface $router,
        ResourceListener $indexer
    ) {
        $this->articleProvider       = $articleProvider;
        $this->attributeFactory      = $attributeFactory;
        $this->attributeValueFactory = $attributeValueFactory;
        $this->oeCodeFactory         = $oeCodeFactory;
        $this->productImageFactory   = $productImageFactory;
        $this->tecDocDocumentFactory = $tecDocDocumentFactory;
        $this->localeContext         = $localeContext;
        $this->router                = $router;
        $this->indexer               = $indexer;
    }

    /**
     * Decorate single product
     *
     * @param ProductInterface $product
     */
    public function decorate(ProductInterface $product): void
    {
        $article = $this->articleProvider->findArticle($product);
        if (!$article) {
            return;
        }

        $this->decorateProductFromArticleData($product, $article);
    }

    /**
     * Decorate multiple products
     *
     * Products which are missing brand data (Code and ID) will not be decorated.
     *
     * @param ProductInterface[] $products
     * @param array $decoratedProducts
     */
    public function decorateMultipleProducts(array $products, array $decoratedProducts = []): void
    {
        $articles = $this->articleProvider->findArticles($products, $decoratedProducts);

        foreach ($articles as $article) {
            $this->decorateProductFromArticleData($article->product, $article);
        }
    }

    /**
     * Decorate the product based on given decorated article
     *
     * Article must have all necessary decorated data.
     *
     * @param ProductInterface $product
     * @param Article $article
     */
    private function decorateProductFromArticleData(ProductInterface $product, Article $article)
    {
        $product->setTecDocName($article->getName());
        $product->setHasVehicles($article->hasVehicles());

        $this->addOeNumbers($product, $article);
        $this->addAttributes($product, $article);
        $this->addImages($product, $article);
        $this->addDocuments($product, $article);
    }

    private function addOeNumbers(ProductInterface $product, Article $article): void
    {
        $reindex = false;

        foreach ($article->getOeNumbers() as $oeNumber) {
            $existing = $product->getOeNumbers()->filter(
                static function (OeNumber $number) use ($oeNumber) {
                    return sprintf('%s-%s', $number->getCodeNumber(), $number->getBrandName())
                        === sprintf('%s-%s', $oeNumber->getNumber(), $oeNumber->getBrand());
                }
            );

            if ($existing->count() > 0) {
                continue;
            }

            $product->addOeNumber($this->oeCodeFactory->createNewFromTecDoc($product, $oeNumber));
            $reindex = true;
        }

        if ($reindex) {
            $this->indexer->createResourceIndex(new ResourceControllerEvent($product));
        }
    }

    private function addImages(ProductInterface $product, Article $article): void
    {
        /** @var Thumbnail $thumbnail */
        foreach ($article->getThumbnails() as $thumbnail) {
            $product->addTecDocImage($this->createProductImage($thumbnail));
        }
    }

    private function addDocuments(ProductInterface $product, Article $article): void
    {
        /** @var Document[] $documents */
        $documents = $article->getDocuments()->filter(
            static function (Document $document) {
                return pathinfo(strtolower($document->getFileName()), PATHINFO_EXTENSION) === 'pdf';
            }
        );

        foreach ($documents as $document) {
            $product->addTecDocDocument($this->tecDocDocumentFactory->createFromDocument($document));
        }
    }

    private function addAttributes(ProductInterface $product, Article $article): void
    {
        /** @var Attribute $attribute */
        foreach ($article->getAttributes() as $attribute) {
            $weight = $article->getWeight();
            if ($weight !== null && $attribute->isWeightAttribute()) {
                $product->addTecDocAttribute(
                    $this->createAttribute(
                        $this->getAttributeName($attribute),
                        (string)$attribute->getId(),
                        $weight->getValue() . ' ' . $weight->getUnit()
                    )
                );

                continue;
            }

            if (!is_null($attribute->getValue()) && trim($attribute->getValue()) !== '') {
                $product->addTecDocAttribute(
                    $this->createAttribute(
                        $this->getAttributeName($attribute),
                        (string)$attribute->getId(),
                        $attribute->getValue()
                    )
                );
            }
        }

        foreach ($article->getArticleInfo() as $articleInfo) {
            $product->addTecDocAttribute(
                $this->createAttribute(
                    $articleInfo->getTypeName(),
                    'info_' . $articleInfo->getId(),
                    $articleInfo->getText()
                )
            );
        }
    }

    private function createAttribute(string $name, string $code, $value): ProductAttributeValueInterface
    {
        $attribute = $this->attributeFactory->createTyped(TextAttributeType::TYPE);
        $attribute->setCurrentLocale($this->localeContext->getLocaleCode());
        $attribute->setName($name);
        $attribute->setCode($code);

        /** @var ProductAttributeValueInterface $attributeValue */
        $attributeValue = $this->attributeValueFactory->createNew();
        $attributeValue->setLocaleCode($this->localeContext->getLocaleCode());
        $attributeValue->setAttribute($attribute);
        $attributeValue->setValue($value);

        return $attributeValue;
    }

    private function createProductImage(Thumbnail $thumbnail): ProductImageInterface
    {
        /** @var ProductImageInterface $productImage */
        $productImage = $this->productImageFactory->createNew();

        $productImage->setPath(
            $this->router->generate(
                'app_document_download_image',
                [
                    'thumbnailId' => $thumbnail->getId(),
                ],
            )
        );
        $productImage->setType('tecdoc');

        return $productImage;
    }

    private function getAttributeName(Attribute $attribute): string
    {
        $shortName = $attribute->getShortName();

        return trim($shortName) === '' ? $attribute->getName() : $shortName;
    }
}
