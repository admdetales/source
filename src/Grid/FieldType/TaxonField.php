<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Grid\FieldType;

use App\Entity\Taxonomy\Taxon;
use Sylius\Component\Grid\Definition\Field;
use Sylius\Component\Grid\FieldTypes\FieldTypeInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TaxonField implements FieldTypeInterface
{
    /**
     * @param Field                 $field
     * @param Taxon                 $data
     * @param array<string, mixed>  $options
     *
     * @return string
     */
    public function render(Field $field, $data, array $options): string
    {
        if ($data->getTaxon() !== null) {
            return $data->getTaxon()->getName();
        }

        return '';
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['dynamic' => false]);
        $resolver->setAllowedTypes('dynamic', ['boolean']);
    }
}
