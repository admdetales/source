<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Grid\Filter;

use Omni\Sylius\FilterPlugin\Grid\Filter\PriceFilter as BasePriceFilter;
use Sylius\Component\Grid\Data\DataSourceInterface;
use Sylius\Component\Grid\Filtering\FilterInterface;

use function is_array;

final class PriceFilter implements FilterInterface
{
    /** @var BasePriceFilter */
    private $baseFilter;

    public function __construct(BasePriceFilter $baseFilter)
    {
        $this->baseFilter = $baseFilter;
    }

    public function apply(DataSourceInterface $dataSource, string $name, $data, array $options): void
    {
        if (!is_array($data)) {
            return;
        }

        $this->baseFilter->apply($dataSource, $name, $data, $options);
    }
}
