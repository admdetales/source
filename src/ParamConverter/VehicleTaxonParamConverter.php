<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\ParamConverter;

use App\Entity\Taxonomy\Taxon;
use App\Provider\Taxon\VehicleTaxonProvider;
use LogicException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use function rtrim;

class VehicleTaxonParamConverter implements ParamConverterInterface
{
    /** @var VehicleTaxonProvider */
    private $vehicleTaxonProvider;

    public function __construct(VehicleTaxonProvider $vehicleTaxonProvider)
    {
        $this->vehicleTaxonProvider = $vehicleTaxonProvider;
    }

    public function apply(Request $request, ParamConverter $configuration): bool
    {
        $mapping = $configuration->getOptions()['mapping']['taxonSlug'] ?? 'taxonSlug';

        $slug = $request->attributes->get($mapping, null);
        $slug = $slug ? rtrim($slug, '/') : null;

        try {
            $taxon = $this->vehicleTaxonProvider->getVehicleTaxon($slug);
        } catch (LogicException $e) {
            throw new NotFoundHttpException($e->getMessage());
        }

        $request->attributes->set($configuration->getName(), $taxon);
        $request->attributes->set($mapping, $taxon->getSlug());

        return true;
    }

    public function supports(ParamConverter $configuration): bool
    {
        return $configuration->getClass() === Taxon::class;
    }
}
