<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Transformer\UrlParameter;

use Sylius\Component\Product\Generator\SlugGeneratorInterface;

use function explode;
use function sprintf;

class VehicleUrlParameterTransformer
{
    /** @var SlugGeneratorInterface */
    private $slugGenerator;

    public function __construct(SlugGeneratorInterface $slugGenerator)
    {
        $this->slugGenerator = $slugGenerator;
    }

    public function transform(string $id, string $title): string
    {
        return sprintf('%s-%s', $id, $this->slugGenerator->generate($title));
    }

    public function reverse(string $url): array
    {
        $data = explode('-', $url, 2);
        if (isset($data[0], $data[1])) {
            return [
                'id'    => $data[0],
                'title' => $data[1],
            ];
        }

        throw new UrlParameterTransformationFailedException('Unable to reverse vehicle url.');
    }
}
