<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Generator\Taxonomy;

use App\Entity\Taxonomy\Taxon;
use Behat\Transliterator\Transliterator;
use Sylius\Component\Taxonomy\Generator\TaxonSlugGeneratorInterface;
use Sylius\Component\Taxonomy\Model\TaxonInterface;
use Webmozart\Assert\Assert;

final class TaxonFixtureSlugGenerator implements TaxonSlugGeneratorInterface
{
    public function generate(TaxonInterface $taxon, ?string $locale = null): string
    {
        $name = $taxon->getTranslation($locale)->getName();

        Assert::notEmpty($name, 'Cannot generate slug without a name.');

        $slug = $this->transliterate($name);

        $parentTaxon = $taxon->getParent();
        if (null === $parentTaxon) {
            return $slug;
        }

        if ($parentTaxon->getCode() === Taxon::TREE_CODE) {
            return $slug;
        }

        $parentSlug = $parentTaxon->getTranslation($locale)->getSlug() ?: $this->generate($parentTaxon, $locale);

        return $parentSlug . '/' . $slug;
    }

    private function transliterate(string $string): string
    {
        return Transliterator::transliterate(str_replace('\'', '-', $string));
    }
}
