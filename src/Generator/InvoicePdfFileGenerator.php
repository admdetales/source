<?php

declare(strict_types=1);

namespace App\Generator;

use App\Entity\Order\Order;
use Knp\Snappy\GeneratorInterface;
use LogicException;
use Sylius\InvoicingPlugin\Entity\InvoiceInterface;
use Sylius\InvoicingPlugin\Generator\InvoicePdfFileGeneratorInterface;
use Sylius\InvoicingPlugin\Model\InvoicePdf;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\Config\FileLocatorInterface;
use Sylius\Component\Core\Repository\OrderRepositoryInterface as BaseOrderRepositoryInterface;

class InvoicePdfFileGenerator implements InvoicePdfFileGeneratorInterface
{
    private const FILE_EXTENSION = '.pdf';

    /** @var EngineInterface */
    private $templatingEngine;

    /** @var GeneratorInterface */
    private $pdfGenerator;

    /** @var FileLocatorInterface */
    private $fileLocator;

    /** @var string */
    private $template;

    /** @var BaseOrderRepositoryInterface */
    private $orderRepository;

    public function __construct(
        EngineInterface $templatingEngine,
        GeneratorInterface $pdfGenerator,
        FileLocatorInterface $fileLocator,
        string $template,
        BaseOrderRepositoryInterface $orderRepository
    ) {
        $this->templatingEngine = $templatingEngine;
        $this->pdfGenerator = $pdfGenerator;
        $this->fileLocator = $fileLocator;
        $this->template = $template;
        $this->orderRepository = $orderRepository;
    }

    public function generate(InvoiceInterface $invoice): InvoicePdf
    {
        /** @var string $filename */
        $filename = str_replace('/', '_', $invoice->number()) . self::FILE_EXTENSION;

        /** @var Order|null $order */
        $order = $this->orderRepository->findOneBy(['number' => $invoice->orderNumber()]);

        if (!$order) {
            throw new LogicException("Order not found by number: {$invoice->number()}.");
        }

        $pdf = $this->pdfGenerator->getOutputFromHtml(
            $this->templatingEngine->render($this->template, [
                'invoice' => $invoice,
                'customer' => $order->getCustomer(),
                'billingAddress' => $order->getBillingAddress(),
                'channel' => $invoice->channel(),
                'invoiceLogoPath' => $this->fileLocator->locate('assets/images/invoice-logo.png'),
            ])
        );

        return new InvoicePdf($filename, $pdf);
    }
}
