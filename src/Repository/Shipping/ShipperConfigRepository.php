<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Repository\Shipping;

use Doctrine\ORM\Query\Expr\Join;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;

class ShipperConfigRepository extends EntityRepository
{
    /**
     * @param int $senderId
     * @param string $shippingGatewayCode
     *
     * @return array|null
     */
    public function findCredentials(int $senderId, string $shippingGatewayCode): ?array
    {
        $qb = $this->createQueryBuilder('shipperConfigs');

        $qb->select('shipperConfigs.id', 'shipperConfigs.config');

        $qb->innerJoin(
            'shipperConfigs.sender',
            'channel',
            Join::WITH,
            $qb->expr()->eq('channel.id', ':senderId')
        );

        $qb->where(
            $qb->expr()->eq('shipperConfigs.gateway', ':gateway')
        );

        $qb->setParameter('senderId', $senderId);
        $qb->setParameter('gateway', $shippingGatewayCode);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param int $shipperConfigId
     * @param array $configParams
     *
     * @return mixed
     */
    public function updateCredentials(int $shipperConfigId, array $configParams)
    {
        $qb = $this->createQueryBuilder('s');

        $qb->update();
        $qb->set('s.config', ':configValue');

        $qb->where('s.id = :id');

        $qb->setParameter('configValue', \json_encode($configParams));
        $qb->setParameter('id', $shipperConfigId);

        return $qb->getQuery()->execute();
    }
}
