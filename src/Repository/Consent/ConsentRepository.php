<?php

declare(strict_types=1);

namespace App\Repository\Consent;

use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;

class ConsentRepository extends EntityRepository implements ConsentRepositoryInterface
{

}
