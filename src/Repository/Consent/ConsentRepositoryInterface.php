<?php

declare(strict_types=1);

namespace App\Repository\Consent;

use Sylius\Component\Resource\Repository\RepositoryInterface;

interface ConsentRepositoryInterface extends RepositoryInterface
{

}
