<?php

declare(strict_types=1);

namespace App\Repository\Product;

use Sylius\Bundle\ProductBundle\Doctrine\ORM\ProductAttributeValueRepository as BaseAttributeRepository;

class ProductAttributeValueRepository extends BaseAttributeRepository implements ProductAttributeValueRepositoryInterface
{
    /**
     * {@inheritDoc}
     */
    public function nullifyAttributeValueById(int $attributeId): void
    {
        $this->createQueryBuilder('pav')
        ->update()
        ->set('pav.text', ':emptyValue')
        ->where('pav.attribute = :attributeId')
        ->setParameter('emptyValue', '')
        ->setParameter('attributeId', $attributeId)
        ->getQuery()->execute();
    }
}