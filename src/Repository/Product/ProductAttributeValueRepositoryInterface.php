<?php

declare(strict_types=1);

namespace App\Repository\Product;

use Sylius\Component\Product\Repository\ProductAttributeValueRepositoryInterface as BaseAttributeInterface;

interface ProductAttributeValueRepositoryInterface extends BaseAttributeInterface
{
    /**
     * @param int $attributeId
     * @return void
     */
    public function nullifyAttributeValueById(int $attributeId): void;
}