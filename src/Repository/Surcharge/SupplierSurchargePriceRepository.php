<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Repository\Surcharge;

use App\Entity\Customer\CustomerInterface;
use App\Entity\Supplier\SupplierInterface;
use App\Entity\Surcharge\SupplierSurchargePrice;
use App\Exception\ResourceNotFoundException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

class SupplierSurchargePriceRepository extends ServiceEntityRepository implements
    SupplierSurchargePriceRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SupplierSurchargePrice::class);
    }

    /**
     * @inheritDoc
     */
    public function getAmountByCustomerAndSupplier(?CustomerInterface $customer, ?SupplierInterface $supplier): string
    {
        try {
            return (string)$this->createQueryBuilder('ssp')
                ->select('ssp.amount')
                ->andWhere('ssp.customer = :customer')
                ->setParameter('customer', $customer)
                ->andWhere('ssp.supplier = :supplier')
                ->setParameter('supplier', $supplier)
                ->getQuery()->getSingleScalarResult();
        } catch (NoResultException $e) {
            throw new ResourceNotFoundException();
        }
    }
}
