<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Repository\Surcharge;

use App\Entity\Channel\ChannelPricingInterface;
use App\Entity\Customer\CustomerInterface;
use App\Exception\ResourceNotFoundException;

interface CustomerSurchargePriceRepositoryInterface
{
    /**
     * @param CustomerInterface $customer
     * @param ChannelPricingInterface $channelPricing
     * @return string
     * @throws ResourceNotFoundException
     */
    public function getAmountByCustomerAndChannel(
        CustomerInterface $customer,
        ChannelPricingInterface $channelPricing
    ): string;
}
