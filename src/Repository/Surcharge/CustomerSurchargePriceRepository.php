<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Repository\Surcharge;

use App\Entity\Channel\ChannelPricingInterface;
use App\Entity\Customer\CustomerInterface;
use App\Entity\Surcharge\CustomerSurchargePrice;
use App\Exception\ResourceNotFoundException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

class CustomerSurchargePriceRepository extends ServiceEntityRepository implements
    CustomerSurchargePriceRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CustomerSurchargePrice::class);
    }

    /**
     * {@inheritdoc}
     */
    public function getAmountByCustomerAndChannel(
        CustomerInterface $customer,
        ChannelPricingInterface $channelPricing
    ): string {
        try {
            return (string)$this->createQueryBuilder('csp')
                ->select('csp.amount')
                ->andWhere('csp.customer = :customer')
                ->setParameter('customer', $customer)
                ->andWhere('csp.channelPricing = :channelPricing')
                ->setParameter('channelPricing', $channelPricing)
                ->getQuery()->getSingleScalarResult();
        } catch (NoResultException $e) {
            throw new ResourceNotFoundException();
        }
    }
}
