<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Repository\Taxon;

use App\Entity\Taxonomy\TaxonInterface;
use Sylius\Component\Core\Repository\ProductTaxonRepositoryInterface as BaseProductTaxonRepositoryInterface;

interface ProductTaxonRepositoryInterface extends BaseProductTaxonRepositoryInterface
{
    /**
     * @param int[] $productsIds
     * @param TaxonInterface $taxon
     */
    public function replaceProductsTaxon(array $productsIds, TaxonInterface $taxon): void;
}
