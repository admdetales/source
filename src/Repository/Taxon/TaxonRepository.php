<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Repository\Taxon;

use App\Entity\Taxonomy\TaxonInterface;
use Doctrine\ORM\QueryBuilder;
use Sylius\Bundle\TaxonomyBundle\Doctrine\ORM\TaxonRepository as BaseTaxonRepository;

/**
 * @method TaxonInterface|null find($id, $lockMode = null, $lockVersion = null)
 * @method TaxonInterface|null findOneBy(array $criteria, array $orderBy = null)
 * @method TaxonInterface[]    findAll()
 * @method TaxonInterface[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaxonRepository extends BaseTaxonRepository implements TaxonRepositoryInterface
{
    public function findOneByNameAndParent(string $name, string $locale, ?TaxonInterface $parent): ?TaxonInterface
    {
        $qb = $this->createQueryBuilder('taxon');

        $qb->innerJoin('taxon.translations', 'translation');

        $qb->where($qb->expr()->eq('translation.name', ':name'));
        $qb->andWhere($qb->expr()->eq('translation.locale', ':locale'));

        $qb->setParameter('name', $name);
        $qb->setParameter('locale', $locale);

        if (null === $parent) {
            $qb->andWhere($qb->expr()->isNull('taxon.parent'));
        } else {
            $qb->andWhere($qb->expr()->eq('taxon.parent', ':parent'));
            $qb->setParameter('parent', $parent->getId());
        }

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * {@inheritDoc}
     */
    public function findByCode(array $codes): array
    {
        return $this->findBy(['code' => $codes]);
    }

    /**
     * {@inheritDoc}
     */
    public function findByCodesWithSort(array $codes, string $locale, ?string $sort = 'ASC'): array
    {
        $qb = $this->createQueryBuilder('taxon');
        $qb->innerJoin('taxon.translations', 'translation')
            ->andWhere('taxon.code IN (:codes)')
            ->andWhere('translation.locale = :locale')
            ->orderBy('translation.name', $sort)
            ->setParameter('codes', $codes)
            ->setParameter('locale', $locale);

        return $qb->getQuery()->getResult();
    }

    /**
     * {@inheritdoc}
     */
    public function findChildrenByCodeWithSort(string $parentCode, ?string $locale = null, ?string $sort = 'ASC'): array
    {
        return $this->createQueryBuilder('o')
            ->addSelect('translation')
            ->leftJoin('o.translations', 'translation')
            ->andWhere('translation.locale = :locale')
            ->setParameter('locale', $locale)
            ->addSelect('child')
            ->innerJoin('o.parent', 'parent')
            ->leftJoin('o.children', 'child')
            ->andWhere('parent.code = :parentCode')
            ->addOrderBy('translation.name', $sort)
            ->setParameter('parentCode', $parentCode)
            ->getQuery()
            ->getResult();
    }

    public function findOneByCode(string $code): ?TaxonInterface
    {
        return $this->findOneBy(['code' => $code]);
    }

    /**
     * {@inheritdoc}
     */
    public function createListQueryBuilderForChoiceForm(): QueryBuilder
    {
        return $this->createQueryBuilder('o')
            ->addSelect('translation')
            ->leftJoin('o.translations', 'translation')
            ->andWhere('o.parent IS NOT NULL');
    }
}
