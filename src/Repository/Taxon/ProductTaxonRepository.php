<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Repository\Taxon;

use App\Entity\Taxonomy\TaxonInterface;
use Sylius\Bundle\CoreBundle\Doctrine\ORM\ProductTaxonRepository as BaseProductTaxonRepository;
use Sylius\Component\Core\Model\ProductTaxonInterface;

class ProductTaxonRepository extends BaseProductTaxonRepository implements ProductTaxonRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function replaceProductsTaxon(array $productsIds, TaxonInterface $taxon): void
    {
        // First delete existing records.
        $this->createQueryBuilder('pt')
            ->delete()
            ->where('pt.taxon = :taxon')
            ->setParameter('taxon', $taxon)
            ->getQuery()->execute();

        if (empty($productsIds)) {
            return;
        }

        $manager = $this->getEntityManager();
        $connection = $manager->getConnection();
        $metadata = $manager->getClassMetadata(ProductTaxonInterface::class);
        $tableName = $metadata->getTableName();
        $productFieldColumnName = $metadata->getSingleAssociationJoinColumnName('product');
        $taxonFieldColumnName = $metadata->getSingleAssociationJoinColumnName('taxon');
        $positionFieldColumnName = $metadata->getFieldForColumn('position');
        $taxonId = (int)$taxon->getId();

        // Insert first position so that next query could increment position
        $connection->executeQuery(
            \sprintf(
                'INSERT INTO %s (%s, %s, %s) VALUES (%d, %d, %d)',
                $tableName,
                $productFieldColumnName,
                $taxonFieldColumnName,
                $positionFieldColumnName,
                (int)\array_shift($productsIds),
                $taxonId,
                0
            )
        );

        foreach (\array_chunk($productsIds, 100) as $productsIdsChunk) {
            $insertValues = [];
            foreach ($productsIdsChunk as $productId) {
                $insertValues[] = \sprintf(
                    '(%d, %d, (SELECT max(spt.position) + 1 FROM sylius_product_taxon spt where spt.taxon_id = %d))',
                    $productId,
                    $taxonId,
                    $taxonId
                );
            }

            $connection->executeQuery(
                \sprintf(
                    'INSERT INTO %s (%s, %s, %s) VALUES %s',
                    $tableName,
                    $productFieldColumnName,
                    $taxonFieldColumnName,
                    $positionFieldColumnName,
                    \implode(', ', $insertValues)
                )
            );
        }
    }
}
