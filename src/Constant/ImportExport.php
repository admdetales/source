<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Constant;

class ImportExport
{
    public const DEFAULT_LOCALE = 'lt_LT';
    public const LOCALE_RU = 'ru_RU';
    public const LOCALE_EN = 'en_US';
    public const DEFAULT_IMAGE_COLUMN_PREFIX = 'img_';
    public const COLLECTION_DELIMITER = ' | ';
    public const OPTION_ATTRIBUTE_TYPE = 'variant_option';
    public const DEFAULT_TAX_CATEGORY = 'default_taxes';
    // AD
    public const PRODUCT_PROVIDER_NAME_AD = 'AD';
    public const PRODUCT_PROVIDER_CODE_HEADER_AD = 'Prekės ID';
    public const PRODUCT_ORIGINAL_PRICE_HEADER_AD = 'Original price';
    public const PRODUCT_PRICE_HEADER_AD = 'AD price';
    public const PRODUCT_QUANTITY_HEADER_AD = 'Kiekis';
    public const PRODUCT_WAREHOUSE_NAME_HEADER_AD = 'Sandėlis';
    public const BRAND_CODE_HEADER_AD = 'Brand ID';
    public const PRODUCT_SUPPLIER_HEADER = 'Tiekėjas';
    public const BRAND_NAME_HEADER_AD = 'Gamintojas';
    public const PRODUCT_BRAND_CODE_HEADER_AD = 'Pagrindinis kodas';
    public const PRODUCT_NAME_HEADER_AD = 'Pavadinimas';
    public const PRODUCT_PROVIDER_CATEGORY_HEADER_AD = 'Prekės grupė';
    public const PRODUCT_OE_NUMBERS_HEADER_AD = 'Požymis 1';
    public const PRODUCT_DISPLAY_HEADER_AD = 'Nerodyti www';
    public const PRODUCT_NAME_RU_HEADER_AD = 'Pavadinimas RU';
    public const PRODUCT_NAME_EN_HEADER_AD = 'Pavadinimas EN';
    public const SLUG_BEGINNING_AD = 'AD';
    // HART
    public const PRODUCT_PROVIDER_NAME_HART = 'HART';
    public const PRODUCT_PROVIDER_CODE_HEADER_HART = 'KOD_HART';
    public const PRODUCT_ORIGINAL_PRICE_HEADER_HART = 'Original price';
    public const PRODUCT_PRICE_HEADER_HART = 'price';
    public const PRODUCT_PROVIDER_CODE_HEADER_QUANTITY_HART = 'productProviderCode';
    public const PRODUCT_QUANTITY_HEADER_HART = 'productQuantity';
    public const PRODUCT_WAREHOUSE_NAME = 'Tiekėjo sandėlys';
    public const BRAND_CODE_HEADER_HART = 'TECDOC_KOD';
    public const BRAND_NAME_HEADER_HART = 'DOSTAWCA';
    public const PRODUCT_BRAND_CODE_HEADER_HART = 'KOD_TOWARU';
    public const PRODUCT_NAME_HEADER_HART = 'NAZWA';
    public const PRODUCT_PROVIDER_CATEGORY_HEADER_HART = 'KATEGORIA';
    public const PRODUCT_OE_NUMBERS_HEADER_HART = 'OE_NUMBER';
    public const SLUG_BEGINNING_HART = 'PROVIDER';
    public const QUANTITY_REVIEW_PERIOD_HART = '-3 hour';
}
