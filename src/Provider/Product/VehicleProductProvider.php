<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Provider\Product;

use App\Entity\Product\ProductInterface;
use App\Entity\Taxonomy\Taxon;
use App\Repository\Product\ProductRepositoryInterface;
use Nfq\Bundle\TecDocBundle\Entity\Article;
use Nfq\Bundle\TecDocBundle\Entity\Vehicle;
use Nfq\Bundle\TecDocBundle\ModelManager\VehicleManager;
use Sylius\Component\Channel\Context\ChannelContextInterface;
use Sylius\Component\Locale\Context\LocaleContextInterface;

class VehicleProductProvider
{
    private const CHUNK_SIZE = 25;

    /** @var VehicleManager */
    private $vehicleManager;

    /** @var ProductRepositoryInterface */
    private $productRepository;

    /** @var ChannelContextInterface */
    private $channelContext;

    /** @var LocaleContextInterface */
    private $localeContext;

    public function __construct(
        VehicleManager $vehicleManager,
        ProductRepositoryInterface $productRepository,
        ChannelContextInterface $channelContext,
        LocaleContextInterface $localeContext
    ) {
        $this->vehicleManager    = $vehicleManager;
        $this->productRepository = $productRepository;
        $this->channelContext    = $channelContext;
        $this->localeContext     = $localeContext;
    }

    /**
     * @param Vehicle    $vehicle
     * @param Taxon|null $taxon
     *
     * @return ProductInterface[]
     */
    public function getProducts(Vehicle $vehicle, ?Taxon $taxon = null): array
    {
        if ($taxon === null || $taxon->getLevel() < 1 || empty($taxon->getGenericArticleIds())) {
            return [];
        }

        return $this->productRepository->getProductsByArticles(
            $this->channelContext->getChannel(),
            $this->localeContext->getLocaleCode(),
            $this->getArticlesByVehicleAndCategory($vehicle, $taxon)
        );
    }

    /**
     * @param Vehicle $vehicle
     * @param Taxon $taxon
     * @return Article[]
     */
    private function getArticlesByVehicleAndCategory(Vehicle $vehicle, Taxon $taxon): array
    {
        $articles = [];

        foreach (\array_chunk($taxon->getGenericArticleIds(), self::CHUNK_SIZE) as $genericArticleIds) {
            $articles[] = $this->vehicleManager->getArticlesByVehicleAndGenericArticle(
                $genericArticleIds,
                $vehicle
            );
        }

        return !empty($articles) ? \array_merge(...$articles) : $articles;
    }
}
