<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Provider\Taxon;

use App\Entity\Taxonomy\TaxonInterface;
use App\Repository\Taxon\TaxonRepositoryInterface;
use LogicException;
use Sylius\Component\Locale\Context\LocaleContextInterface;

class VehicleTaxonProvider
{
    /** @var TaxonRepositoryInterface */
    private $taxonRepository;

    /** @var LocaleContextInterface */
    private $localeContext;

    public function __construct(TaxonRepositoryInterface $taxonRepository, LocaleContextInterface $localeContext)
    {
        $this->taxonRepository = $taxonRepository;
        $this->localeContext   = $localeContext;
    }

    public function getVehicleTaxon(?string $slug = null): TaxonInterface
    {
        $taxon = $slug
            ? $this->taxonRepository->findOneBySlug($slug, $this->localeContext->getLocaleCode())
            : $this->taxonRepository->findOneByCode(TaxonInterface::TREE_CODE);

        if (!$taxon) {
            throw new LogicException(\sprintf('Taxon with slug "%s" not found.', $slug));
        }

        return $taxon;
    }

    /** @return TaxonInterface[] */
    public function getInnerPageCategoryTaxons(): array
    {
        return $this->taxonRepository->findByCodesWithSort(
            TaxonInterface::INNER_PAGE_CODES,
            $this->localeContext->getLocaleCode(),
            'ASC'
        );
    }

    /**
     * @param string $parentCode
     * @param null|string $sort
     *
     * @return TaxonInterface[]
     */
    public function findChildrenByCodeWithSort($parentCode, $sort = 'ASC'): array
    {
        return $this->taxonRepository->findChildrenByCodeWithSort(
            $parentCode,
            $this->localeContext->getLocaleCode(),
            $sort
        );
    }
}
