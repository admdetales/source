<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Provider;

use App\Repository\Shipping\ShipperConfigRepository;
use Omni\Sylius\ShippingPlugin\Provider\CredentialProviderInterface;

class CredentialProvider implements CredentialProviderInterface
{
    /** @var ShipperConfigRepository */
    private $shipperConfigRepository;

    /**
     * @param ShipperConfigRepository $shipperConfigRepository
     */
    public function __construct(ShipperConfigRepository $shipperConfigRepository)
    {
        $this->shipperConfigRepository = $shipperConfigRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function getCredentials(int $senderId, string $shippingGatewayCode): ?array
    {
        return $this->shipperConfigRepository->findCredentials($senderId, $shippingGatewayCode);
    }

    /**
     * {@inheritdoc}
     */
    public function updateCredentials(int $shipperConfigId, string $shippingGatewayCode, array $configParams): void
    {
        $this->shipperConfigRepository->updateCredentials($shipperConfigId, $configParams);
    }
}
