<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Provider\TecDoc;

use App\Entity\TecDoc\PopularManufacturer;
use App\Repository\TecDoc\PopularManufacturerRepositoryInterface;
use Generator;
use Nfq\Bundle\TecDocBundle\ModelManager\VehicleManager;

use function array_map;
use function in_array;

final class UnselectedPopularManufacturerProvider
{
    /** @var PopularManufacturerRepositoryInterface */
    private $popularManufacturerRepository;

    /** @var VehicleManager */
    private $vehicleManager;

    public function __construct(
        PopularManufacturerRepositoryInterface $popularManufacturerRepository,
        VehicleManager $vehicleManager
    ) {
        $this->popularManufacturerRepository = $popularManufacturerRepository;
        $this->vehicleManager                = $vehicleManager;
    }

    public function getUnselectedManufacturers(): Generator
    {
        $manufacturers = $this->vehicleManager->getVehicleManufacturers();
        $popularIds    = array_map(
            static function (PopularManufacturer $manufacturer) {
                return $manufacturer->getManufacturerId();
            },
            $this->popularManufacturerRepository->findAll()
        );

        foreach ($manufacturers as $manufacturer) {
            if (in_array($manufacturer->getId(), $popularIds, true)) {
                continue;
            }

            yield $manufacturer->getTitle() => $manufacturer->getId();
        }
    }
}
