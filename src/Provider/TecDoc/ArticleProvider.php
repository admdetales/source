<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Provider\TecDoc;

use App\Entity\Product\Product;
use App\Entity\Product\ProductInterface;
use Doctrine\ORM\EntityManagerInterface;
use Nfq\Bundle\TecDocBundle\Entity\Article;
use Nfq\Bundle\TecDocBundle\Helpers\ArticleHelper;
use Nfq\Bundle\TecDocBundle\ModelManager\ArticleManager;
use Nfq\Bundle\TecDocBundle\TecDoc;

use function array_filter;
use function reset;

final class ArticleProvider
{
    /** @var ArticleManager */
    private $articleManager;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(ArticleManager $articleManager, EntityManagerInterface $entityManager)
    {
        $this->articleManager = $articleManager;
        $this->entityManager = $entityManager;
    }

    public function findArticle(ProductInterface $product): ?Article
    {
        $brandCode     = $product->getBrandCode();
        $brand         = $product->getBrand();
        $tecDocBrandId = $brand ? $brand->getTecDocBrandId() : null;
        if (!$brandCode || !$brand || !$tecDocBrandId) {
            return null;
        }

        $articles = array_filter(
            $this->articleManager->findArticlesByCode($brandCode, TecDoc::ARTICLE_NUMBER),
            static function (Article $article) use ($tecDocBrandId) {
                return $article->getBrandNumber() === $tecDocBrandId;
            }
        );

        $fullArticles = $this->articleManager->findProductsByArticleIds(ArticleHelper::getArticlesIds($articles));
        $article      = reset($fullArticles);
        if (!$article) {
            return null;
        }

        return $article;
    }

    /**
     * Returns a list of articles with decorated data
     *
     * If an article doesn't have decorated data - it will be skipped from the list.
     * The size of given input might not match returned output (due to missing decorated article data).
     * Each decorated article will have a back-reference to product as property `product`.
     *
     * @param ProductInterface[] $products
     * @param array $decoratedProducts
     * @return Article[]
     */
    public function findArticles(array $products, array $decoratedProducts): array
    {
        $foundDecoratedArticles = [];

        $articleIDToProductMap = $this->getArticleIDToProductMap($products, $decoratedProducts);
        $decoratedArticles = $this->articleManager->findProductsByArticleIds(array_keys($articleIDToProductMap));

        /** @var Article $decoratedArticle */
        foreach ($decoratedArticles as $decoratedArticle) {
            $articleID = $decoratedArticle->getId();

            if (array_key_exists($articleID, $articleIDToProductMap)) {
                // Provide back reference from article to product
                $decoratedArticle->product = $articleIDToProductMap[$articleID];

                $foundDecoratedArticles[] = $decoratedArticle;
            }
        }

        return $foundDecoratedArticles;
    }

    /**
     * Returns a map with articleID as key and supplied product as value
     *
     * @param ProductInterface[] $products
     * @param array $decoratedProducts
     * @return ProductInterface[]
     */
    private function getArticleIDToProductMap(array $products, array $decoratedProducts): array
    {
        $map = [];

        foreach ($products as $product) {
            if (isset($decoratedProducts[$product->getId()])) {
                continue;
            }

            $brandData = $this->extractBrandData($product);

            if (is_null($brandData)) {
                continue;
            }

            list($brandCode, $brandID) = $brandData;
            $article = $this->findFirstArticleByBrandData($brandCode, $brandID);

            if (!is_null($article)) {
                $map[$article->getId()] = $product;
            }
        }

        return $map;
    }

    /**
     * Returns first matching article by given brand information
     *
     * @param string $brandCode
     * @param string $brandID
     *
     * @return Article|null
     */
    private function findFirstArticleByBrandData($brandCode, $brandID): ?Article
    {
        $articles = array_filter(
            $this->articleManager->findArticlesByCode($brandCode, TecDoc::ARTICLE_NUMBER),
            static function (Article $article) use ($brandID) {
                return $article->getBrandNumber() === $brandID;
            }
        );

        return count($articles) > 0 ? reset($articles) : null;
    }

    /**
     * Returns a tuple of "brand code" and "brand ID" values from product
     *
     * If "brand code" or "brand ID" is missing - null is returned instead.
     * Both "brand code" and "brand ID" are necessary to locate an article ID from TecDoc.
     *
     * @param ProductInterface $product
     *
     * @return string[]|null
     */
    private function extractBrandData(ProductInterface $product): ?array
    {
        $brandCode = $product->getBrandCode();
        $brand = $product->getBrand();

        $brandID = $brand ? $brand->getTecDocBrandId() : null;

        return (!empty($brandCode) && !empty($brandID)) ? [$brandCode, $brandID] : null;
    }

    public function findArticlesByOeNumber(string $oeNumber): ?array
    {
        $articles = $this->articleManager->findArticlesByCode($oeNumber, TecDoc::OE_NUMBER);

        return $this->getProductsByArticles($articles);
    }

    public function findArticlesByBrandCode(string $brandCode): ?array
    {
        $articles = $this->articleManager->findArticlesByCode($brandCode);

        return $this->getProductsByArticles($articles);
    }

    /**
     * @param $articles
     * @return array|null
     */
    private function getProductsByArticles($articles): ?array
    {
        $rep = $this->entityManager->getRepository(Product::class);
        $codes = [];
        foreach ($articles as $article) {
            $codes[] = $article->getNumber();
        }
        return empty($codes) ? [] : $rep->findAllByBrandCode($codes);
    }
}
