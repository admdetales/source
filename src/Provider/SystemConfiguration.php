<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Provider;

use App\Repository\SystemConfigValueRepository;

final class SystemConfiguration
{
    /** @var SystemConfigValueRepository */
    private $configValueRepository;

    public function __construct(SystemConfigValueRepository $configValueRepository)
    {
        $this->configValueRepository = $configValueRepository;
    }

    public function get(string $type): string
    {
        $configValue = $this->configValueRepository->findOneBy(['type' => $type]);

        if (!$configValue) {
            return '';
        }

        return $configValue->getValue();
    }

    public function replace(string $type, string $value): void
    {
        $this->configValueRepository->replace($type, $value);
    }
}
