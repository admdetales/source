<?php

declare(strict_types=1);

namespace App\CommandHandler;

use Sylius\InvoicingPlugin\Command\SendInvoiceEmail;

final class SendInvoiceEmailHandler
{
    public function __invoke(SendInvoiceEmail $command): void
    {
        return;
    }
}
