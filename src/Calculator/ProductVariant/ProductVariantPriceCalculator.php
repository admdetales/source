<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Calculator\ProductVariant;

use App\Calculator\Surcharge\CustomerSurchargePriceCalculator;
use App\Entity\Channel\ChannelPricingInterface;
use App\Entity\Customer\CustomerInterface;
use App\Entity\Supplier\Supplier;
use App\Exception\ResourceNotFoundException;
use App\Repository\Surcharge\CustomerSurchargePriceRepositoryInterface;
use App\Repository\Surcharge\SupplierSurchargePriceRepositoryInterface;
use Sylius\Component\Channel\Context\ChannelContextInterface;
use Sylius\Component\Core\Calculator\ProductVariantPriceCalculatorInterface;
use Sylius\Component\Core\Exception\MissingChannelConfigurationException;
use Sylius\Component\Core\Model\ChannelInterface;
use Sylius\Component\Core\Model\ProductVariantInterface;
use Sylius\Component\Customer\Context\CustomerContextInterface;
use Sylius\Component\Taxation\Resolver\TaxRateResolverInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class ProductVariantPriceCalculator implements ProductVariantPriceCalculatorInterface
{
    /**
     * @var CustomerContextInterface
     */
    private $customerContext;

    /**
     * @var CustomerSurchargePriceRepositoryInterface
     */
    private $customerSurchargePriceRepository;

    /**
     * @var CustomerSurchargePriceCalculator
     */
    private $customerSurchargePriceCalculator;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var ChannelContextInterface
     */
    private $channelContext;

    /**
     * @var TaxRateResolverInterface
     */
    private $taxRateResolver;

    /**
     * @var SupplierSurchargePriceRepositoryInterface
     */
    private $supplierSurchargePriceRepository;

    /**
     * @param CustomerContextInterface $customerContext
     * @param CustomerSurchargePriceRepositoryInterface $customerSurchargePriceRepository
     * @param SupplierSurchargePriceRepositoryInterface $SupplierSurchargePriceRepository
     * @param CustomerSurchargePriceCalculator $customerSurchargePriceCalculator
     * @param RequestStack $requestStack
     * @param ChannelContextInterface $channelContext
     * @param TaxRateResolverInterface $taxRateResolver
     */
    public function __construct(
        CustomerContextInterface $customerContext,
        CustomerSurchargePriceRepositoryInterface $customerSurchargePriceRepository,
        SupplierSurchargePriceRepositoryInterface $SupplierSurchargePriceRepository,
        CustomerSurchargePriceCalculator $customerSurchargePriceCalculator,
        RequestStack $requestStack,
        ChannelContextInterface $channelContext,
        TaxRateResolverInterface $taxRateResolver
    ) {
        $this->customerContext = $customerContext;
        $this->customerSurchargePriceRepository = $customerSurchargePriceRepository;
        $this->supplierSurchargePriceRepository = $SupplierSurchargePriceRepository;
        $this->customerSurchargePriceCalculator = $customerSurchargePriceCalculator;
        $this->requestStack = $requestStack;
        $this->channelContext = $channelContext;
        $this->taxRateResolver = $taxRateResolver;
    }

    /**
     * {@inheritdoc}
     */
    public function calculate(ProductVariantInterface $productVariant, array $context): int
    {
        $context = $this->resolveContext($context);

        $price = $this->calculateVariantPrice($productVariant, $context);
        $taxRate = $this->taxRateResolver->resolve($productVariant, ['zone' => $context['zone']]);

        if ($taxRate !== null && $taxRate->isIncludedInPrice()) {
            return $price + (int)\round($price * $taxRate->getAmount());
        }

        return $price;
    }

    private function calculateVariantPrice(ProductVariantInterface $productVariant, array $context): int
    {
        $channelPricing = $this->getChannelPricing($productVariant, $context);
        $price = (int)$channelPricing->getPrice();
        $originalPrice = (int)$channelPricing->getOriginalPrice();

        if (($context['price_only'] ?? false) || $this->isAdminRequest()) {
            return $price;
        }

        if (isset($context['original_price'])) {
            return $originalPrice;
        }

        /** @var CustomerInterface|null $customer */
        $customer = $this->customerContext->getCustomer();
        if ($customer === null) {
            return $price;
        }

        $calculatedPride = $this->calculateOnProductLevel($customer, $channelPricing, $originalPrice, $price);

        if ($calculatedPride && $calculatedPride == $price) {
            $supplier = $productVariant->getProduct()->getSupplier();
            $calculatedPride = $this->calculateOnSupplierLevel($customer, $supplier, $originalPrice, $price);
        }

        return $calculatedPride;
    }

    /**
     * @param ProductVariantInterface $productVariant
     * @param array $context
     * @return ChannelPricingInterface
     */
    private function getChannelPricing(ProductVariantInterface $productVariant, array $context): ChannelPricingInterface
    {
        /** @var ChannelPricingInterface|null $channelPricing */
        $channelPricing = $productVariant->getChannelPricingForChannel($context['channel']);

        if (null === $channelPricing) {
            throw new MissingChannelConfigurationException(
                \sprintf(
                    'Channel %s has no price defined for product variant %s',
                    $context['channel']->getName(),
                    $productVariant->getName()
                )
            );
        }

        return $channelPricing;
    }

    /**
     * @return bool
     */
    private function isAdminRequest(): bool
    {
        $masterRequest = $this->requestStack->getMasterRequest();

        return $masterRequest !== null && \stripos($masterRequest->getPathInfo(), '/admin') !== false;
    }

    /**
     * @param array $context
     * @return array
     */
    private function resolveContext(array $context): array
    {
        /** @var ChannelInterface $channel */
        $channel = $context['channel'] ?? $this->channelContext->getChannel();
        $zone = $context['zone'] ?? $channel->getDefaultTaxZone();

        $context['channel'] = $channel;
        $context['zone'] = $zone;

        return $context;
    }

    private function calculateOnProductLevel(
        CustomerInterface $customer,
        ChannelPricingInterface $channelPricing,
        int $originalPrice,
        int $price
    ) {
        try {
            return $this->customerSurchargePriceCalculator->calculate(
                $this->customerSurchargePriceRepository->getAmountByCustomerAndChannel($customer, $channelPricing),
                $originalPrice
            );
        } catch (ResourceNotFoundException $e) {
            return $price;
        }
    }

    private function calculateOnSupplierLevel(
        ?CustomerInterface $customer,
        ?Supplier $supplier,
        int $originalPrice,
        int $price
    ) {
        try {
            return $this->customerSurchargePriceCalculator->calculate(
                $this->supplierSurchargePriceRepository->getAmountByCustomerAndSupplier($customer, $supplier),
                $originalPrice
            );
        } catch (ResourceNotFoundException $e) {
            return $price;
        }
    }
}
