<?php

declare(strict_types=1);

namespace App\Form\Extension\Product;

use App\Entity\Product\Product;
use App\Form\Type\Product\OeNumberType;
use Sylius\Bundle\ProductBundle\Form\Type\ProductType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductTypeExtension extends AbstractTypeExtension
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'weekly',
                CheckboxType::class,
                [
                    'required' => false,
                    'label'    => 'product.form.weekly',
                ]
            )
            ->add(
                'mostPopular',
                CheckboxType::class,
                [
                    'required' => false,
                    'label'    => 'product.form.most_popular',
                ]
            );

        $builder->add(
            'brandCode',
            TextType::class,
            [
                'label'    => 'product.form.brand_code',
                'required' => false,
            ]
        );

        $builder->add(
            'oeNumbers',
            CollectionType::class,
            [
                'label' => 'product.form.oe_codes',
                'label_attr' => [
                    'class' => 'ui dividing header'
                ],
                'entry_type' => OeNumberType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'required' => false,
                'by_reference' => false
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }

    public static function getExtendedTypes(): iterable
    {
        return [ProductType::class];
    }
}
