<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Form\Extension\Brand;

use Loevgaard\SyliusBrandPlugin\Form\Type\BrandType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class BrandTypeExtension extends AbstractTypeExtension
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add(
            'tecDocBrandId',
            TextType::class,
            [
                'label'    => 'loevgaard_sylius_brand.form.brand.tec_doc_brand_id',
                'required' => false,
            ]
        );
    }

    public static function getExtendedTypes(): iterable
    {
        return [BrandType::class];
    }
}
