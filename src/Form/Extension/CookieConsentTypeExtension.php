<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Form\Extension;

use ConnectHolland\CookieConsentBundle\Form\CookieConsentType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Routing\RouterInterface;

class CookieConsentTypeExtension extends AbstractTypeExtension
{
    /** @var RouterInterface */
    private $router;

    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->setAction($this->router->generate('app_cookie_consent'));

        $builder->add('necessary', ChoiceType::class, [
            'expanded' => true,
            'multiple' => false,
            'data'     => 'true',
            'disabled' => true,
            'choices'  => [
                ['ch_cookie_consent.yes' => 'true', 'ch_cookie_consent.no' => 'false'],
            ],
        ]);
    }

    public static function getExtendedTypes(): iterable
    {
        return [CookieConsentType::class];
    }
}
