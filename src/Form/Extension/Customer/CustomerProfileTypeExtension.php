<?php

declare(strict_types=1);

namespace App\Form\Extension\Customer;

use Sylius\Bundle\CustomerBundle\Form\Type\CustomerProfileType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class CustomerProfileTypeExtension extends AbstractTypeExtension
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('companyName', TextType::class, [
                'required' => false,
                'label' => 'customer.form.company_name',
            ])
            ->add('companyCode', TextType::class, [
                'required' => false,
                'label' => 'customer.form.company_code',
            ])
            ->add('vatNumber', TextType::class, [
                'required' => false,
                'label' => 'customer.form.vat_number',
            ]);
    }

    public static function getExtendedTypes(): iterable
    {
        return [CustomerProfileType::class];
    }
}
