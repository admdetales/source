<?php

declare(strict_types=1);

namespace App\Form\Type\Consent;

use App\Entity\Customer\CustomerInterface;
use App\Entity\Order\OrderInterface;
use App\Repository\Consent\ConsentRepositoryInterface;
use Symfony\Component\Form\AbstractType;
use App\Entity\Consent\Consent;
use App\Entity\Consent\ConsentAgreement;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Webmozart\Assert\Assert;

class ConsentType extends AbstractType
{
    /** @var ConsentRepositoryInterface */
    private $consentRepository;

    public function __construct(ConsentRepositoryInterface $consentRepository)
    {
        $this->consentRepository = $consentRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var Consent[] $consents */
        $consents = $this->consentRepository->findBy(['locationCode' => $options['locationCode']]);

        foreach ($consents as $consent) {
            $constraints = [];

            if ($consent->isMandatory()) {
                $constraints[] = new IsTrue(['groups' => ['sylius']]);
            }

            $builder->add(
                'consent_' . $consent->getId(),
                CheckboxType::class,
                [
                    'required' => $consent->isMandatory(),
                    'label' => $consent->getDescription(),
                    'constraints' => $constraints,
                    'attr' => ['consent' => $consent],
                ]
            );
        }

        $builder->addEventListener(
            FormEvents::POST_SUBMIT,
            static function (FormEvent $event) use ($builder) {
                /** @var OrderInterface $parentForm */
                $parentForm = $event->getForm()->getParent()->getData();
                Assert::isInstanceOf($parentForm, OrderInterface::class);

                /** @var CustomerInterface $customer */
                $consentSubject = $parentForm->getConsentSubject();
                $data = $event->getData();

                foreach ($data as $consentName => $agreed) {
                    /** @var Consent $consent */
                    $consent = $builder->get($consentName)->getOption('attr')['consent'];

                    $consentAgreement = new ConsentAgreement();
                    $consentAgreement->setConsent($consent);
                    $consentAgreement->setAgreed($agreed);

                    $consentSubject->addConsentAgreement($consentAgreement);
                }
            }
        );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setRequired('locationCode');
    }
}
