<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Form\Type\Supplier;

use App\Entity\Supplier\Supplier;
use App\Entity\Supplier\SupplierInterface;
use App\Form\Type\Surcharge\SupplierSurchargePriceType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Valid;
use Webmozart\Assert\Assert;

class SupplierPricingType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            static function (FormEvent $event): void {
                /** @var SupplierInterface $supplier */
                $supplier = $event->getData();
                Assert::isInstanceOf($supplier, SupplierInterface::class);

                $event->getForm()
                ->add(
                    'surchargePrices',
                    CollectionType::class,
                    [
                        'entry_type' => SupplierSurchargePriceType::class,
                        'label' => 'app.ui.surcharges',
                        'allow_add' => true,
                        'allow_delete' => true,
                        'by_reference' => false,
                        'property_path' => 'surchargePrices',
                        'constraints' => [
                            new Valid(),
                        ],
                    ]
                );
            }
        );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Supplier::class,
            ]
        );
    }
}
