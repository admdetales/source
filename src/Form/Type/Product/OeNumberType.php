<?php

declare(strict_types=1);

namespace App\Form\Type\Product;

use App\Entity\Product\OeNumber;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OeNumberType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add(
            'codeNumber',
            TextType::class,
            [
                'required' => true,
                'label'    => 'app.ui.oe_number_code',
            ]
        );

        $builder->add(
            'brandName',
            TextType::class,
            [
                'required' => true,
                'label'    => 'app.ui.oe_number_brand_name',
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            [
                'data_class' => OeNumber::class
            ]
        );
    }
}
