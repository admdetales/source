<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Form\Type\Product;

use App\Entity\Product\Product;
use App\Entity\Product\ProductInterface;
use App\Entity\Product\ProductVariantInterface;
use Sylius\Bundle\CoreBundle\Form\Type\ChannelCollectionType;
use Sylius\Bundle\CoreBundle\Form\Type\Product\ChannelPricingType;
use Sylius\Component\Channel\Model\ChannelInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Valid;
use Webmozart\Assert\Assert;

class ProductPricingType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            static function (FormEvent $event): void {
                /** @var ProductInterface $product */
                $product = $event->getData();
                Assert::isInstanceOf($product, ProductInterface::class);

                if ($product->getVariants()->isEmpty()) {
                    return;
                }

                /** @var ProductVariantInterface $variant */
                $variant = $product->getVariants()->first();
                Assert::isInstanceOf($variant, ProductVariantInterface::class);

                if ($variant->getChannelPricings()->isEmpty()) {
                    return;
                }

                $event->getForm()->add('variant', ProductVariantPricingType::class, [
                    'property_path' => 'variants[0]',
                    'constraints' => [
                        new Valid(),
                    ],
                    'label' => false,
                ]);
            }
        );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Product::class,
            ]
        );
    }
}
