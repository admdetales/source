<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Form\Type\TecDoc;

use App\Provider\TecDoc\UnselectedPopularManufacturerProvider;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

use function iterator_to_array;

class PopularManufacturerType extends AbstractType
{
    /** @var UnselectedPopularManufacturerProvider */
    private $popularManufacturerProvider;

    public function __construct(UnselectedPopularManufacturerProvider $popularManufacturerProvider)
    {
        $this->popularManufacturerProvider = $popularManufacturerProvider;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add(
            'manufacturerId',
            ChoiceType::class,
            [
                'choices' => iterator_to_array($this->popularManufacturerProvider->getUnselectedManufacturers()),
                'label'   => 'app.ui.manufacturer',
            ]
        );
    }
}
