<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Controller;

use Nfq\Bundle\TecDocBundle\Entity\VehicleManufacturer;
use Nfq\Bundle\TecDocBundle\Entity\VehicleModel;
use Nfq\Bundle\TecDocBundle\ModelManager\VehicleManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class VehicleController extends AbstractController
{
    /** @var VehicleManager */
    private $vehicleManager;

    /** @var NormalizerInterface */
    private $normalizer;

    public function __construct(VehicleManager $vehicleManager, NormalizerInterface $normalizer)
    {
        $this->vehicleManager = $vehicleManager;
        $this->normalizer     = $normalizer;
    }

    /**
     * @Route("/", name="app_vehicle_manufacturers", methods={"GET"})
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function manufacturersAction(): Response
    {
        return $this->render(
            '@SyliusShop/Vehicle/manufacturers.html.twig',
            $this->normalizer->normalize(
                [
                    'manufacturers' => $this->vehicleManager->getVehicleManufacturers(),
                ]
            )
        );
    }

    /**
     * @Route(
     *     "/{vehicleManufacturer}",
     *     name="app_vehicle_manufacturer",
     *     requirements={"vehicleManufacturer": "[^/]+"},
     *     methods={"GET"}
     * )
     *
     * @param VehicleManufacturer $vehicleManufacturer
     *
     * @return Response
     */
    public function modelsAction(VehicleManufacturer $vehicleManufacturer): Response
    {
        return $this->render(
            '@SyliusShop/Vehicle/models.html.twig',
            [
                'manufacturer' => $vehicleManufacturer,
                'models'       => $this->vehicleManager->getVehicleModels($vehicleManufacturer->getId())
            ]
        );
    }

    /**
     * @Route(
     *     "/{vehicleManufacturer}/{vehicleModel}",
     *     name="app_vehicle_model",
     *     requirements={
     *         "vehicleManufacturer": "[^/]+",
     *         "vehicleModel": "[^/]+"
     *     },
     *     methods={"GET"}
     * )
     * @ParamConverter("vehicleModel", options={"vehicleManufacturer": "vehicleManufacturer"})
     *
     * @param VehicleManufacturer $vehicleManufacturer
     * @param VehicleModel        $vehicleModel
     *
     * @return Response
     */
    public function enginesAction(VehicleManufacturer $vehicleManufacturer, VehicleModel $vehicleModel): Response
    {
        return $this->render(
            '@SyliusShop/Vehicle/engines.html.twig',
            [
                'manufacturer' => $vehicleManufacturer,
                'model'        => $vehicleModel,
                'vehicles'     => $this->vehicleManager->getVehicles(
                    $vehicleManufacturer->getId(),
                    $vehicleModel->getId()
                ),
            ]
        );
    }
}
