<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class ProductFeedController extends AbstractController
{
    /**
     * @Route("/product/remarketing-feed", name="app_remarketing_feed", methods={"GET"})
     */
    public function productRemarketingFeedCsv(): Response
    {
        $filePath = $this->getParameter('remarketing_directory') . $this->getParameter('remarketing_file_name');
        if (!file_exists($filePath)) {
            throw new NotFoundHttpException('Error encountered while fetching remarketing feed.');
        }
        $response = new Response();
        $date = date('Y-m-d');
        $response->headers->set('Content-type', 'text/csv');
        $response->headers->set('Cache-Control', 'private');
        $response->headers->set('Content-Disposition', 'attachment; filename="Remarketing_feed_' . $date . '.csv";');
        $response->sendHeaders();
        $response->setContent(file_get_contents($filePath));

        return $response;
    }
}
