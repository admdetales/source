<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Controller\Cms;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Validator\ValidatorInterface;

use function pathinfo;
use function sprintf;
use function transliterator_transliterate;
use function uniqid;

use const PATHINFO_FILENAME;

class AjaxController
{
    /** @var ValidatorInterface */
    private $validator;

    /** @var string */
    private $uploadsPath;

    /** @var string */
    private $publicPath;

    public function __construct(ValidatorInterface $validator, string $uploadsPath, string $publicPath)
    {
        $this->validator   = $validator;
        $this->uploadsPath = $uploadsPath;
        $this->publicPath  = $publicPath;
    }

    public function uploadImageAction(Request $request): Response
    {
        $file             = $request->files->get('upload');
        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);

        $violations = $this->validator->validate(
            $file,
            new File(
                [
                    'maxSize'   => '6M',
                    'mimeTypes' => ['image/*']
                ]
            )
        );

        if ($violations->count() > 0) {
            return new JsonResponse(
                [
                    'uploaded' => 0,
                    'error'    => [
                        'message' => $violations[0]->getMessage(),
                    ]
                ]
            );
        }

        // this is needed to safely include the file name as part of the URL
        $safeFilename = transliterator_transliterate(
            'Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()',
            $originalFilename
        );
        $newFilename  = $safeFilename . '-' . uniqid() . '.' . $file->guessExtension();

        $file->move($this->uploadsPath, $newFilename);

        return new JsonResponse(
            [
                'uploaded' => 1,
                'fileName' => $newFilename,
                'url'      => sprintf('%s/%s', $this->publicPath, $newFilename),
            ]
        );
    }
}
