<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Product\ProductInterface;
use App\Provider\TecDoc\ArticleProvider;
use Nfq\Bundle\TecDocBundle\Entity\VehicleManufacturer;
use Nfq\Bundle\TecDocBundle\ModelManager\VehicleManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

use function array_merge;

final class CompatibleVehicleController extends AbstractController
{
    /** @var ArticleProvider */
    private $articleProvider;

    /** @var VehicleManager */
    private $vehicleManager;

    /** @var NormalizerInterface */
    private $normalizer;

    public function __construct(
        ArticleProvider $articleProvider,
        VehicleManager $vehicleManager,
        NormalizerInterface $normalizer
    ) {
        $this->articleProvider = $articleProvider;
        $this->vehicleManager  = $vehicleManager;
        $this->normalizer      = $normalizer;
    }

    /**
     * @Route(
     *     "/vehicles/{product}/{articleId}/{manufacturerId}",
     *     name="app_compatible_vehicles",
     *     requirements={"product": "\d+", "articleId": "\d+", "manufacturerId": "\S+"},
     *     defaults={"articleId": null, "manufacturerId": null}
     * )
     * @ParamConverter("product", class="App\Entity\Product\Product")
     *
     * @param ProductInterface $product
     * @param int|null         $articleId
     * @param string|null      $manufacturerId
     *
     * @return Response
     * @throws ExceptionInterface
     */
    public function vehiclesAction(
        ProductInterface $product,
        ?int $articleId = null,
        ?string $manufacturerId = null
    ): Response {
        $vehicleManufacturers = [];
        $vehicles             = [];

        if ($articleId !== null) {
            $vehicleManufacturers = $this->vehicleManager->geArticleCompatibleVehicleManufacturers($articleId);

            if ($manufacturerId !== null) {
                $vehicles = $this->vehicleManager->getArticleCompatibleVehicles($articleId, $manufacturerId);
            }
        } else {
            $article = $this->articleProvider->findArticle($product);
            if ($article) {
                $articleId            = $article->getId();
                $vehicleManufacturers = $this->vehicleManager->geArticleCompatibleVehicleManufacturers($articleId);

                $manufacturer = reset($vehicleManufacturers);
                if ($manufacturer instanceof VehicleManufacturer) {
                    $vehicles = $this->vehicleManager->getArticleCompatibleVehicles(
                        $articleId,
                        (string)$manufacturer->getId()
                    );
                }
            }
        }

        return $this->render(
            '@SyliusShop/Product/Show/_compatibleVehicles.html.twig',
            array_merge(
                $this->normalizer->normalize(
                    [
                        'vehicleManufacturers' => $vehicleManufacturers,
                        'vehicles'             => $vehicles,
                        'articleId'            => $articleId,
                    ]
                ),
                [
                    'product' => $product,
                    'requestedManufacturerId' => $manufacturerId,
                ]
            )
        );
    }
}
