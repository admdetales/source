<?php

declare(strict_types=1);

namespace App\Controller\Order;

use App\Decorator\ProductDecorator;
use App\Entity\Product\Product;
use App\Entity\Product\ProductVariant;
use FOS\RestBundle\View\View;
use Sylius\Bundle\CoreBundle\Controller\OrderController as SyliusOrderController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class OrderController extends SyliusOrderController
{
    public function widgetAction(Request $request): Response
    {
        $configuration = $this->requestConfigurationFactory->create($this->metadata, $request);

        $cart = $this->getCurrentCart();

        if (!$configuration->isHtmlRequest()) {
            return $this->viewHandler->handle($configuration, View::create($cart));
        }

        $view = View::create()
            ->setTemplate($configuration->getTemplate('summary.html'))
            ->setData(['cart' => $cart])
        ;

        /** @var ProductDecorator $productDecorator */
        $productDecorator = $this->get(ProductDecorator::class);
        $products = [];

        foreach ($cart->getItems() as $item) {
            /** @var ProductVariant $variant */
            $variant = $item->getVariant();
            /** @var Product $product */
            $products[] = $variant->getProduct();
        }

        $productDecorator->decorateMultipleProducts($products);

        return $this->viewHandler->handle($configuration, $view);
    }
}
