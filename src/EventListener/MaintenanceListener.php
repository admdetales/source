<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\EventListener;

use App\Manager\MaintenanceManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\Templating\EngineInterface;

use function in_array;
use function strpos;

final class MaintenanceListener
{
    /** @var MaintenanceManager */
    private $maintenanceManager;

    /** @var EngineInterface */
    private $renderer;

    public function __construct(MaintenanceManager $maintenanceManager, EngineInterface $renderer)
    {
        $this->maintenanceManager = $maintenanceManager;
        $this->renderer           = $renderer;
    }

    public function onKernelRequest(RequestEvent $event): void
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        if (!$this->maintenanceManager->isUnderMaintenance()) {
            return;
        }

        $request = $event->getRequest();

        if ($this->isPathAllowed($request->getPathInfo(), ['/admin', '/_wdt', '/_profiler'])) {
            return;
        }

        if (in_array($request->getClientIp(), $this->maintenanceManager->getWhitelist(), true)) {
            return;
        }

        $event->setResponse(
            new Response(
                $this->renderer->render('Maintenance/maintenance.html.twig'),
                Response::HTTP_SERVICE_UNAVAILABLE
            )
        );
    }

    private function isPathAllowed(string $path, array $whitelist): bool
    {
        foreach ($whitelist as $item) {
            if (strpos($path, $item) !== false) {
                return true;
            }
        }

        return false;
    }
}
