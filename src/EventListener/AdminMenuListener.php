<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\EventListener;

use Knp\Menu\ItemInterface;
use Knp\Menu\Util\MenuManipulator;
use Sylius\Bundle\UiBundle\Menu\Event\MenuBuilderEvent;

class AdminMenuListener
{
    /** @var MenuManipulator */
    private $menuManipulator;

    public function __construct(MenuManipulator $menuManipulator)
    {
        $this->menuManipulator = $menuManipulator;
    }

    public function addAdminMenuItems(MenuBuilderEvent $event): void
    {
        $menu = $event->getMenu();

        $this->addProductPricingItem($menu);
        $this->addSupplierPricingItem($menu);
        $this->addConfigurationItems($menu);
        $this->addTecTocItems($menu);
        $this->addMapperItems($menu);
    }

    private function addProductPricingItem(ItemInterface $menu): void
    {
        $catalog = $menu->getChild('catalog');
        if (!$catalog) {
            return;
        }

        $catalog
            ->addChild('product-pricing', ['route' => 'app_admin_product_pricing_index'])
            ->setLabel('admin.menu.catalog.product_pricing')
            ->setLabelAttribute('icon', 'money');

        $this->menuManipulator->moveChildToPosition($catalog, $catalog->getChild('product-pricing'), 2);
    }

    private function addSupplierPricingItem(ItemInterface $menu): void
    {
        $catalog = $menu->getChild('catalog');
        if (!$catalog) {
            return;
        }

        $catalog
            ->addChild('supplier-pricing', ['route' => 'app_admin_supplier_pricing_index'])
            ->setLabel('admin.menu.catalog.supplier_pricing')
            ->setLabelAttribute('icon', 'money');

        $this->menuManipulator->moveChildToPosition($catalog, $catalog->getChild('supplier-pricing'), 3);
    }

    private function addConfigurationItems(ItemInterface $menu): void
    {
        $configuration = $menu->getChild('configuration');
        if (!$configuration) {
            return;
        }

        $translations = $configuration
            ->addChild('lexik-translations', ['route' => 'lexik_translation_overview'])
            ->setLabel('sylius.ui.translations')
            ->setLabelAttribute('icon', 'globe');

        $this->menuManipulator->moveToPosition($translations, 6);

        $configuration
            ->addChild('maintenance', ['route' => 'app_admin_maintenance_index'])
            ->setLabel('app.ui.shop_maintenance_mode')
            ->setLabelAttribute('icon', 'exclamation circle');
    }

    private function addTecTocItems(ItemInterface $menu): void
    {
        $tecDoc = $menu
            ->addChild('tec-doc')
            ->setLabel('admin.menu.main.tec_doc');

        $tecDoc
            ->addChild('tec-doc-popular-vehicles', ['route' => 'app_admin_popular_manufacturer_index'])
            ->setLabel('app.ui.popular_manufacturers')
            ->setLabelAttribute('icon', 'industry');
    }

    private function addMapperItems(ItemInterface $menu): void
    {
        $mapper = $menu
            ->addChild('mappers')
            ->setLabel('admin.menu.main.mappers');
        $mapper
            ->addChild('mappers-internal-brands', ['route' => 'app_admin_internal_brand_index'])
            ->setLabel('admin.menu.main.internal_brands')
            ->setLabelAttribute('icon', 'bandcamp');
        $mapper
            ->addChild('mappers-internal-categories', ['route' => 'app_admin_internal_category_index'])
            ->setLabel('admin.menu.main.internal_categories')
            ->setLabelAttribute('icon', 'folder');
    }
}
