<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\EventListener\Search;

use Doctrine\ORM\EntityManagerInterface;
use Omni\Sylius\SearchPlugin\Listener\ResourceListener as BaseResourceListener;
use Sylius\Bundle\ResourceBundle\Event\ResourceControllerEvent;

final class ResourceListener
{
    /** @var BaseResourceListener */
    private $listener;

    /** @var EntityManagerInterface */
    private $entityManager;

    public function __construct(BaseResourceListener $listener, EntityManagerInterface $entityManager)
    {
        $this->listener      = $listener;
        $this->entityManager = $entityManager;
    }

    public function createResourceIndex(ResourceControllerEvent $event): void
    {
        $this->entityManager->transactional(
            function () use ($event) {
                $this->listener->createResourceIndex($event);
            }
        );
    }

    public function onRemove(ResourceControllerEvent $event): void
    {
        $this->entityManager->transactional(
            function () use ($event) {
                $this->listener->onRemove($event);
            }
        );
    }
}
