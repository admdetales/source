<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Router\Generator;

use App\Entity\Taxonomy\Taxon;
use App\Transformer\UrlParameter\VehicleUrlParameterTransformer;
use Nfq\Bundle\TecDocBundle\Entity\Vehicle;
use Nfq\Bundle\TecDocBundle\Entity\VehicleManufacturer;
use Nfq\Bundle\TecDocBundle\Entity\VehicleModel;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

use function sprintf;
use function substr;

class VehicleUrlGenerator
{
    /** @var UrlGeneratorInterface */
    private $urlGenerator;

    /** @var VehicleUrlParameterTransformer */
    private $urlParameterTransformer;

    public function __construct(
        UrlGeneratorInterface $urlGenerator,
        VehicleUrlParameterTransformer $urlParameterTransformer
    ) {
        $this->urlGenerator            = $urlGenerator;
        $this->urlParameterTransformer = $urlParameterTransformer;
    }

    public function generateManufacturerUrl(VehicleManufacturer $vehicleManufacturer): string
    {
        return $this->urlGenerator->generate(
            'app_vehicle_manufacturer',
            [
                'vehicleManufacturer' => $this->generateVehicleManufacturerParameter($vehicleManufacturer),
            ]
        );
    }

    public function generateModelUrl(VehicleManufacturer $vehicleManufacturer, VehicleModel $vehicleModel): string
    {
        return $this->urlGenerator->generate(
            'app_vehicle_model',
            [
                'vehicleManufacturer' => $this->generateVehicleManufacturerParameter($vehicleManufacturer),
                'vehicleModel'        => $this->generateVehicleModelParameter($vehicleModel),
            ]
        );
    }

    public function generateProductUrl(Vehicle $vehicle, ?Taxon $taxon = null): string
    {
        return $this->urlGenerator->generate(
            'app_vehicle_product',
            [
                'vehicleManufacturer' => $this->generateVehicleManufacturerParameter($vehicle->getManufacturer()),
                'vehicleModel'        => $this->generateVehicleModelParameter($vehicle->getModel()),
                'vehicle'             => $this->urlParameterTransformer->transform(
                    (string)$vehicle->getId(),
                    sprintf(
                        '%s-%s-%s',
                        $vehicle->getCylinderCapacity(),
                        $vehicle->getFuelType() ? substr($vehicle->getFuelType(), 0, 1) : '',
                        $vehicle->getPowerKwTo() . 'kw'
                    )
                ),
                'taxonSlug'           => $taxon ? $taxon->getSlug() : null,
            ]
        );
    }

    private function generateVehicleManufacturerParameter(VehicleManufacturer $vehicleManufacturer): string
    {
        return $this->urlParameterTransformer->transform(
            (string)$vehicleManufacturer->getId(),
            $vehicleManufacturer->getTitle()
        );
    }

    private function generateVehicleModelParameter(VehicleModel $vehicleModel): string
    {
        return $this->urlParameterTransformer->transform(
            (string)$vehicleModel->getId(),
            sprintf(
                '%s-%s-%s',
                $vehicleModel->getTitle(),
                $vehicleModel->getFormatedYearOfConstructionFrom('Y'),
                $vehicleModel->getFormatedYearOfConstructionTo('Y')
            )
        );
    }
}
