<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Fixture;

use App\Entity\Locale\Locale;
use App\Entity\Taxonomy\InternalCategory;
use App\Entity\Taxonomy\TaxonInterface;
use App\Entity\Taxonomy\TaxonTranslation;
use Doctrine\ORM\EntityManagerInterface;
use League\Csv\Reader;
use League\Csv\Statement;
use Sylius\Bundle\FixturesBundle\Fixture\AbstractFixture;
use Sylius\Component\Taxonomy\Factory\TaxonFactoryInterface;
use Sylius\Component\Taxonomy\Generator\TaxonSlugGeneratorInterface;

use function array_filter;
use function array_map;
use function explode;
use function sprintf;

final class TaxonFixture extends AbstractFixture
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var TaxonFactoryInterface */
    private $taxonFactory;

    /** @var TaxonSlugGeneratorInterface */
    private $taxonSlugGenerator;

    /** @var string */
    private $taxonsFixtureFile;

    public function __construct(
        EntityManagerInterface $entityManager,
        TaxonFactoryInterface $taxonFactory,
        TaxonSlugGeneratorInterface $taxonSlugGenerator,
        string $taxonsFixtureFile
    ) {
        $this->entityManager      = $entityManager;
        $this->taxonFactory       = $taxonFactory;
        $this->taxonSlugGenerator = $taxonSlugGenerator;
        $this->taxonsFixtureFile  = $taxonsFixtureFile;
    }

    public function getName(): string
    {
        return 'ad_taxon';
    }

    public function load(array $options): void
    {
        $localeRepository = $this->entityManager->getRepository(Locale::class);

        /** @var string[] $locales */
        $locales = array_map(
            static function (Locale $locale) {
                return $locale->getCode();
            },
            $localeRepository->findAll()
        );

        $csv = Reader::createFromPath($this->taxonsFixtureFile, 'r');
        $csv->setHeaderOffset(0);

        /** @var TaxonInterface[] $taxons */
        $taxons  = [];
        $records = (new Statement())->process($csv);

        foreach ($records as $record) {
            $taxon = $this->createTaxon($record, $locales);

            $taxons[$record['CODE']] = $taxon;
        }

        foreach ($records as $record) {
            if ($record['PARENT'] === '') {
                continue;
            }

            $taxons[$record['CODE']]->setParent($taxons[$record['PARENT']]);
        }

        foreach ($records as $record) {
            $this->addTaxon($taxons[$record['CODE']]);
        }

        $this->entityManager->flush();
    }

    private function createTaxon(array $record, array $locales): TaxonInterface
    {
        /** @var TaxonInterface $taxon */
        $taxon = $this->taxonFactory->createNew();

        $taxon->setCode($record['CODE']);

        foreach ($locales as $locale) {
            $translation = new TaxonTranslation();
            $translation->setLocale($locale);
            $translation->setName($record['NAME-LT']);

            $taxon->addTranslation($translation);
        }

        $ids = array_filter(explode(', ', $record['GENERIC_ARTICLE_IDS']));
        if ($ids) {
            $taxon->setGenericArticleIds(array_map('intval', $ids));
        }

        return $taxon;
    }

    private function createInternalCategory(TaxonInterface $taxon): InternalCategory
    {
        $internalCategory = new InternalCategory();
        $internalCategory->setName($this->createInternalCategoryName($taxon));

        $this->entityManager->persist($internalCategory);

        return $internalCategory;
    }

    private function createSlug(TaxonInterface $taxon): void
    {
        /** @var TaxonTranslation $translation */
        foreach ($taxon->getTranslations() as $translation) {
            $translation->setSlug($this->taxonSlugGenerator->generate($taxon, $translation->getLocale()));
        }
    }

    private function addTaxon(TaxonInterface $taxon): void
    {
        $this->createSlug($taxon);

        $taxon->addInternalCategory($this->createInternalCategory($taxon));

        $this->entityManager->persist($taxon);
    }

    private function createInternalCategoryName(TaxonInterface $taxon): string
    {
        $name        = $taxon->getName();
        $parentTaxon = $taxon->getParent();

        if (!$parentTaxon) {
            return $name;
        }

        $parentName = $this->createInternalCategoryName($parentTaxon);

        return sprintf('%s / %s', $parentName, $name);
    }
}
