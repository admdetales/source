<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Importer;

use Doctrine\Common\Persistence\ObjectManager;
use FriendsOfSylius\SyliusImportExportPlugin\Importer\ImporterResultInterface;
use FriendsOfSylius\SyliusImportExportPlugin\Importer\ImportResultLoggerInterface;
use FriendsOfSylius\SyliusImportExportPlugin\Importer\ResourceImporter as BaseResourceImporter;
use FriendsOfSylius\SyliusImportExportPlugin\Processor\ResourceProcessorInterface;
use League\Csv\Reader;
use Port\Reader\ReaderFactory;
use Symfony\Component\Filesystem\Filesystem;
use App\Importer\Processor\Quantity;
use App\Importer\Processor\ProductInfo;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Omni\Sylius\FilterPlugin\Doctrine\ORM\ProductAttributeRepository;
use App\Repository\Product\ProductRepositoryInterface;
use App\Repository\Product\ProductAttributeValueRepositoryInterface;

class ResourceImporter extends BaseResourceImporter
{
    protected const IMPORT_ATTRIBUTE_CODE = 'last_import_provider_codes';
    protected const IMPORT_ATTRIBUTE_LT_NAME = 'Paskutinio importo tiekėjų kodai';

    /**
     * @var Filesystem
     */
    protected $filesystem;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManagerInterface;
    
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var ContainerBagInterface
     */
    protected $params;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var ProductAttributeValueRepositoryInterface
     */
    protected $productAttributeValueRepository;
    /**
     * @var ProductAttributeRepository
     */
    private $productAttributeRepository;

    /**
     * @var bool
     */
    private $shouldClearAttributeData;

    /**
     * {@inheritdoc}
     */
    public function __construct(
        ReaderFactory $readerFactory,
        ObjectManager $objectManager,
        ResourceProcessorInterface $resourceProcessor,
        ImportResultLoggerInterface $importerResult,
        EntityManagerInterface $entityManagerInterface,
        ContainerInterface $container,
        ContainerBagInterface $params,
        ProductRepositoryInterface $productRepository,
        ProductAttributeRepository $productAttributeRepository,
        ProductAttributeValueRepositoryInterface $productAttributeValueRepository,
        int $batchSize,
        bool $failOnIncomplete,
        bool $stopOnFailure,
        bool $shouldClearAttributeData = false
    ) {
        parent::__construct(
            $readerFactory,
            $objectManager,
            $resourceProcessor,
            $importerResult,
            $batchSize,
            $failOnIncomplete,
            $stopOnFailure
        );

        $this->filesystem = new Filesystem();
        $this->entityManagerInterface = $entityManagerInterface;
        $this->container = $container;
        $this->params = $params;
        $this->productRepository = $productRepository;
        $this->productAttributeValueRepository = $productAttributeValueRepository;
        $this->productAttributeRepository = $productAttributeRepository;
        $this->shouldClearAttributeData = $shouldClearAttributeData;
    }

    public function import(string $fileName): ImporterResultInterface
    {
        $this->result->start();

        if (!$this->filesystem->exists($fileName)) {
            $this->result->getLogger()->notice(\sprintf('Data import filename %s does not exist', $fileName));
            $this->result->stop();

            return $this->result;
        }

        $this->createProviderCodesAttribute();
        $providerAttributeNullified = false;
        $isProcessingQuantity = $this->resourceProcessor instanceof Quantity;

        if ($isProcessingQuantity) {
            $enabledProductCodes = [];
            $providerName = '';
        }

        $fileName = $this->renameFileForPreImport($fileName);

        $delimiter = $this->detectDelimiter($fileName);
        $csvReader = Reader::createFromPath($fileName);
        $csvReader->setHeaderOffset(0);
        $csvReader->setDelimiter($delimiter);

        foreach ($csvReader as $i => $row) {
            $dataEntry = $this->resourceProcessor->readDataEntry($row);

            if ($this->resourceProcessor instanceof ProductInfo) {
                $providerName = $dataEntry->getProductProviderName();
            } else {
                $providerName = $dataEntry->getProduct()->getProductProviderName();
            }
            
            if ($providerName == 'AD' && !$providerAttributeNullified && $this->shouldClearAttributeData) {
                $providerAttributeNullified = $this->nullifyProviderCodesAttribute();
            }

            if ($this->importData((int) $i, $row)) {
                break;
            }

            if ((int) $i % $this->batchSize === 0) {
                $this->objectManager->flush();
                $this->objectManager->clear();
            }     

            if ($isProcessingQuantity && $dataEntry->getQuantity()->getQuantity() != 0) {
                $enabledProductCodes[] = $dataEntry->getProduct()->getCode();
            }
        }

        $this->objectManager->flush();

        if ($isProcessingQuantity) {
            $this->disableUnchangedProducts(array_unique($enabledProductCodes), $providerName);
        }

        $this->result->stop();

        $this->renameFileForPostImport($fileName);

        return $this->result;
    }

    protected function renameFileForPreImport(string $fileName): string
    {
        ['dirname' => $dir, 'extension' => $ext, 'filename' => $fn] = \pathinfo($fileName);

        $newFileName = \sprintf('%s%s%s-started-%s.%s', $dir, \DIRECTORY_SEPARATOR, $fn, \time(), $ext);

        $this->filesystem->rename($fileName, $newFileName);

        return $newFileName;
    }

    protected function renameFileForPostImport(string $fileName): void
    {
        ['dirname' => $dir, 'extension' => $ext, 'filename' => $fn] = \pathinfo($fileName);

        $newFileName = \sprintf('%s%s%s-imported-%s.%s', $dir, \DIRECTORY_SEPARATOR, $fn, \time(), $ext);

        $this->filesystem->rename($fileName, $newFileName);
    }

    /**
     * @param string $filename
     * @return string
     */
    protected function detectDelimiter(string $filename): string
    {
        $fh = \fopen($filename, 'rb');

        $delimiters = [',', ';', '|', "\t"];

        $data2 = [];

        $delimiter = $delimiters[0];
        foreach ($delimiters as $d) {
            $data1 = \fgetcsv($fh, 4096, $d);

            if (\count($data1) > \count($data2)) {
                $delimiter = $d;
                $data2 = $data1;
            }

            \rewind($fh);
        }

        \fclose($fh);

        return $delimiter;
    }

    /**
     * @param array $enabledProductCodes
     * @param string $providerName
     * @return void
     */
    protected function disableUnchangedProducts(
        array $enabledProductCodes,
        string $providerName
    ): void
    {
        if ($providerName === '') {
            return;
        }

        $importCodeAttribute = $this->productAttributeRepository->findOneBy(['code' => self::IMPORT_ATTRIBUTE_CODE]);
        $this->productRepository->disableProductsByProviderAttribute($importCodeAttribute->getId(), $enabledProductCodes);
        $this->productRepository->enableProductsByProviderAttribute($importCodeAttribute->getId(), $enabledProductCodes);
    }

    /**
     * @param ProductAttributeRepository $attributeRepository
     * @return void
     */
    protected function createProviderCodesAttribute(): void
    {
        if (!$this->productAttributeRepository->findOneBy(['code' => self::IMPORT_ATTRIBUTE_CODE])) {
            $attributeFactory = $this->container->get('sylius.factory.product_attribute');
            $attribute = $attributeFactory->createTyped('text');
            $attribute->setName(self::IMPORT_ATTRIBUTE_LT_NAME);
            $attribute->setCode(self::IMPORT_ATTRIBUTE_CODE);
            $this->productAttributeRepository->add($attribute);
        }
    }

    /**
     * @return bool
     */
    protected function nullifyProviderCodesAttribute(): bool
    {
        $importCodeAttribute = $this->productAttributeRepository->findOneBy(['code' => self::IMPORT_ATTRIBUTE_CODE]);
        $this->productAttributeValueRepository->nullifyAttributeValueById($importCodeAttribute->getId());

        return true;
    }
}
