<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Importer\Utils;

use function Symfony\Component\String\u;

class OeNumberHelper
{
    /**
     * Tokenize OE Numbers from string into an array
     *
     * During tokenize process the values are processed and filtered.
     * Note: Only unique values are returned.
     *
     * @param string $inputData
     *
     * @return array
     * @see OeNumberHelper::normalizeToken()
     * @see OeNumberHelper::isValidToken()
     *
     */
    public static function tokenizeOeNumbers(string $inputData): array
    {
        $tokens = \explode(',', $inputData);

        $normalizedTokens = array_map('static::normalizeToken', $tokens);
        $filteredTokens = array_filter($normalizedTokens, 'static::isValidToken');

        return \array_unique($filteredTokens);
    }

    /**
     * Normalize given token
     *
     * - Remove any white-space characters from both sides of string
     * - Remove any single or double quotes from both sides of string
     *
     * @param string $token
     *
     * @return string
     */
    public static function normalizeToken(string $token): string
    {
        return trim($token, " \t\n\r\0\x0B'\"");
    }

    /**
     * Returns true in case given token is an accepted OE entry
     *
     * @see OeNumberHelper::isTokenNotEmpty()
     * @see OeNumberHelper::isTokenNotAbsentEntry()
     *
     * @param string $token
     *
     * @return bool
     */
    public static function isValidToken(string $token): bool
    {
        return static::isTokenNotEmpty($token) && static::isTokenNotAbsentEntry($token);
    }

    /**
     * Returns true if given token is NOT an empty value
     *
     * @param string $token
     *
     * @return bool
     */
    private static function isTokenNotEmpty(string $token): bool
    {
        return !empty($token);
    }

    /**
     * Returns true if given token does NOT contain an entry of "nera" in any form
     *
     * @param string $token
     *
     * @return bool
     */
    private static function isTokenNotAbsentEntry(string $token): bool
    {
        return (string)u($token)->ascii()->lower() !== 'nera';
    }
}
