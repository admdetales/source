<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Importer\Utils;

use Webmozart\Assert\Assert as BaseAssert;

class Assert
{
    /**
     * @param array $data
     * @param array $keys
     * @throws \InvalidArgumentException
     */
    public static function arrayHasKeys(array $data, array $keys): void
    {
        $errorMessage = \sprintf(
            'Wrong data structure provided. Expected "%s", actual: "%s"',
            \implode(',', $keys),
            \implode(',', \array_keys($data))
        );

        foreach ($keys as $key) {
            BaseAssert::keyExists(
                $data,
                $key,
                \sprintf('%s. Expected the key %s to exist.', $errorMessage, $key)
            );
        }
    }
}
