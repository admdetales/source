<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Importer\DTO;

class ProductInfo extends Product
{
    /**
     * @var int|null
     */
    private $brandCode;

    /**
     * @var string|null
     */
    private $brandName;

    /**
     * @var string|null
     */
    private $productBrandCode;

    /**
     * @var array
     */
    private $productNameMap;

    /**
     * @var string|null
     */
    private $productProviderCategory;

    /**
     * @var string[]
     */
    private $productOeNumbers;

    /**
     * @var string|null
     */
    private $supplier;

    /**
     * @var string
     */
    private $slug;

    /**
     * @var bool
     */
    private $visible;

    /**
     * @param string $productProviderName
     * @param string $productProviderCode
     * @param int|null $brandCode
     * @param string|null $brandName
     * @param string|null $productBrandCode
     * @param array $productNameMap
     * @param string|null $productProviderCategory
     * @param array $productOeNumbers
     * @param string|null $supplier
     * @param string $slug
     * @param bool $visible
     */
    public function __construct(
        string $productProviderName,
        string $productProviderCode,
        ?int $brandCode,
        ?string $brandName,
        ?string $productBrandCode,
        array $productNameMap,
        ?string $productProviderCategory,
        array $productOeNumbers,
        ?string $supplier,
        string $slug,
        bool $visible
    ) {
        parent::__construct($productProviderName, $productProviderCode);

        $this->brandCode = $brandCode;
        $this->brandName = $brandName;
        $this->productBrandCode = $productBrandCode;
        $this->productNameMap = $productNameMap;
        $this->productProviderCategory = $productProviderCategory;
        $this->productOeNumbers = $productOeNumbers;
        $this->supplier = $supplier;
        $this->slug = $slug;
        $this->visible = $visible;
    }

    /**
     * @return int|null
     */
    public function getBrandCode(): ?int
    {
        return $this->brandCode;
    }

    /**
     * @return string|null
     */
    public function getBrandName(): ?string
    {
        return $this->brandName;
    }

    /**
     * @return string|null
     */
    public function getProductBrandCode(): ?string
    {
        return $this->productBrandCode;
    }

    /**
     * @return array
     */
    public function getProductNameMap(): array
    {
        return $this->productNameMap;
    }

    /**
     * @param string $locale
     * @return string|null
     */
    public function getProductNameByLocale(string $locale): ?string
    {
        return $this->productNameMap[$locale] ?? null;
    }

    /**
     * @return string|null
     */
    public function getProductProviderCategory(): ?string
    {
        return $this->productProviderCategory;
    }

    /**
     * @return string[]
     */
    public function getProductOeNumbers(): array
    {
        return $this->productOeNumbers;
    }

    /**
     * @return string|null
     */
    public function getSupplier(): ?string
    {
        return $this->supplier;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @return bool
     */
    public function isVisible(): bool
    {
        return $this->visible;
    }
}
