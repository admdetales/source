<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Importer\DTO;

class Product
{
    /**
     * @var string
     */
    private $productProviderName;

    /**
     * @var string
     */
    private $productProviderCode;

    /**
     * @param string $productProviderName
     * @param string|null $productProviderCode
     */
    public function __construct(string $productProviderName, string $productProviderCode)
    {
        $this->productProviderName = $productProviderName;
        $this->productProviderCode = $productProviderCode;
    }

    public function getCode(): string
    {
        return \sprintf('%s-%s', $this->productProviderName, $this->productProviderCode);
    }

    /**
     * @return string
     */
    public function getProductProviderName(): ?string
    {
        return $this->productProviderName;
    }

    /**
     * @return string
     */
    public function getProductProviderCode(): ?string
    {
        return $this->productProviderCode;
    }
}
