<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Importer\DTO;

class Quantity
{
    /**
     * @var int
     */
    private $quantity;

    /**
     * @var string
     */
    private $warehouse;

    /**
     * @param int $quantity
     * @param string $warehouse
     */
    public function __construct(int $quantity, string $warehouse)
    {
        $this->quantity = $quantity;
        $this->warehouse = $warehouse;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @return string
     */
    public function getWarehouse(): string
    {
        return $this->warehouse;
    }
}
