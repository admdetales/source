<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Importer\Reader\Ad;

use App\Importer\DTO\Price;
use App\Importer\DTO\Product;
use App\Importer\DTO\ProductPrice;
use App\Importer\Reader\ProductPriceReaderInterface;
use App\Importer\Utils\Assert;

class AdProductPriceReader implements ProductPriceReaderInterface
{
    /**
     * {@inheritdoc}
     */
    public function read(array $data): ProductPrice
    {
        Assert::arrayHasKeys(
            $data,
            [
                AdDataReader::PRODUCT_PROVIDER_CODE_HEADER,
                AdDataReader::PRODUCT_ORIGINAL_PRICE_HEADER,
                AdDataReader::PRODUCT_PRICE_HEADER,
            ]
        );

        return new ProductPrice(
            new Product(
                AdDataReader::PRODUCT_PROVIDER_NAME,
                $this->readProductProviderCode($data)
            ),
            new Price(
                $this->readProductPrice($data),
                $this->readProductOriginalPrice($data)
            )
        );
    }

    public function readProductPrice(array $data): int
    {
        return (int)(\round((float)\trim((string)$data[AdDataReader::PRODUCT_PRICE_HEADER]), 2) * 100);
    }

    private function readProductOriginalPrice(array $data): int
    {
        return (int)(\round((float)\trim((string)$data[AdDataReader::PRODUCT_ORIGINAL_PRICE_HEADER]), 2) * 100);
    }

    private function readProductProviderCode(array $data): string
    {
        return \trim((string)$data[AdDataReader::PRODUCT_PROVIDER_CODE_HEADER]);
    }
}
