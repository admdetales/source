<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Importer\Reader\Ad;

final class AdDataReader
{
    public const PRODUCT_PROVIDER_NAME = 'AD';
    public const PRODUCT_PROVIDER_CODE_HEADER = 'Prekės ID';
    public const PRODUCT_ORIGINAL_PRICE_HEADER = 'Original price';
    public const PRODUCT_PRICE_HEADER = 'AD price';
    public const PRODUCT_QUANTITY_HEADER = 'Kiekis';
    public const PRODUCT_WAREHOUSE_NAME_HEADER = 'Sandėlis';
    public const BRAND_CODE_HEADER = 'Brand ID';
    public const BRAND_NAME_HEADER = 'Gamintojas';
    public const PRODUCT_BRAND_CODE_HEADER = 'Pagrindinis kodas';
    public const PRODUCT_NAME_HEADER = 'Pavadinimas';
    public const PRODUCT_NAME_RU_HEADER = 'Pavadinimas RU';
    public const PRODUCT_NAME_EN_HEADER = 'Pavadinimas EN';
    public const PRODUCT_PROVIDER_CATEGORY_HEADER = 'Prekės grupė';
    public const PRODUCT_OE_NUMBERS_HEADER = 'Požymis 1';
    public const PRODUCT_SUPPLIER_HEADER = 'Tiekėjas';
    public const PRODUCT_DISPLAY_HEADER = 'Nerodyti www';

    /**
     * Do not instantiate this class.
     */
    private function __construct()
    {
    }
}
