<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Importer\Reader;

use App\Importer\DTO\ProductInfo;

interface ProductInfoReaderInterface
{
    public const DEFAULT_LOCALE = 'lt_LT';
    public const LOCALE_RU = 'ru_RU';
    public const LOCALE_EN = 'en_US';

    /**
     * @param array $data
     * @return ProductInfo
     */
    public function read(array $data): ProductInfo;
}
