<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Importer\Reader\Hart;

final class HartDataReader
{
    public const PRODUCT_PROVIDER_NAME = 'HART';
    public const PRODUCT_PROVIDER_CODE_HEADER = 'productProviderCode';
    public const PRODUCT_PROVIDER_CODE_HEADER_PRODUCT_IMPORT = 'KOD_HART';
    public const PRODUCT_PROVIDER_CODE_HEADER_QUANTITY_IMPORT = HartDataReader::PRODUCT_PROVIDER_CODE_HEADER;
    public const PRODUCT_PROVIDER_CODE_HEADER_PRICE_IMPORT = HartDataReader::PRODUCT_PROVIDER_CODE_HEADER;
    public const PRODUCT_ORIGINAL_PRICE_HEADER = 'Original price';
    public const PRODUCT_PRICE_HEADER = 'price';
    public const PRODUCT_QUANTITY_HEADER = 'productQuantity';
    public const PRODUCT_WAREHOUSE_NAME_HEADER = 'productWarehouse';
    public const PRODUCT_WAREHOUSE_NAME = 'Tiekėjo sandėlys';
    public const QUANTITY_REVIEW_PERIOD = '-3 hour';
    public const BRAND_CODE_HEADER = 'TECDOC_KOD';
    public const BRAND_NAME_HEADER = 'DOSTAWCA';
    public const PRODUCT_BRAND_CODE_HEADER = 'KOD_TOWARU';
    public const PRODUCT_NAME_HEADER = 'NAZWA';
    public const PRODUCT_PROVIDER_CATEGORY_HEADER = 'KATEGORIA';
    public const PRODUCT_OE_NUMBERS_HEADER = 'OE_NUMBER';

    /**
     * Do not instantiate this class.
     */
    private function __construct()
    {
    }
}
