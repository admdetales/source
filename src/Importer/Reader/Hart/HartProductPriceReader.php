<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Importer\Reader\Hart;

use App\Importer\DTO\Price;
use App\Importer\DTO\Product;
use App\Importer\DTO\ProductPrice;
use App\Importer\Reader\ProductPriceReaderInterface;
use App\Importer\Utils\Assert;

class HartProductPriceReader implements ProductPriceReaderInterface
{
    /**
     * {@inheritdoc}
     */
    public function read(array $data): ProductPrice
    {
        Assert::arrayHasKeys(
            $data,
            [
                HartDataReader::PRODUCT_PROVIDER_CODE_HEADER_PRICE_IMPORT,
                HartDataReader::PRODUCT_ORIGINAL_PRICE_HEADER,
                HartDataReader::PRODUCT_PRICE_HEADER,
            ]
        );

        return new ProductPrice(
            new Product(
                HartDataReader::PRODUCT_PROVIDER_NAME,
                $this->readProductProviderCode($data)
            ),
            new Price(
                $this->readProductPrice($data),
                $this->readProductOriginalPrice($data)
            )
        );
    }

    public function readProductPrice(array $data): int
    {
        $priceValueWithDot = $this->getPriceValueSeparatedAsDot($data[HartDataReader::PRODUCT_PRICE_HEADER]);

        return $this->getPriceAsIntegerWithFractionalPart($priceValueWithDot);
    }

    private function readProductOriginalPrice(array $data): int
    {
        $priceValueWithDot = $this->getPriceValueSeparatedAsDot($data[HartDataReader::PRODUCT_ORIGINAL_PRICE_HEADER]);

        return $this->getPriceAsIntegerWithFractionalPart($priceValueWithDot);
    }

    private function getPriceValueSeparatedAsDot(string $priceValue): string
    {
        return $priceValueWithDot = str_replace(',', '.', $priceValue);
    }

    private function getPriceAsIntegerWithFractionalPart(string $priceValue): int
    {
        return (int)(\round((float)\trim((string)$priceValue), 2) * 100);
    }

    private function readProductProviderCode(array $data): string
    {
        return \trim((string)$data[HartDataReader::PRODUCT_PROVIDER_CODE_HEADER_PRICE_IMPORT]);
    }
}
