<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Importer\Reader\Hart;

use App\Importer\DTO\Product;
use App\Importer\DTO\ProductQuantity;
use App\Importer\DTO\Quantity;
use App\Importer\Reader\ProductQuantityReaderInterface;
use App\Importer\Utils\Assert;

class HartProductQuantityReader implements ProductQuantityReaderInterface
{
    /**
     * {@inheritdoc}
     */
    public function read(array $data): ProductQuantity
    {
        Assert::arrayHasKeys(
            $data,
            [
                HartDataReader::PRODUCT_QUANTITY_HEADER,
                HartDataReader::PRODUCT_PROVIDER_CODE_HEADER_QUANTITY_IMPORT,
                HartDataReader::PRODUCT_WAREHOUSE_NAME_HEADER,
            ]
        );

        return new ProductQuantity(
            new Product(
                HartDataReader::PRODUCT_PROVIDER_NAME,
                $this->readProductProviderCode($data)
            ),
            new Quantity(
                $this->readProductQuantity($data),
                HartDataReader::PRODUCT_WAREHOUSE_NAME
            )
        );
    }

    private function readProductProviderCode(array $data): string
    {
        return \trim((string)$data[HartDataReader::PRODUCT_PROVIDER_CODE_HEADER_QUANTITY_IMPORT]);
    }

    private function readProductWarehouseName(array $data): string
    {
        return \sprintf(
            '%s %s',
            HartDataReader::PRODUCT_WAREHOUSE_NAME,
            \trim((string)$data[HartDataReader::PRODUCT_WAREHOUSE_NAME_HEADER])
        );
    }

    private function readProductQuantity(array $data): int
    {
        $quantityData = \trim((string)$data[HartDataReader::PRODUCT_QUANTITY_HEADER]);

        return $quantityData === '>10' ? 10 : (int)$quantityData;
    }
}
