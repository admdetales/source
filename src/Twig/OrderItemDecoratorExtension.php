<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Twig;

use App\Decorator\OrderItemDecorator;
use Sylius\Component\Order\Model\OrderItemInterface;
use Sylius\Component\Order\Modifier\OrderItemQuantityModifierInterface;
use Sylius\Component\Product\Model\ProductVariantInterface;
use Sylius\Component\Resource\Factory\FactoryInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class OrderItemDecoratorExtension extends AbstractExtension
{
    /** @var FactoryInterface */
    private $orderItemFactory;

    /** @var OrderItemDecorator */
    private $orderItemDecorator;

    /** @var OrderItemQuantityModifierInterface */
    private $orderItemQuantityModifier;

    public function __construct(
        FactoryInterface $orderItemFactory,
        OrderItemDecorator $orderItemDecorator,
        OrderItemQuantityModifierInterface $orderItemQuantityModifier
    ) {
        $this->orderItemFactory = $orderItemFactory;
        $this->orderItemDecorator = $orderItemDecorator;
        $this->orderItemQuantityModifier = $orderItemQuantityModifier;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('get_decorated_order_item_from_variant', [$this, 'getDecoratedOrderItemFromVariant']),
        ];
    }

    /**
     * Get decorated order item from given product variant
     *
     * @param ProductVariantInterface|null $productVariant
     *
     * @return OrderItemInterface
     */
    public function getDecoratedOrderItemFromVariant(?ProductVariantInterface $productVariant)
    {
        /** @var OrderItemInterface $orderItem */
        $orderItem = $this->orderItemFactory->createNew();

        if ($productVariant) {
            $orderItem->setVariant($productVariant);

            $this->orderItemQuantityModifier->modify($orderItem, 1);
            $this->orderItemDecorator->decorate($orderItem);
        }

        return $orderItem;
    }
}
