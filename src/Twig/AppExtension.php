<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Twig;

use App\Entity\Product\Product;
use Omni\Sylius\CmsPlugin\Model\NodeInterface;
use Omni\Sylius\CmsPlugin\Repository\NodeRepositoryInterface;
use Sylius\Component\Taxonomy\Model\TaxonInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    /** @var NodeRepositoryInterface */
    private $nodeRepository;

    /** @var RequestStack */
    private $requestStack;

    /**
     * @param NodeRepositoryInterface $nodeRepository
     * @param RequestStack $requestStack
     */
    public function __construct(NodeRepositoryInterface $nodeRepository, RequestStack $requestStack)
    {
        $this->nodeRepository = $nodeRepository;
        $this->requestStack = $requestStack;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('app_get_product_node', [$this, 'getProductNode']),
            new TwigFunction('app_get_master_request', [$this, 'getMasterRequest']),
            new TwigFunction('app_get_full_taxon', [$this, 'getFullTaxonForProduct']),
            new TwigFunction('app_get_product_internal_category', [$this, 'getProductInternalCategory']),
        ];
    }

    public function getProductNode(Product $product): ?NodeInterface
    {
        $taxon = $product->getMainTaxon();
        if (null === $taxon) {
            return null;
        }

        return $this->nodeRepository->findOneBy(
            [
                'type' => 'taxon',
                'relationId' => $taxon->getId(),
                'enabled' => true,
            ]
        );
    }

    /**
     * @return Request|null
     */
    public function getMasterRequest(): ?Request
    {
        return $this->requestStack->getMasterRequest();
    }

    public function getFullTaxonForProduct(Product $product): ?string
    {
        $taxon = $product->getMainTaxon();

        return $taxon ? $this->makeFullTaxon($taxon) . '/' . $product->getCode() : null;
    }

    private function makeFullTaxon(TaxonInterface $taxon, array &$taxons = []): string
    {
        if ($taxon->getParent() !== null) {
            $taxons[] = $taxon->getCode();
            $this->makeFullTaxon($taxon->getParent(), $taxons);
        }

        return implode('/', array_reverse($taxons));
    }

    public function getProductInternalCategory(Product $product): string
    {
        $category = $product->getInternalCategory();

        return $category === null ? '' : $category->getName();
    }
}
